﻿/// <binding BeforeBuild='default' />
var gulp = require('gulp');
var sass = require('gulp-sass');
var cleanCSS = require('gulp-clean-css');
var rename = require("gulp-rename");
var uglify = require('gulp-uglify');
var beautify = require('gulp-html-beautify');
var templateCache = require('gulp-angular-templatecache');
var useRef = require('gulp-useref');
var concat = require('gulp-concat');
var ts = require('gulp-typescript');
var watch = require('gulp-watch');

gulp.task('sass', function () {
    return gulp.src('app/style/style.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('app/style/'));
});
// Minify compiled CSS
gulp.task('minify-css', ['sass'], function () {
    return gulp.src('app/style/*.css')
        .pipe(cleanCSS({
            compatibility: 'ie8'
        }))
        .pipe(rename({
            suffix: '.min'
        }))
        .pipe(gulp.dest('dist'));
});
var tsProject = ts.createProject({
    outFile: "output.js"
});

// Compile typescript to javascript
gulp.task('typescript', function () {
    return gulp.src('app/**/*.*.ts')
        .pipe(tsProject()).pipe(gulp.dest('dist'));
});
//  Minify custom JS
// gulp.task('minify-js', function () {
//     return gulp.src('dist/output.js')
//         .pipe(uglify())
//         .pipe(rename({
//             suffix: '.min'
//         }))
//         .pipe(gulp.dest('dist'));
// });

gulp.task('templates', function () {
    return gulp.src('app/**/*.html')
        .pipe(templateCache('templates.js', {
            root: 'app/',
            standalone: true,
            module: 'userprofile-templates',
            transformUrl: function (url) {
                return url.toLowerCase();
            }
        }))
        .pipe(gulp.dest('app'));
});

gulp.task('app', ['templates'], function () {
    return gulp.src('index.html')
        .pipe(useRef())
        .pipe(gulp.dest('dist'));
});
// Default task
gulp.task('default', ['minify-css', 'typescript', 'templates', 'app'], function () {
    gulp.watch('app/style/style.scss', ['sass']);
    gulp.watch('app/style/*.css', ['minify-css']);
    gulp.watch('app/**/*.*.ts', ['typescript']);
    gulp.watch('app/**/*.html', ['templates']);
    gulp.watch('index.html', ['app']);
});
