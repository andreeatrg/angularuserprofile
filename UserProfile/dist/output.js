/// <reference path="./userProfile.d.ts" />
var UserProfile;
(function (UserProfile) {
    "use strict";
    UserProfile.app = angular.module("userProfile", [
        "ui.router",
        "ngSanitize",
        "ngAnimate",
        "ngMessages",
        "ngCookies",
        "ui.bootstrap",
        "ngFileUpload",
        "chart.js",
        "ngImgCrop",
        "userprofile-templates"
    ]);
    UserProfile.app.config([
        "$stateProvider", "$urlRouterProvider", "$httpProvider", "$locationProvider",
        function ($stateProvider, $urlRouterProvider, $httpProvider, $locationProvider) {
            $stateProvider.state("home", {
                url: "/",
                reloadOnSearch: false,
                templateUrl: "app/main-view/main-view.html",
                controller: "MainCtrl",
                controllerAs: "vm",
                data: { requiresLogin: true }
            })
                .state("login", {
                url: "/login",
                templateUrl: "app/login/login.html",
                controller: "LoginCtrl",
                controllerAs: "vm"
            }).state("register", {
                url: "/register",
                controller: "RegisterCtrl",
                controllerAs: "vm",
                templateUrl: "app/login/register.html"
            }).state("forgotPassword", {
                url: "/forgot-password",
                controller: "ForgotPasswordCtrl",
                controllerAs: "vm",
                templateUrl: "app/login/forgot-password.html"
            }).state("reset", {
                url: "/reset/:id",
                templateUrl: "app/login/reset.html",
                controller: "ResetCtrl",
                controllerAs: "vm"
            }).state("portfolio", {
                url: "/portfolio",
                reloadOnSearch: false,
                templateUrl: "app/portfolio/portfolio.tpl.html",
                controller: "PortfolioCtrl",
                controllerAs: "vm",
                data: { requiresLogin: true }
            }).state("friends", {
                url: "/friends",
                reloadOnSearch: false,
                templateUrl: "app/friends/friends.tpl.html",
                controller: "FriendsCtrl",
                controllerAs: "vm",
                data: { requiresLogin: true }
            }).state("person", {
                url: "/people/{personId}",
                reloadOnSearch: false,
                templateUrl: "app/friend-details/friend-details.tpl.html",
                controller: "FriendDetailsCtrl",
                controllerAs: "vm",
                data: { requiresLogin: true }
            }).state("skills", {
                url: "/skills",
                reloadOnSearch: false,
                templateUrl: "app/skills/skills.tpl.html",
                controller: "SkillsCtrl",
                controllerAs: "vm",
                data: { requiresLogin: true }
            }).state("account-settings", {
                url: "/account-settings",
                reloadOnSearch: false,
                templateUrl: "app/account-settings/account-settings.tpl.html",
                controller: "AccountSettingsCtrl",
                controllerAs: "vm",
                data: { requiresLogin: true }
            }).state("portfolio-settings", {
                url: "/portfolio-settings",
                reloadOnSearch: false,
                templateUrl: "app/portfolio-settings/portfolio-settings.tpl.html",
                controller: "PortfolioSettingsCtrl",
                controllerAs: "vm",
                data: { requiresLogin: true }
            }).state("shopping-bag", {
                url: "/shopping-bag",
                reloadOnSearch: false,
                templateUrl: "app/shopping-bag/shopping-bag.tpl.html",
                controller: "ShoppingBagCtrl",
                controllerAs: "vm",
                data: { requiresLogin: true }
            });
            $urlRouterProvider.otherwise("/");
            $httpProvider.interceptors.push(["$q", "$location", function ($q, $location) {
                    return {
                        "request": function (config) {
                            config.headers = config.headers || {};
                            if (localStorage.token) {
                                config.headers.Authorization = "Bearer " + localStorage.token;
                            }
                            return config;
                        },
                        "responseError": function (response) {
                            console.log(response.status);
                            if (response.status === 401 || response.status === 403) {
                                localStorage.removeItem("isAuthenticated");
                                $location.path("/login");
                            }
                            return $q.reject(response);
                        }
                    };
                }]);
        }
    ]).run([
        "$rootScope", "$state", "$location", "AuthService",
        function ($rootScope, $state, $location, authService) {
            stateChanged($rootScope, $state, $location);
        }
    ]);
    function stateChanged($rootScope, $state, $location) {
        $rootScope.$on("$stateChangeStart", function (event, toState, toParams, fromState) {
            if (JSON.parse(localStorage.getItem("isAuthenticated")) && (toState.name === "login" || toState.name === "register" || toState.name === "forgotPassword" || toState.name === "reset")) {
                event.preventDefault();
                $location.path("/");
            }
        });
    }
})(UserProfile || (UserProfile = {}));
var UserProfile;
(function (UserProfile) {
    "use strict";
    var AccountSettingsCtrl = /** @class */ (function () {
        function AccountSettingsCtrl($scope, userProfileData, authService, $location) {
            this.$scope = $scope;
            this.userProfileData = userProfileData;
            this.authService = authService;
            this.$location = $location;
            this.alerts = [];
        }
        AccountSettingsCtrl.prototype.$onInit = function () {
            this.userId = localStorage.getItem("userId");
            this.user = this.userProfileData.getUserDetails();
        };
        AccountSettingsCtrl.prototype.checkPassword = function (password, confirmPassword, form) {
            this.form = form;
            if (password === confirmPassword) {
                this.form.confirmPassword.$error.validationError = false;
                return true;
            }
            else {
                this.form.confirmPassword.$error.validationError = true;
                return false;
            }
        };
        AccountSettingsCtrl.prototype.submitForm = function (form, file) {
            this.form = form;
            if (form.$valid) {
                this.saveChanges(file);
            }
            angular.forEach(form.$error, function (field) {
                angular.forEach(field, function (errorField) {
                    errorField.$setTouched();
                });
            });
        };
        AccountSettingsCtrl.prototype.saveChanges = function (file) {
            var _this = this;
            if (file != null) {
                this.userProfileData.saveChanges(file, this.user).then(function () {
                    _this.$location.path("/");
                })["catch"](function (err) {
                    _this.alerts.push({ type: "danger", message: err.data.message });
                });
            }
        };
        AccountSettingsCtrl.$inject = [
            "$scope",
            "UserProfileData",
            "AuthService",
            "$location"
        ];
        return AccountSettingsCtrl;
    }());
    UserProfile.AccountSettingsCtrl = AccountSettingsCtrl;
    UserProfile.app.controller("AccountSettingsCtrl", AccountSettingsCtrl);
})(UserProfile || (UserProfile = {}));
var UserProfile;
(function (UserProfile) {
    "use strict";
    var FooterCtrl = /** @class */ (function () {
        function FooterCtrl($scope) {
            var _this = this;
            this.$scope = $scope;
            this.$scope.$watch(function () { return localStorage.getItem("isAuthenticated"); }, function (newVal, oldVal) {
                _this.initData(newVal);
            });
        }
        FooterCtrl.prototype.$onInit = function () {
        };
        FooterCtrl.prototype.initData = function (isAuthenticated) {
            if (JSON.parse(isAuthenticated) === true) {
                this.year = new Date().getFullYear();
            }
            else {
                this.year = null;
            }
        };
        FooterCtrl.$inject = [
            "$scope",
        ];
        return FooterCtrl;
    }());
    UserProfile.FooterCtrl = FooterCtrl;
    UserProfile.app.controller("FooterCtrl", FooterCtrl);
})(UserProfile || (UserProfile = {}));
var UserProfile;
(function (UserProfile) {
    "use strict";
    var AddToCart = /** @class */ (function () {
        function AddToCart() {
            this.template = "";
            this.controller = "ShoppingBagCtrl";
            this.controllerAs = "vm";
            this.bindings = {
                item: "=",
                userId: "="
            };
        }
        return AddToCart;
    }());
    UserProfile.AddToCart = AddToCart;
    UserProfile.app.component("addToCart", new AddToCart());
})(UserProfile || (UserProfile = {}));
var UserProfile;
(function (UserProfile) {
    "use strict";
    var FriendDetailsCtrl = /** @class */ (function () {
        function FriendDetailsCtrl($scope, $element, userProfileData, $q, $uibModal, $state) {
            this.$scope = $scope;
            this.$element = $element;
            this.userProfileData = userProfileData;
            this.$q = $q;
            this.$uibModal = $uibModal;
            this.$state = $state;
        }
        FriendDetailsCtrl.prototype.$onInit = function () {
            var _this = this;
            this.userId = this.$state.params.personId;
            this.userProfileData.getUserData(this.userId).then(function (data) {
                _this.user = data;
                var count = 1;
                _this.getPortfolioImages(_this.userId).then(function (data) {
                    _this.portfolioData = _.flatMap(data, function (value) {
                        value.category = count;
                        var x = [];
                        _.forEach(value.files, function (file) {
                            x.push({
                                _id: value._id,
                                imageId: file._id,
                                path: file.path,
                                category: value.category,
                                portfolioName: value.portfolioName,
                                price: file.price,
                                createdAt: moment.utc(file.createdAt).format("Y/m/d")
                            });
                        });
                        count++;
                        return x;
                    });
                    if (_this.imageCategories.length) {
                        setTimeout(function () {
                            _this.$element.find(".filtr-container").filterizr();
                        }, 200);
                    }
                });
            });
        };
        FriendDetailsCtrl.prototype.getPortfolioImages = function (userId) {
            var _this = this;
            var deferred = this.$q.defer();
            var promise = this.userProfileData.getPortfolioList(userId).then(function (data) {
                _this.imageCategories = data;
                var images = _.map(data, function (category) {
                    return _this.userProfileData.getImages(userId, category.id);
                });
                _this.$q.all(images).then(function (data) {
                    deferred.resolve(data);
                });
            });
            return deferred.promise;
        };
        FriendDetailsCtrl.prototype.zoomImage = function (img) {
            var open = this.$uibModal.open({
                animation: true,
                templateUrl: "app/shared/modal/image-modal.tpl.html",
                windowClass: "show",
                backdropClass: "show",
                controller: "ModalController",
                controllerAs: "vm",
                size: "lg",
                resolve: {
                    entityName: function () {
                        return img;
                    }
                }
            });
            open.result.then(function (success) {
                if (success) {
                }
            });
        };
        FriendDetailsCtrl.prototype.addItemClass = function ($event) {
            _.forEach(this.$element.find(".simplefilter").children(), function (item) {
                angular.element(item).removeClass("active");
            });
            angular.element($event.currentTarget).addClass("active");
        };
        FriendDetailsCtrl.prototype.addToCart = function (item) {
            item.quantity = 1;
            this.userProfileData.addedItem({ item: item, user: this.user });
        };
        FriendDetailsCtrl.$inject = [
            "$scope",
            "$element",
            "UserProfileData",
            "$q",
            "$uibModal",
            "$state"
        ];
        return FriendDetailsCtrl;
    }());
    UserProfile.FriendDetailsCtrl = FriendDetailsCtrl;
    UserProfile.app.controller("FriendDetailsCtrl", FriendDetailsCtrl);
})(UserProfile || (UserProfile = {}));
var UserProfile;
(function (UserProfile) {
    "use strict";
    var FriendsCtrl = /** @class */ (function () {
        function FriendsCtrl($scope, userProfileData, $uibModal, $state) {
            this.$scope = $scope;
            this.userProfileData = userProfileData;
            this.$uibModal = $uibModal;
            this.$state = $state;
            this.friends = [];
            this.addedFriends = [];
        }
        FriendsCtrl.prototype.$onInit = function () {
            var _this = this;
            this.userId = localStorage.getItem("userId");
            this.userProfileData.getFriends().then(function (data) {
                _this.friends = data;
                _this.getAddedFriendsList();
                console.log(_this.friends);
            });
        };
        FriendsCtrl.prototype.onSelect = function ($item, $model, $label) {
            $item.fullName = $item.firstName + " " + $item.lastName;
        };
        FriendsCtrl.prototype.addFriend = function (friend) {
            var _this = this;
            friend.status = true;
            if (this.addedFriends) {
                this.updateFriendList(friend);
            }
            else {
                this.userProfileData.addFriend(this.userId, friend).then(function (data) {
                    _this.getAddedFriendsList();
                });
            }
        };
        FriendsCtrl.prototype.getAddedFriendsList = function () {
            var _this = this;
            this.userProfileData.getAddedFriendsList(this.userId).then(function (data) {
                _this.addedFriends = data;
                _this.friends = _.filter(_this.friends, function (friend) {
                    var foundFriend = _.find(_this.addedFriends, function (fr) {
                        return fr._id === friend._id;
                    });
                    if (foundFriend) {
                        console.log(foundFriend);
                        return foundFriend._id !== _this.userId && foundFriend._id !== friend._id;
                    }
                    else {
                        return friend._id !== _this.userId;
                    }
                });
                console.log(_this.friends);
            })["catch"](function () {
                _this.addedFriends = [];
                _this.friends = _.filter(_this.friends, function (friend) {
                    return friend._id !== _this.userId;
                });
            });
        };
        FriendsCtrl.prototype.updateFriendList = function (friend) {
            var _this = this;
            this.userProfileData.updateFriendsList(this.userId, friend).then(function (data) {
                _this.getAddedFriendsList();
            });
        };
        FriendsCtrl.prototype.removeFriend = function (friend) {
            var _this = this;
            var open = this.$uibModal.open({
                animation: true,
                templateUrl: "app/shared/modal/modal.tpl.html",
                windowClass: "show",
                backdropClass: "show",
                controller: "ModalController",
                controllerAs: "vm",
                resolve: {
                    entityName: function () {
                        return "delete" + " " + friend.firstName + " " + friend.lastName;
                    }
                }
            });
            open.result.then(function (success) {
                if (success) {
                    _this.friends.push(friend);
                    _this.userProfileData.removeFriend(_this.userId, friend._id).then(function (data) {
                        _this.getAddedFriendsList();
                    });
                }
            });
        };
        FriendsCtrl.$inject = [
            "$scope",
            "UserProfileData",
            "$uibModal",
            "$state"
        ];
        return FriendsCtrl;
    }());
    UserProfile.FriendsCtrl = FriendsCtrl;
    UserProfile.app.controller("FriendsCtrl", FriendsCtrl);
})(UserProfile || (UserProfile = {}));
var UserProfile;
(function (UserProfile) {
    "use strict";
    var ForgotPasswordCtrl = /** @class */ (function () {
        function ForgotPasswordCtrl($scope, authService, $timeout, $location) {
            this.$scope = $scope;
            this.authService = authService;
            this.$timeout = $timeout;
            this.$location = $location;
            this.user = new UserProfile.User();
            this.alerts = [];
        }
        ForgotPasswordCtrl.prototype.$onInit = function () { };
        ForgotPasswordCtrl.prototype.submitResetForm = function (form) {
            this.formRegister = form;
            if (form.$valid) {
                this.forgotPassword();
            }
            angular.forEach(form.$error, function (field) {
                angular.forEach(field, function (errorField) {
                    errorField.$setTouched();
                });
            });
        };
        ForgotPasswordCtrl.prototype.forgotPassword = function () {
            var _this = this;
            this.authService.forgotPassword(this.user).then(function (result) {
                _this.alerts.push({ type: "success", message: result.message });
            })["catch"](function (err) {
                _this.alerts.push({ type: "danger", message: err.data.message });
            });
        };
        ForgotPasswordCtrl.$inject = [
            "$scope",
            "AuthService",
            "$timeout",
            "$location"
        ];
        return ForgotPasswordCtrl;
    }());
    UserProfile.ForgotPasswordCtrl = ForgotPasswordCtrl;
    UserProfile.app.controller("ForgotPasswordCtrl", ForgotPasswordCtrl);
})(UserProfile || (UserProfile = {}));
var UserProfile;
(function (UserProfile) {
    "use strict";
    var LoginCtrl = /** @class */ (function () {
        function LoginCtrl($scope, authService, $location, userProfileData) {
            this.$scope = $scope;
            this.authService = authService;
            this.$location = $location;
            this.userProfileData = userProfileData;
            this.alerts = [];
        }
        LoginCtrl.prototype.$onInit = function () {
        };
        LoginCtrl.prototype.submitLoginForm = function (form) {
            this.form = form;
            if (form.$valid) {
                this.setToken();
            }
            angular.forEach(form.$error, function (field) {
                angular.forEach(field, function (errorField) {
                    errorField.$setTouched();
                });
            });
        };
        LoginCtrl.prototype.setToken = function () {
            var _this = this;
            this.authService.login(this.user).then(function (data) {
                localStorage.setItem("token", data.token);
                localStorage.setItem("userId", data.userId);
                localStorage.setItem("isAuthenticated", _.toString(data.isAuthenticated));
                _this.authService.isLoggedIn = data.isAuthenticated;
                console.log(_this.authService.isLoggedIn);
                if (_this.authService.isLoggedIn) {
                    _this.$location.path("/");
                }
            })["catch"](function (err) {
                _this.alerts.push({ type: "danger", message: err.data.message });
            });
        };
        LoginCtrl.$inject = [
            "$scope",
            "AuthService",
            "$location",
            "UserProfileData"
        ];
        return LoginCtrl;
    }());
    UserProfile.LoginCtrl = LoginCtrl;
    UserProfile.app.controller("LoginCtrl", LoginCtrl);
})(UserProfile || (UserProfile = {}));
var UserProfile;
(function (UserProfile) {
    "use strict";
    var RegisterCtrl = /** @class */ (function () {
        function RegisterCtrl($scope, authService, $timeout, $location) {
            this.$scope = $scope;
            this.authService = authService;
            this.$timeout = $timeout;
            this.$location = $location;
            this.user = new UserProfile.User();
            this.alerts = [];
        }
        RegisterCtrl.prototype.$onInit = function () { };
        RegisterCtrl.prototype.checkPassword = function (password, confirmPassword, form) {
            this.formRegister = form;
            if (password === confirmPassword) {
                this.formRegister.confirmPassword.$error.validationError = false;
                return true;
            }
            else {
                this.formRegister.confirmPassword.$error.validationError = true;
                return false;
            }
        };
        RegisterCtrl.prototype.submitRegisterForm = function (form, file) {
            this.formRegister = form;
            if (form.$valid) {
                this.registerUser(file);
            }
            angular.forEach(form.$error, function (field) {
                angular.forEach(field, function (errorField) {
                    errorField.$setTouched();
                });
            });
        };
        RegisterCtrl.prototype.registerUser = function (file) {
            if (file != null) {
                this.upload(file);
            }
        };
        RegisterCtrl.prototype.upload = function (file) {
            var _this = this;
            this.authService.registerUser(file, this.user).then(function (result) {
                _this.$location.path("/login");
            })["catch"](function (err) {
                _this.alerts.push({ type: "danger", message: err.data.message });
            });
        };
        RegisterCtrl.$inject = [
            "$scope",
            "AuthService",
            "$timeout",
            "$location"
        ];
        return RegisterCtrl;
    }());
    UserProfile.RegisterCtrl = RegisterCtrl;
    UserProfile.app.controller("RegisterCtrl", RegisterCtrl);
})(UserProfile || (UserProfile = {}));
var UserProfile;
(function (UserProfile) {
    "use strict";
    var ResetCtrl = /** @class */ (function () {
        function ResetCtrl($scope, authService, $stateParams, $location) {
            this.$scope = $scope;
            this.authService = authService;
            this.$stateParams = $stateParams;
            this.$location = $location;
            this.user = new UserProfile.User();
            this.alerts = [];
        }
        ResetCtrl.prototype.$onInit = function () { };
        ResetCtrl.prototype.checkPassword = function (password, confirmPassword, form) {
            this.resetForm = form;
            if (password === confirmPassword) {
                this.resetForm.confirmPassword.$error.validationError = false;
                return true;
            }
            else {
                this.resetForm.confirmPassword.$error.validationError = true;
                return false;
            }
        };
        ResetCtrl.prototype.submitResetForm = function (form) {
            this.resetForm = form;
            if (form.$valid) {
                this.resetPassword();
            }
            angular.forEach(form.$error, function (field) {
                angular.forEach(field, function (errorField) {
                    errorField.$setTouched();
                });
            });
        };
        ResetCtrl.prototype.resetPassword = function () {
            var _this = this;
            this.authService.resetPassword(this.$stateParams.id, { newPassword: this.user.password, confirmPassword: this.user.confirmPassword }).then(function (result) {
                _this.$location.path("/login");
            })["catch"](function (err) {
                _this.alerts.push({ type: "danger", message: err.data.message });
            });
        };
        ResetCtrl.$inject = [
            "$scope",
            "AuthService",
            "$stateParams",
            "$location"
        ];
        return ResetCtrl;
    }());
    UserProfile.ResetCtrl = ResetCtrl;
    UserProfile.app.controller("ResetCtrl", ResetCtrl);
})(UserProfile || (UserProfile = {}));
var UserProfile;
(function (UserProfile) {
    "use strict";
    var MainCtrl = /** @class */ (function () {
        function MainCtrl($scope, userProfileData, $element) {
            this.$scope = $scope;
            this.userProfileData = userProfileData;
            this.$element = $element;
        }
        MainCtrl.prototype.$onInit = function () {
            var _this = this;
            this.userProfileData.getMessages().then(function (resp) {
                _this.messages = resp;
            });
            this.userProfileData.getAlerts().then(function (resp) {
                _this.alerts = resp;
            });
            this.userProfileData.getChartData().then(function (resp) {
                _this.chart = resp;
            });
            this.userProfileData.getCustomers().then(function (resp) {
                _this.customers = resp;
                _this.$element.find("#dataTable").DataTable();
            });
        };
        MainCtrl.$inject = [
            "$scope",
            "UserProfileData",
            "$element"
        ];
        return MainCtrl;
    }());
    UserProfile.MainCtrl = MainCtrl;
    UserProfile.app.controller("MainCtrl", MainCtrl);
})(UserProfile || (UserProfile = {}));
var UserProfile;
(function (UserProfile) {
    "use strict";
    var User = /** @class */ (function () {
        function User() {
            this.file = undefined;
        }
        return User;
    }());
    UserProfile.User = User;
})(UserProfile || (UserProfile = {}));
var UserProfile;
(function (UserProfile) {
    "use strict";
    var DataType;
    (function (DataType) {
        DataType[DataType["Message"] = 1] = "Message";
        DataType[DataType["Alert"] = 2] = "Alert";
    })(DataType = UserProfile.DataType || (UserProfile.DataType = {}));
    var NavbarCtrl = /** @class */ (function () {
        function NavbarCtrl($scope, $uibModal, $location, authService, $element, userProfileData) {
            var _this = this;
            this.$scope = $scope;
            this.$uibModal = $uibModal;
            this.$location = $location;
            this.authService = authService;
            this.$element = $element;
            this.userProfileData = userProfileData;
            this.isMessage = false;
            this.isAlert = false;
            this.isExpand = true;
            this.isDataLoading = false;
            this.messages = [];
            this.alerts = [];
            this.$scope.$watch(function () { return localStorage.getItem("isAuthenticated"); }, function (newVal, oldVal) {
                _this.initData(newVal);
            });
        }
        NavbarCtrl.prototype.$onInit = function () { };
        NavbarCtrl.prototype.initData = function (isAuthenticated) {
            var _this = this;
            if (JSON.parse(isAuthenticated) === true) {
                this.userId = localStorage.getItem("userId");
                this.userProfileData.getUserData(this.userId).then(function (data) {
                    _this.user = data;
                    _this.userProfilePath = data.file;
                })["catch"](function (err) {
                    _this.userProfilePath = "images/profile/profile_user.jpg";
                });
                this.alertsInfo();
                this.messagesInfo();
            }
            else {
                this.user = null;
                this.$location.path("login");
                console.log("user null");
            }
        };
        NavbarCtrl.prototype.messagesInfo = function () {
            var _this = this;
            this.isDataLoading = true;
            this.userProfileData.getMessages().then(function (resp) {
                _this.messages = resp;
                _this.isDataLoading = false;
            });
        };
        NavbarCtrl.prototype.alertsInfo = function () {
            var _this = this;
            this.isDataLoading = true;
            this.userProfileData.getAlerts().then(function (resp) {
                _this.alerts = resp;
                _this.isDataLoading = false;
            });
        };
        NavbarCtrl.prototype.showDropdownData = function (type) {
            switch (type) {
                case DataType.Message:
                    this.isMessage = !this.isMessage;
                    this.isAlert = false;
                    break;
                case DataType.Alert:
                    this.isAlert = !this.isAlert;
                    this.isMessage = false;
                    break;
            }
        };
        NavbarCtrl.prototype.logOut = function () {
            var _this = this;
            var open = this.$uibModal.open({
                animation: true,
                templateUrl: "app/shared/modal/modal.tpl.html",
                controller: "ModalController",
                controllerAs: "vm",
                windowClass: "show",
                backdropClass: "show",
                resolve: {
                    entityName: function () {
                        return "logout";
                    }
                }
            });
            open.result.then(function (success) {
                if (success) {
                    _this.authService.isLoggedIn = false;
                    localStorage.removeItem("token");
                    localStorage.removeItem("isAuthenticated");
                    _this.$location.path("/login");
                }
            });
        };
        NavbarCtrl.prototype.toggleNav = function () {
            console.log(this.isExpand);
            this.isExpand = !this.isExpand;
            this.isExpand ? this.$element.parent().removeClass("sidenav-toggled") : this.$element.parent().addClass("sidenav-toggled");
        };
        NavbarCtrl.prototype.removeSidenavToggled = function (e) {
            e.preventDefault();
            angular.element("body").removeClass("sidenav-toggled");
        };
        NavbarCtrl.$inject = [
            "$scope",
            "$uibModal",
            "$location",
            "AuthService",
            "$element",
            "UserProfileData"
        ];
        return NavbarCtrl;
    }());
    UserProfile.NavbarCtrl = NavbarCtrl;
    UserProfile.app.controller("NavbarCtrl", NavbarCtrl);
})(UserProfile || (UserProfile = {}));
var UserProfile;
(function (UserProfile) {
    "use strict";
    var PortfolioCtrl = /** @class */ (function () {
        function PortfolioCtrl($scope, $element, userProfileData, $q, $uibModal, $location) {
            this.$scope = $scope;
            this.$element = $element;
            this.userProfileData = userProfileData;
            this.$q = $q;
            this.$uibModal = $uibModal;
            this.$location = $location;
        }
        PortfolioCtrl.prototype.$onInit = function () {
            var _this = this;
            this.userId = localStorage.getItem("userId");
            var count = 1;
            this.getPortfolioImages(this.userId).then(function (data) {
                _this.portfolioData = _.flatMap(data, function (value) {
                    value.category = count;
                    var x = [];
                    _.forEach(value.files, function (file) {
                        x.push({
                            _id: value._id,
                            imageId: file._id,
                            path: file.path,
                            category: value.category,
                            portfolioName: value.portfolioName,
                            price: file.price,
                            createdAt: moment.utc(file.createdAt).format("Y/m/d")
                        });
                    });
                    count++;
                    return x;
                });
                if (_this.imageCategories.length) {
                    setTimeout(function () {
                        _this.$element.find(".filtr-container").filterizr();
                    }, 200);
                }
            });
        };
        PortfolioCtrl.prototype.getPortfolioImages = function (userId) {
            var _this = this;
            var deferred = this.$q.defer();
            var promise = this.userProfileData.getPortfolioList(userId).then(function (data) {
                _this.imageCategories = data;
                var images = _.map(data, function (category) {
                    return _this.userProfileData.getImages(userId, category.id);
                });
                _this.$q.all(images).then(function (data) {
                    deferred.resolve(data);
                });
            });
            return deferred.promise;
        };
        PortfolioCtrl.prototype.removeImage = function (img) {
            var _this = this;
            console.log(img);
            var open = this.$uibModal.open({
                animation: true,
                templateUrl: "app/shared/modal/modal.tpl.html",
                windowClass: "show",
                backdropClass: "show",
                controller: "ModalController",
                controllerAs: "vm",
                resolve: {
                    entityName: function () {
                        return "delete";
                    }
                }
            });
            open.result.then(function (success) {
                if (success) {
                    var count_1 = 1;
                    _this.userProfileData.removePortfolioImage(_this.userId, img._id, img.imageId).then(function (data) {
                        _this.getPortfolioImages(_this.userId).then(function (data) {
                            _this.portfolioData = _.flatMap(data, function (value) {
                                value.category = count_1;
                                var x = [];
                                _.forEach(value.files, function (file) {
                                    x.push({ _id: value._id, imageId: file._id, path: file.path, category: value.category, portfolioName: value.portfolioName });
                                });
                                count_1++;
                                return x;
                            });
                            setTimeout(function () {
                                _this.$element.find(".filtr-container").filterizr();
                            }, 200);
                        });
                    });
                }
            });
        };
        PortfolioCtrl.prototype.zoomImage = function (img) {
            var open = this.$uibModal.open({
                animation: true,
                templateUrl: "app/shared/modal/image-modal.tpl.html",
                windowClass: "show",
                backdropClass: "show",
                controller: "ModalController",
                controllerAs: "vm",
                size: "lg",
                resolve: {
                    entityName: function () {
                        return img;
                    }
                }
            });
            open.result.then(function (success) {
                if (success) {
                }
            });
        };
        PortfolioCtrl.prototype.addItemClass = function ($event) {
            _.forEach(this.$element.find(".simplefilter").children(), function (item) {
                angular.element(item).removeClass("active");
            });
            angular.element($event.currentTarget).addClass("active");
        };
        PortfolioCtrl.$inject = [
            "$scope",
            "$element",
            "UserProfileData",
            "$q",
            "$uibModal",
            "$location"
        ];
        return PortfolioCtrl;
    }());
    UserProfile.PortfolioCtrl = PortfolioCtrl;
    UserProfile.app.controller("PortfolioCtrl", PortfolioCtrl);
})(UserProfile || (UserProfile = {}));
var UserProfile;
(function (UserProfile) {
    "use strict";
    var PortfolioSettingsCtrl = /** @class */ (function () {
        function PortfolioSettingsCtrl($scope, userProfileData, authService, $location) {
            this.$scope = $scope;
            this.userProfileData = userProfileData;
            this.authService = authService;
            this.$location = $location;
            this.alerts = [];
        }
        PortfolioSettingsCtrl.prototype.$onInit = function () {
            var _this = this;
            this.userId = localStorage.getItem("userId");
            this.userProfileData.getPortfolioList(this.userId).then(function (result) {
                _this.portfolio = result;
            })["catch"](function () {
            });
        };
        PortfolioSettingsCtrl.prototype.convert = function (files) {
            this.userProfileData.convertToBase64(files).then(function (data) {
                _.forEach(files, function (file, keyFile) {
                    file.path = data[keyFile];
                });
                console.log(files);
            });
        };
        PortfolioSettingsCtrl.prototype.uploadPortfolioImages = function (form, files, portfolio) {
            this.form = form;
            if (form.$valid) {
                var portfolioNameFound = _.find(this.portfolio, function (item) {
                    return item.id === portfolio;
                });
                if (portfolioNameFound) {
                    this.updatePortfolioImages(files, portfolioNameFound);
                }
                else {
                    var newPortfolio = {
                        name: portfolio,
                        id: null
                    };
                    this.savePortfolioImages(files, newPortfolio);
                }
            }
            angular.forEach(form.$error, function (field) {
                angular.forEach(field, function (errorField) {
                    errorField.$setTouched();
                });
            });
        };
        PortfolioSettingsCtrl.prototype.updatePortfolioImages = function (files, portfolio) {
            if (files.length) {
                this.userProfileData.updateImages(files, this.userId, portfolio).then(function (data) {
                    console.log("portfolio updated", data);
                });
            }
        };
        PortfolioSettingsCtrl.prototype.savePortfolioImages = function (files, portfolio) {
            if (files.length) {
                this.userProfileData.saveImages(files, this.userId, portfolio).then(function (data) {
                    console.log("portfolio saved", data);
                });
            }
        };
        PortfolioSettingsCtrl.$inject = [
            "$scope",
            "UserProfileData",
            "AuthService",
            "$location"
        ];
        return PortfolioSettingsCtrl;
    }());
    UserProfile.PortfolioSettingsCtrl = PortfolioSettingsCtrl;
    UserProfile.app.controller("PortfolioSettingsCtrl", PortfolioSettingsCtrl);
})(UserProfile || (UserProfile = {}));
var UserProfile;
(function (UserProfile) {
    "use strict";
    var ShoppingBagCtrl = /** @class */ (function () {
        function ShoppingBagCtrl($scope, $uibModal, $location, $element, userProfileData) {
            this.$scope = $scope;
            this.$uibModal = $uibModal;
            this.$location = $location;
            this.$element = $element;
            this.userProfileData = userProfileData;
            this.products = [];
            var products = [];
            this.userProfileData.onAddedItem(function (data) {
                products.push({ item: data.item, userId: data.user._id, userName: data.user.firstName + data.user.lastName });
                console.log(products);
                localStorage.setItem("products", JSON.stringify(products));
            });
        }
        ShoppingBagCtrl.prototype.$onInit = function () {
            this.products = JSON.parse(localStorage.getItem("products")) || [];
            console.log(this.products);
        };
        ShoppingBagCtrl.prototype.nextStep = function () {
        };
        ShoppingBagCtrl.prototype.buyImage = function (img, userId) {
            this.userProfileData.buyImage(userId, img).then(function (payment) {
                console.log(payment);
                for (var i = 0; i < payment.links.length; i++) {
                    if (payment.links[i].rel === "approval_url") {
                        window.location.href = payment.links[i].href;
                    }
                }
            });
        };
        ShoppingBagCtrl.$inject = [
            "$scope",
            "$uibModal",
            "$location",
            "$element",
            "UserProfileData"
        ];
        return ShoppingBagCtrl;
    }());
    UserProfile.ShoppingBagCtrl = ShoppingBagCtrl;
    UserProfile.app.controller("ShoppingBagCtrl", ShoppingBagCtrl);
})(UserProfile || (UserProfile = {}));
var UserProfile;
(function (UserProfile) {
    "use strict";
    var SkillsCtrl = /** @class */ (function () {
        function SkillsCtrl($scope, userProfileData) {
            this.$scope = $scope;
            this.userProfileData = userProfileData;
            this.devSkills = [];
        }
        SkillsCtrl.prototype.$onInit = function () {
            var _this = this;
            this.devSkills = [{
                    name: 'javascript',
                    value: 80
                },
                {
                    name: 'angular',
                    value: 80
                },
                {
                    name: 'jquery',
                    value: 70
                },
                {
                    name: 'typescript',
                    value: 85
                },
                {
                    name: 'html',
                    value: 90
                },
                {
                    name: 'css',
                    value: 90
                },
                {
                    name: 'bootstrap',
                    value: 85
                }];
            this.userProfileData.getChartData().then(function (data) {
                _this.chart = data;
            });
        };
        SkillsCtrl.$inject = [
            "$scope",
            "UserProfileData"
        ];
        return SkillsCtrl;
    }());
    UserProfile.SkillsCtrl = SkillsCtrl;
    UserProfile.app.controller("SkillsCtrl", SkillsCtrl);
})(UserProfile || (UserProfile = {}));
var UserProfile;
(function (UserProfile) {
    "use strict";
    var AuthService = /** @class */ (function () {
        function AuthService($http, $q, Upload) {
            var _this = this;
            this.$http = $http;
            this.$q = $q;
            this.Upload = Upload;
            this.isLoggedIn = false;
            this.alreadyLoggedIn = function () {
                return _this.isLoggedIn;
            };
            this.apiPath = "http://localhost:4000";
        }
        AuthService.prototype.$onInit = function () {
            console.log(this.isLoggedIn);
        };
        AuthService.prototype.login = function (data) {
            return this.post(this.apiPath + "/auth/login", data);
        };
        AuthService.prototype.forgotPassword = function (data) {
            return this.post(this.apiPath + "/auth/forgot_password", data);
        };
        AuthService.prototype.resetPassword = function (id, data) {
            return this.post(this.apiPath + "/auth/reset/" + id, data);
        };
        AuthService.prototype.registerUser = function (file, user) {
            var _this = this;
            return this.convertToBase64(file).then(function (baseUrl) {
                user.file = baseUrl;
                return _this.post(_this.apiPath + "/auth/register", user);
            });
        };
        AuthService.prototype.getUserProfileImage = function (userId) {
            return this.get(this.apiPath + "/image/" + userId);
        };
        AuthService.prototype.convertToBase64 = function (file) {
            var deferred = this.$q.defer();
            this.Upload.base64DataUrl(file).then(function (base64Urls) {
                deferred.resolve(base64Urls);
            });
            return deferred.promise;
        };
        AuthService.prototype.get = function (url) {
            return this.$http.get(url, {
                headers: {
                    "Content-Type": "application/json"
                }
            }).then(function (response) {
                return response.data;
            });
        };
        AuthService.prototype.put = function (url, data) {
            return this.$http.put(url, data).then(function (response) {
                return response.data;
            });
        };
        AuthService.prototype.post = function (url, data) {
            return this.$http.post(url, data).then(function (response) {
                return response.data;
            });
        };
        AuthService.prototype["delete"] = function (url) {
            return this.$http["delete"](url).then(function (response) {
                return response.data;
            });
        };
        AuthService.prototype.deleteWithBody = function (url, data) {
            return this.$http({
                method: "DELETE",
                url: url,
                data: data,
                headers: { "Content-Type": "application/json;charset=utf-8" }
            }).then(function (response) {
                return response.data;
            });
        };
        AuthService.$inject = ["$http", "$q",
            "Upload"];
        return AuthService;
    }());
    UserProfile.AuthService = AuthService;
    UserProfile.app.service("AuthService", AuthService);
})(UserProfile || (UserProfile = {}));
var UserProfile;
(function (UserProfile) {
    "use strict";
    var DeveloperSkills = /** @class */ (function () {
        function DeveloperSkills() {
            this.templateUrl = "app/shared/component/developer-skills.tpl.html";
            this.bindings = {
                devSkills: "="
            };
        }
        return DeveloperSkills;
    }());
    UserProfile.DeveloperSkills = DeveloperSkills;
    UserProfile.app.component("developerSkills", new DeveloperSkills());
})(UserProfile || (UserProfile = {}));
var UserProfile;
(function (UserProfile) {
    "use strict";
    var ModalCtrl = /** @class */ (function () {
        function ModalCtrl($modalInstance, entityName) {
            this.$modalInstance = $modalInstance;
            this.entityName = entityName;
        }
        ModalCtrl.prototype.$onInit = function () { };
        ModalCtrl.prototype.ok = function () {
            this.$modalInstance.close(true);
        };
        ModalCtrl.prototype.cancel = function () {
            this.$modalInstance.dismiss(false);
        };
        ModalCtrl.$inject = [
            "$uibModalInstance",
            "entityName"
        ];
        return ModalCtrl;
    }());
    UserProfile.ModalCtrl = ModalCtrl;
    UserProfile.app.controller("ModalController", ModalCtrl);
})(UserProfile || (UserProfile = {}));
var UserProfile;
(function (UserProfile) {
    "use strict";
    var UserProfileData = /** @class */ (function () {
        function UserProfileData($http, $q, Upload, authService, $rootScope) {
            this.$http = $http;
            this.$q = $q;
            this.Upload = Upload;
            this.authService = authService;
            this.$rootScope = $rootScope;
            this.apiPath = "http://localhost:4000";
        }
        UserProfileData.prototype.addFriend = function (userId, friend) {
            var data = [];
            data = data.concat(friend);
            return this.post(this.apiPath + "/friends/" + userId, { userId: userId, list: data });
        };
        UserProfileData.prototype.getAddedFriendsList = function (userId) {
            return this.get(this.apiPath + "/friends/" + userId);
        };
        UserProfileData.prototype.removeFriend = function (userId, friendId) {
            return this["delete"](this.apiPath + "/friends/" + friendId + "/" + userId);
        };
        UserProfileData.prototype.updateFriendsList = function (userId, friend) {
            return this.put(this.apiPath + "/friends/" + userId, { list: [friend] });
        };
        UserProfileData.prototype.saveChanges = function (file, user) {
            var _this = this;
            return this.convertToBase64([file]).then(function (baseUrl) {
                user.file = _.first(baseUrl);
                return _this.put(_this.apiPath + "/users/" + user._id, user);
            });
        };
        UserProfileData.prototype.buyImage = function (userId, img) {
            return this.post(this.apiPath + "/paypal/" + userId, { userId: userId, product: img });
        };
        UserProfileData.prototype.removePortfolioImage = function (userId, portfolioId, imgId) {
            return this["delete"](this.apiPath + "/portfolio/" + imgId + "/" + portfolioId + "/" + userId);
        };
        UserProfileData.prototype.convertToBase64 = function (files) {
            var _this = this;
            var deferred = this.$q.defer();
            if (files.length) {
                var promiseArr_1 = [];
                _.forEach(files, function (file) {
                    promiseArr_1.push(_this.Upload.resize(file, { height: 300, width: 300 }).then(function (resizedFile) {
                        return _this.Upload.base64DataUrl(resizedFile);
                    }));
                });
                this.$q.all(promiseArr_1).then(function (data) {
                    deferred.resolve(data);
                });
            }
            return deferred.promise;
        };
        UserProfileData.prototype.updateImages = function (files, userId, portfolio) {
            console.log(files);
            var newSchema = _.map(files, function (val) {
                return { path: val.path, price: val.price, createdAt: val.lastModifiedDate };
            });
            console.log(newSchema);
            return this.put(this.apiPath + "/portfolio/" + portfolio.id + "/" + userId, { files: newSchema });
        };
        UserProfileData.prototype.saveImages = function (files, userId, portfolio) {
            var newSchema = _.map(files, function (val) {
                return { path: val.path, price: val.price, createdAt: val.lastModifiedDate };
            });
            return this.post(this.apiPath + "/portfolio/" + userId, { files: newSchema, userId: userId, portfolioName: portfolio.name });
        };
        UserProfileData.prototype.getPortfolioList = function (userId) {
            return this.get(this.apiPath + "/portfolio/" + userId + "/list");
        };
        UserProfileData.prototype.getUserData = function (id) {
            var _this = this;
            return this.get(this.apiPath + "/users/" + id).then(function (data) { return _this.user = data; });
        };
        UserProfileData.prototype.getImages = function (userId, portfolioId) {
            return this.get(this.apiPath + "/portfolio/" + portfolioId + "/" + userId);
        };
        UserProfileData.prototype.getCustomers = function () {
            return this.get("app/shared/service/mock-data/customers.json");
        };
        UserProfileData.prototype.getChartData = function () {
            return this.get("app/shared/service/mock-data/chart.json");
        };
        UserProfileData.prototype.getFriends = function () {
            return this.get(this.apiPath + "/users");
        };
        UserProfileData.prototype.getMessages = function () {
            return this.get("app/shared/service/mock-data/messages.json");
        };
        UserProfileData.prototype.getAlerts = function () {
            return this.get("app/shared/service/mock-data/alerts.json");
        };
        UserProfileData.prototype.addedItem = function (data) {
            console.info(data);
            this.$rootScope.$emit("products", data);
        };
        UserProfileData.prototype.onAddedItem = function (handle) {
            return this.$rootScope.$on("products", function (event, data) {
                console.info(data);
                handle(data);
            });
        };
        UserProfileData.prototype.getUserDetails = function () {
            return this.user;
        };
        UserProfileData.prototype.get = function (url) {
            return this.$http.get(url).then(function (response) {
                return response.data;
            });
        };
        UserProfileData.prototype.put = function (url, data) {
            return this.$http.put(url, data).then(function (response) {
                return response.data;
            });
        };
        UserProfileData.prototype.post = function (url, data) {
            return this.$http.post(url, data).then(function (response) {
                return response.data;
            });
        };
        UserProfileData.prototype["delete"] = function (url) {
            return this.$http["delete"](url).then(function (response) {
                return response.data;
            });
        };
        UserProfileData.prototype.deleteWthBody = function (url, data) {
            return this.$http({
                method: "DELETE",
                url: url,
                data: data,
                headers: { "Content-Type": "application/json;charset=utf-8" }
            }).then(function (response) {
                return response.data;
            });
        };
        // metadataComplete: boolean;
        UserProfileData.$inject = ["$http", "$q", "Upload", "AuthService", "$rootScope"];
        return UserProfileData;
    }());
    UserProfile.UserProfileData = UserProfileData;
    UserProfile.app.service("UserProfileData", UserProfileData);
})(UserProfile || (UserProfile = {}));
