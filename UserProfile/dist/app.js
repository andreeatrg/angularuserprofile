angular.module("userprofile-templates", []).run(["$templateCache", function($templateCache) {$templateCache.put("app/account-settings/account-settings.tpl.html","<!-- container-fluid-->\r\n<div class=\"content-wrapper\">\r\n    <div class=\"container-fluid\">\r\n        <!-- Breadcrumbs-->\r\n        <ol class=\"breadcrumb\">\r\n            <li class=\"breadcrumb-item\">\r\n                <a href=\"#\">Account Settings</a>\r\n            </li>\r\n        </ol>\r\n\r\n        <div class=\"col-xl-12 col-sm-12 mb-3\">\r\n            <div ng-form=\"form\">\r\n                <div class=\"form-group\">\r\n                    <div class=\"form-row\">\r\n                        <div class=\"col-md-6\">\r\n                            <div class=\'text-left\'>\r\n                                <label for=\"firstName\">First name</label>\r\n                                <input name=\"firstName\" type=\'text\' class=\'form-control\' ng-model=\'vm.user.firstName\' />\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"col-md-6\">\r\n                            <div class=\'text-left\'>\r\n                                <label for=\"lastName\">Last name</label>\r\n                                <input name=\"lastName\" type=\'text\' class=\'form-control\' ng-model=\'vm.user.lastName\' />\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"col-md-6\">\r\n                            <div class=\'text-left\'>\r\n                                <label for=\"occupation\">Occupation</label>\r\n                                <input name=\"occupation\" type=\'text\' class=\'form-control\' ng-model=\'vm.user.occupation\' />\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"col-md-6\">\r\n                            <div class=\'text-left\' ng-class=\"{\'has-error\' : form.email.$invalid && form.email.$touched}\">\r\n                                <label for=\"email\">Email address</label>\r\n                                <input name=\"email\" type=\'email\' class=\'form-control\' ng-model=\'vm.user.email\' ng-pattern=\'/^(([^<>()\\[\\]\\.,;:\\s@\\\"]+(\\.[^<>()\\[\\]\\.,;:\\s@\\\"]+)*)|(\\\".+\\\"))@(([^<>()[\\]\\.,;:\\s@\\\"]+\\.)+[^<>()[\\]\\.,;:\\s@\\\"]{2,})$/i\'\r\n                                    required />\r\n                                <div ng-messages=\"form.email.$error\" ng-if=\"form.email.$invalid && form.email.$touched\">\r\n                                    <span ng-message=\"required\" class=\"help-block\">Email is required.</span>\r\n                                </div>\r\n                                <div ng-messages=\"form.email.$error\" ng-if=\"form.email.$invalid && form.email.$touched\">\r\n                                    <span ng-message=\"pattern\" class=\"help-block\">Email invalid.</span>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <div class=\"form-group\">\r\n                    <div class=\"form-row\">\r\n                        <div class=\"col-md-6\">\r\n                            <div class=\'text-left\' ng-class=\"{\'has-error\' : form.password.$invalid && form.password.$touched}\">\r\n                                <label for=\"password\">New Password</label>\r\n                                <input name=\"password\" type=\'password\' class=\'form-control\' ng-model=\'vm.user.password\' title=\"Please enter the old password!\"\r\n                                    ng-pattern=\"/^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{6,12}$/i\" />\r\n                                <div ng-messages=\"form.password.$error\" ng-if=\"form.password.$invalid && form.password.$touched\">\r\n                                    <span ng-message=\"pattern\" class=\"help-block\">Password must contain between 6 and 12 characters, including UPPER/lowercase and numbers.</span>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"col-md-6\">\r\n                            <div class=\'text-left\' ng-class=\"{\'has-error\' : form.confirmPassword.$invalid && form.confirmPassword.$touched}\">\r\n                                <label for=\"confirmPassword\">Confirm Password</label>\r\n                                <input name=\"confirmPassword\" type=\'password\' class=\'form-control\' ng-model=\'vm.user.confirmPassword\' ng-change=\"vm.checkPassword(vm.user.password, vm.user.confirmPassword, form)\"\r\n                                    title=\"Please enter the same Password as above\" />\r\n                                <div ng-messages=\"form.confirmPassword.$error\" ng-if=\"form.confirmPassword.$error.validationError\">\r\n                                    <span ng-message=\"validationError\" class=\"help-block\">Confirm Password is not the same with Password.</span>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <div class=\"form-group\">\r\n                    <div class=\"form-row\">\r\n                        <div class=\"col-md-12\">\r\n                            <div class=\'text-left\' ng-class=\"{\'has-error\' : form.file.$invalid && form.file.$touched}\">\r\n                                <input type=\"file\" ngf-select=\"\" ng-model=\"picFile\" name=\"file\" ngf-accept=\"\'image/*\'\" ngf-max-size=\"2MB\" ngf-model-invalid=\"vm.errorFile\">\r\n                                <div class=\"col-sm-6 col-md-3\">\r\n                                    <img ng-show=\"form.file.$valid\" ngf-thumbnail=\"picFile\" class=\"img-responsive\">\r\n                                    <button ng-click=\"picFile = null\" ng-show=\"picFile\" class=\"btn btn-success\">Remove</button>\r\n                                </div>\r\n                                <div ng-messages=\"form.file.$error\" ng-if=\"form.file.$invalid && form.file.$touched\">\r\n                                    <span ng-message=\"maxSize\" class=\"help-block\">File too large {{errorFile.size / 1000000|number:1}}MB: max 2M</span>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <a class=\"btn btn-primary btn-block\" ng-class=\"{\'disabled\':form.$invalid}\" ng-click=\"vm.submitForm(form,picFile)\" ng-disabled=\"form.$invalid\"\r\n                    href>Save settings</a>\r\n            </div>\r\n        </div>\r\n\r\n    </div>\r\n    <!-- end container-fluid-->\r\n</div>");
$templateCache.put("app/footer/footer.html","<footer class=\"sticky-footer\" ng-if=\"vm.year\">\r\n    <div class=\"container\">\r\n        <div class=\"text-center\">\r\n            <small>Copyright © User Profile {{vm.year}}</small>\r\n        </div>\r\n    </div>\r\n</footer>\r\n<!-- Scroll to Top Button-->\r\n<a class=\"scroll-to-top rounded\" href=\"#page-top\">\r\n    <i class=\"fa fa-angle-up\"></i>\r\n</a>");
$templateCache.put("app/friend-details/friend-details.tpl.html","<div class=\"content-wrapper\">\r\n    <div class=\"container-fluid\">\r\n        <!-- user details -->\r\n        <div class=\"row\">\r\n            <div class=\"col-md-12\">\r\n                <div class=\"portlet light portlet-fit bordered\">\r\n                    <div class=\"portlet-body\">\r\n                        <div class=\"mt-element-card mt-element-overlay\">\r\n                            <div class=\"row\">\r\n                                <div class=\"col-lg-3 col-md-4 col-sm-6 col-xs-12\">\r\n                                    <div class=\"mt-card-item\">\r\n                                        <div class=\"mt-card-avatar mt-overlay-1 mb-3\">\r\n                                            <img ng-src=\"{{vm.user.file}}\">\r\n                                            <div class=\"mt-overlay\">\r\n                                                <ul class=\"mt-info\">\r\n                                                    <li>\r\n                                                        <a class=\"btn default btn-outline\" ng-click=\"vm.zoomImage(vm.user.file)\">\r\n                                                            <i class=\"fa fa-search-plus\" aria-hidden=\"true\"></i>\r\n                                                        </a>\r\n                                                    </li>\r\n                                                    <li>\r\n                                                        <a class=\"btn default btn-outline\" ng-click=\"vm.removeFriend(vm.user)\">\r\n                                                            <i class=\"fa fa-trash\" aria-hidden=\"true\"></i>\r\n                                                        </a>\r\n                                                    </li>\r\n                                                </ul>\r\n                                            </div>\r\n                                        </div>\r\n                                        <div class=\"mt-card-content\">\r\n                                            <h3 class=\"mt-card-name\">{{vm.user.firstName}} {{vm.user.lastName}}</h3>\r\n                                            <p class=\"mt-card-email font-grey-mint\">{{vm.user.occupation}}</p>\r\n                                        </div>\r\n                                    </div>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <!-- Filter Controls - Simple Mode -->\r\n        <div class=\"row\" ng-if=\"vm.imageCategories.length\">\r\n            <!-- A basic setup of simple mode filter controls, all you have to do is use data-filter=\"all\"\r\n                for an unfiltered gallery and then the values of your categories to filter between them -->\r\n            <ul class=\"simplefilter\">\r\n                <li class=\"active\" data-filter=\"all\" ng-click=\"vm.addItemClass($event)\">All</li>\r\n                <li data-filter=\"{{category.category}}\" ng-click=\"vm.addItemClass($event)\" ng-repeat=\"category in vm.imageCategories\" ng-init=\"category.category = $index;category.category=category.category+1; \">{{category.name}}</li>\r\n            </ul>\r\n        </div>\r\n\r\n        <!-- Search control -->\r\n        <div class=\"search-row\" ng-if=\"vm.portfolioData.length\">\r\n            <input type=\"text\" class=\"form-control filtr-search\" placeholder=\"Search...\" name=\"filtr-search\" data-search>\r\n        </div>\r\n        <div class=\"row\" ng-if=\"vm.portfolioData.length\">\r\n            <!-- This is the set up of a basic gallery, your items must have the categories they belong to in a data-category\r\n                    attribute, which starts from the value 1 and goes up from there -->\r\n            <div class=\"filtr-container\">\r\n                <div class=\"col-xs-6 col-sm-4 col-md-3 filtr-item\" data-category=\"{{data.category}}\" data-sort=\"{{data.portfolioName}}\" ng-repeat=\"data in vm.portfolioData\">\r\n                    <div class=\"hoverd-area\">\r\n                        <img ng-src=\"{{data.path}}\" class=\"img-resonsive\" />\r\n                        <div class=\"img-hover\">\r\n                            <span>\r\n                                <i class=\"fa fa-search-plus\" aria-hidden=\"true\" ng-click=\"vm.zoomImage(data.path)\"></i>\r\n                                <i class=\"fa fa-trash\" aria-hidden=\"true\" ng-click=\"vm.removeImage(data)\"></i>\r\n                            </span>\r\n                        </div>\r\n                    </div>\r\n                    <div class=\"info\">\r\n                        <div class=\"row\">\r\n                            <div class=\"price col-md-6\">\r\n                                <h5>{{data.portfolioName}}</h5>\r\n                                <h5 class=\"price-text-color\">${{data.price}}</h5>\r\n                            </div>\r\n                            <div class=\"rating hidden-sm col-md-6\">\r\n                                <i class=\"price-text-color fa fa-star\"></i>\r\n                                <i class=\"price-text-color fa fa-star\">\r\n                                </i>\r\n                                <i class=\"price-text-color fa fa-star\"></i>\r\n                                <i class=\"price-text-color fa fa-star\">\r\n                                </i>\r\n                                <i class=\"fa fa-star\"></i>\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"separator clear-left\">\r\n                            <p class=\"btn-add\">\r\n                                <a href class=\"hidden-sm\" ng-click=\"vm.addToCart(data)\">\r\n                                    <i class=\"fa fa-shopping-cart\"></i> Add to cart</a>\r\n                            </p>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>");
$templateCache.put("app/friends/friends.tpl.html","<div class=\"content-wrapper\">\r\n    <div class=\"container-fluid\">\r\n        <!-- Breadcrumbs-->\r\n        <ol class=\"breadcrumb\">\r\n            <li class=\"breadcrumb-item\">\r\n                <a href=\"#\">Friends</a>\r\n            </li>\r\n        </ol>\r\n        <!-- Search control -->\r\n        <div class=\"search-friend mb-3\">\r\n            <input type=\"text\" ng-model=\"vm.selectedItem\" placeholder=\"Search a friend...\" uib-typeahead=\"friend as friend.fullName  for friend in vm.friends | filter:$viewValue\"\r\n                class=\"form-control mb-3\" typeahead-show-hint=\"true\" typeahead-min-length=\"0\" typeahead-template-url=\"customTemplate.html\"\r\n                typeahead-on-select=\"vm.onSelect($item, $model, $label)\" typeahead-no-results=\"noResults\">\r\n            <script type=\"text/ng-template\" id=\"customTemplate.html\">\r\n              <a href>\r\n                <img ng-src=\"{{match.model.file}}\" width=\"20\">\r\n                <span ng-bind-html=\"match.model.firstName  | uibTypeaheadHighlight:query\"></span>\r\n                <span ng-bind-html=\"match.model.lastName  | uibTypeaheadHighlight:query\"></span>\r\n              </a>\r\n            </script>\r\n            <div ng-show=\"noResults && vm.friends.length\">\r\n                <i class=\"glyphicon glyphicon-remove\"></i> No Results Found!\r\n            </div>\r\n            <div ng-show=\"noResults && !vm.friends.length\">\r\n                <i class=\"glyphicon glyphicon-remove\"></i> Friend list is empty!\r\n            </div>\r\n            <div class=\"mt-element-card mt-element-overlay\" ng-if=\"vm.friends.length && vm.selectedItem && !noResults\">\r\n                <div class=\"col-lg-3 col-md-4 col-sm-6 col-xs-12\">\r\n                    <div class=\"mt-card-item\">\r\n                        <div class=\"mt-card-avatar mt-overlay-1 mb-3\">\r\n                            <img ng-src=\"{{vm.selectedItem.file}}\" class=\"img-responsive\" />\r\n                            <div class=\"mt-overlay\">\r\n                                <ul class=\"mt-info\">\r\n                                    <li>\r\n                                        <a class=\"btn default btn-outline\" ng-click=\"vm.addFriend(vm.selectedItem);vm.selectedItem=\'\'\">\r\n                                            <i class=\"fa fa-plus\" aria-hidden=\"true\"></i>\r\n                                        </a>\r\n                                    </li>\r\n                                </ul>\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"mt-card-content\">\r\n                            <h3 class=\"mt-card-desc font-grey-mint\">{{vm.selectedItem.firstName}} {{vm.selectedItem.lastName}}</h3>\r\n                            <h3 class=\"mt-card-name\">{{vm.selectedItem.occupation}}</h3>\r\n                            <p class=\"mt-card-email font-grey-mint\">{{vm.selectedItem.email}}</p>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <div class=\"row\">\r\n            <div class=\"col-md-12\">\r\n                <div class=\"portlet light portlet-fit bordered\">\r\n                    <div class=\"portlet-body\">\r\n                        <div class=\"mt-element-card mt-element-overlay\">\r\n                            <div class=\"row\">\r\n                                <div class=\"col-lg-3 col-md-4 col-sm-6 col-xs-12\" ng-repeat=\"friend in vm.addedFriends  | orderBy:\'firstName\' track by friend._id\">\r\n                                    <div class=\"mt-card-item\">\r\n                                        <div class=\"mt-card-avatar mt-overlay-1 mb-3\">\r\n                                            <img ng-src=\"{{friend.file}}\">\r\n                                            <div class=\"mt-overlay\">\r\n                                                <ul class=\"mt-info\">\r\n                                                    <li>\r\n                                                        <a class=\"btn default btn-outline\" ui-sref=\"person({ personId: friend._id })\">\r\n                                                            <i class=\"fa fa-external-link\" aria-hidden=\"true\"></i>\r\n                                                        </a>\r\n                                                    </li>\r\n                                                    <li>\r\n                                                        <a class=\"btn default btn-outline\" ng-click=\"vm.removeFriend(friend)\">\r\n                                                            <i class=\"fa fa-trash\" aria-hidden=\"true\"></i>\r\n                                                        </a>\r\n                                                    </li>\r\n                                                </ul>\r\n                                            </div>\r\n                                        </div>\r\n                                        <div class=\"mt-card-content\">\r\n                                            <h3 class=\"mt-card-name\">{{friend.firstName}} {{friend.lastName}}</h3>\r\n                                            <p class=\"mt-card-email font-grey-mint\">{{friend.occupation}}</p>\r\n                                        </div>\r\n                                    </div>\r\n                                </div>\r\n                                <div class=\"col-lg-3 col-md-4 col-sm-6 col-xs-12\" ng-if=\"!vm.addedFriends.length\">\r\n                                    Friend list empty!\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>");
$templateCache.put("app/login/forgot-password.html","<div class=\"container\">\r\n    <div class=\"card card-login mx-auto mt-5\">\r\n        <div class=\"card-header\">Reset Password</div>\r\n        <div class=\"card-body\">\r\n            <div class=\"text-center mt-4 mb-5\">\r\n                <h4>Forgot your password?</h4>\r\n                <p>Enter your email address and we will send you instructions on how to reset your password.</p>\r\n            </div>\r\n            <div ng-form=\"resetForm\">\r\n                <div class=\"form-group\">\r\n                    <div class=\'text-left\' ng-class=\"{\'has-error\' : resetForm.email.$invalid && resetForm.email.$touched}\">\r\n                        <input name=\"email\" type=\'email\' class=\'form-control\' ng-model=\'vm.user.email\' ng-pattern=\'/^(([^<>()\\[\\]\\.,;:\\s@\\\"]+(\\.[^<>()\\[\\]\\.,;:\\s@\\\"]+)*)|(\\\".+\\\"))@(([^<>()[\\]\\.,;:\\s@\\\"]+\\.)+[^<>()[\\]\\.,;:\\s@\\\"]{2,})$/i\'\r\n                            required />\r\n                        <div ng-messages=\"resetForm.email.$error\" ng-if=\"resetForm.email.$invalid && resetForm.email.$touched\">\r\n                            <span ng-message=\"required\" class=\"help-block\">Email is required.</span>\r\n                        </div>\r\n                        <div ng-messages=\"resetForm.email.$error\" ng-if=\"resetForm.email.$invalid && resetForm.email.$touched\">\r\n                            <span ng-message=\"pattern\" class=\"help-block\">Email invalid.</span>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <a class=\"btn btn-primary btn-block\" ng-class=\"{\'disabled\':resetForm.$invalid}\" ng-click=\"vm.submitResetForm(resetForm)\"\r\n                    ng-disabled=\"formresetForm.$invalid\" href>Send</a>\r\n            </div>\r\n            <div class=\"text-center\">\r\n                <a class=\"d-block small mt-3\" ui-sref=\"register\">Register an Account</a>\r\n                <a class=\"d-block small\" ui-sref=\"login\">Login Page</a>\r\n            </div>\r\n        </div>\r\n        <div class=\"card-footer\" ng-if=\"vm.alerts.length\">\r\n            <div uib-alert ng-repeat=\"alert in vm.alerts\" typeof=\"{{alert.type}}\" ng-class=\"\'alert-\' + (alert.type)\" close=\"vm.alerts.splice(index, 1)\">{{alert.message}}</div>\r\n        </div>\r\n    </div>\r\n</div>");
$templateCache.put("app/login/login.html","<!--login-->\r\n<div class=\"card card-login mx-auto mt-5\">\r\n    <div class=\"card-header\">Login</div>\r\n    <div class=\"card-body\">\r\n        <div ng-form=\"form\">\r\n            <div class=\"form-group\">\r\n                <div class=\'text-left\' ng-class=\"{\'has-error\' : form.email.$invalid && form.email.$touched}\">\r\n                    <label for=\"email\">Email</label>\r\n                    <input name=\"email\" type=\'email\' class=\'form-control\' ng-model=\'vm.user.email\' ng-pattern=\'/^(([^<>()\\[\\]\\.,;:\\s@\\\"]+(\\.[^<>()\\[\\]\\.,;:\\s@\\\"]+)*)|(\\\".+\\\"))@(([^<>()[\\]\\.,;:\\s@\\\"]+\\.)+[^<>()[\\]\\.,;:\\s@\\\"]{2,})$/i\'\r\n                        required />\r\n                    <div ng-messages=\"form.email.$error\" ng-if=\"form.email.$invalid && form.email.$touched\">\r\n                        <span ng-message=\"required\" class=\"help-block\">Email is required.</span>\r\n                    </div>\r\n                    <div ng-messages=\"form.email.$error\" ng-if=\"form.email.$invalid && form.email.$touched\">\r\n                        <span ng-message=\"pattern\" class=\"help-block\">Email invalid.</span>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n            <div class=\"form-group\">\r\n                <div class=\'text-left\' ng-class=\"{\'has-error\' : form.password.$invalid && form.password.$touched}\">\r\n                    <label for=\"password\">Password</label>\r\n                    <input name=\"password\" type=\'password\' class=\'form-control\' ng-model=\'vm.user.password\' required />\r\n                    <div ng-messages=\"form.password.$error\" ng-if=\"form.password.$invalid && form.password.$touched\">\r\n                        <span ng-message=\"required\" class=\"help-block\">Password is required.</span>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n            <div class=\"form-group\">\r\n                <div class=\"form-check\">\r\n                    <label class=\"form-check-label\">\r\n                        <input class=\"form-check-input\" type=\"checkbox\" ng-model=\'vm.user.rememberPassword\'> Remember Password\r\n                    </label>\r\n                </div>\r\n            </div>\r\n            <a class=\"btn btn-primary btn-block\" ng-class=\"{\'disabled\':form.$invalid}\" ng-click=\"vm.submitLoginForm(form)\" ng-disabled=\"form.$invalid\"\r\n                href>Login</a>\r\n        </div>\r\n        <div class=\"text-center\">\r\n            <a class=\"d-block small mt-3\" ui-sref=\"register\">Register an Account</a>\r\n            <a class=\"d-block small\" ui-sref=\"forgotPassword\">Forgot Password?</a>\r\n        </div>\r\n    </div>\r\n    <div class=\"card-footer\" ng-if=\"vm.alerts.length\">\r\n        <div uib-alert ng-repeat=\"alert in vm.alerts\" typeof=\"{{alert.type}}\" ng-class=\"\'alert-\' + (alert.type)\" close=\"vm.alerts.splice(index, 1)\">{{alert.message}}</div>\r\n    </div>\r\n</div>");
$templateCache.put("app/login/register.html","<div class=\"container\">\r\n    <div class=\"card card-register mx-auto mt-5\">\r\n        <div class=\"card-header\">Register an Account</div>\r\n        <div class=\"card-body\">\r\n            <div ng-form=\"formRegister\">\r\n                <div class=\"form-group\">\r\n                    <div class=\"form-row\">\r\n                        <div class=\"col-md-6\">\r\n                            <div class=\'text-left\' ng-class=\"{\'has-error\' : formRegister.firstName.$invalid && formRegister.firstName.$touched}\">\r\n                                <label for=\"firstName\">First name</label>\r\n                                <input name=\"firstName\" type=\'text\' class=\'form-control\' ng-model=\'vm.user.firstName\' required />\r\n                                <div ng-messages=\"formRegister.firstName.$error\" ng-if=\"formRegister.firstName.$invalid && formRegister.firstName.$touched\">\r\n                                    <span ng-message=\"required\" class=\"help-block\">First Name is required.</span>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"col-md-6\">\r\n                            <div class=\'text-left\' ng-class=\"{\'has-error\' : formRegister.lastName.$invalid && formRegister.lastName.$touched}\">\r\n                                <label for=\"lastName\">Last name</label>\r\n                                <input name=\"lastName\" type=\'text\' class=\'form-control\' ng-model=\'vm.user.lastName\' required />\r\n                                <div ng-messages=\"formRegister.lastName.$error\" ng-if=\"formRegister.lastName.$invalid && formRegister.lastName.$touched\">\r\n                                    <span ng-message=\"required\" class=\"help-block\">First Name is required.</span>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <div class=\"form-group\">\r\n                    <div class=\'text-left\' ng-class=\"{\'has-error\' : formRegister.email.$invalid && formRegister.email.$touched}\">\r\n                        <label for=\"email\">Email address</label>\r\n                        <input name=\"email\" type=\'email\' class=\'form-control\' ng-model=\'vm.user.email\' ng-pattern=\'/^(([^<>()\\[\\]\\.,;:\\s@\\\"]+(\\.[^<>()\\[\\]\\.,;:\\s@\\\"]+)*)|(\\\".+\\\"))@(([^<>()[\\]\\.,;:\\s@\\\"]+\\.)+[^<>()[\\]\\.,;:\\s@\\\"]{2,})$/i\'\r\n                            required />\r\n                        <div ng-messages=\"formRegister.email.$error\" ng-if=\"formRegister.email.$invalid && formRegister.email.$touched\">\r\n                            <span ng-message=\"required\" class=\"help-block\">Email is required.</span>\r\n                        </div>\r\n                        <div ng-messages=\"formRegister.email.$error\" ng-if=\"formRegister.email.$invalid && formRegister.email.$touched\">\r\n                            <span ng-message=\"pattern\" class=\"help-block\">Email invalid.</span>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <div class=\"form-group\">\r\n                    <div class=\"form-row\">\r\n                        <div class=\"col-md-6\">\r\n                            <div class=\'text-left\' ng-class=\"{\'has-error\' : formRegister.password.$invalid && formRegister.password.$touched}\">\r\n                                <label for=\"password\">Password</label>\r\n                                <input name=\"password\" type=\'password\' class=\'form-control\' ng-model=\'vm.user.password\' required title=\"Password must contain between 6 and 12 characters, including UPPER/lowercase and numbers\"\r\n                                    ng-pattern=\"/^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{6,12}$/i\" />\r\n                                <div ng-messages=\"formRegister.password.$error\" ng-if=\"formRegister.password.$invalid && formRegister.password.$touched\">\r\n                                    <span ng-message=\"required\" class=\"help-block\">Password is required.</span>\r\n                                </div>\r\n                                <div ng-messages=\"formRegister.password.$error\" ng-if=\"formRegister.password.$invalid && formRegister.password.$touched\">\r\n                                    <span ng-message=\"pattern\" class=\"help-block\">Password must contain between 6 and 12 characters, including UPPER/lowercase and numbers.</span>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"col-md-6\">\r\n                            <div class=\'text-left\' ng-class=\"{\'has-error\' : formRegister.confirmPassword.$invalid && formRegister.confirmPassword.$touched}\">\r\n                                <label for=\"confirmPassword\">Confirm Password</label>\r\n                                <input name=\"confirmPassword\" type=\'password\' class=\'form-control\' ng-model=\'vm.user.confirmPassword\' required ng-change=\"vm.checkPassword(vm.user.password, vm.user.confirmPassword, formRegister)\"\r\n                                    title=\"Please enter the same Password as above\" />\r\n                                <div ng-messages=\"formRegister.confirmPassword.$error\" ng-if=\"formRegister.confirmPassword.$invalid && formRegister.confirmPassword.$touched\">\r\n                                    <span ng-message=\"required\" class=\"help-block\">Confirm Password is required.</span>\r\n                                </div>\r\n                                <div ng-messages=\"formRegister.confirmPassword.$error\" ng-if=\"formRegister.confirmPassword.$error.validationError\">\r\n                                    <span ng-message=\"validationError\" class=\"help-block\">Confirm Password is not the same with Password.</span>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"col-md-12\">\r\n                            <div class=\'text-left\'>\r\n                                <label for=\"occupation\">Occupation</label>\r\n                                <input name=\"occupation\" type=\'text\' class=\'form-control\' ng-model=\'vm.user.occupation\' />\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"col-md-12\">\r\n                            <div class=\'text-left\' ng-class=\"{\'has-error\' : formRegister.file.$invalid && formRegister.file.$touched}\">\r\n                                <input type=\"file\" ngf-select=\"vm.updateModel()\" ng-model=\"picFile\" name=\"file\" ngf-accept=\"\'image/*\'\" required=\"\" ngf-max-size=\"2MB\"\r\n                                    ngf-model-invalid=\"vm.errorFile\">\r\n                                <div class=\"col-sm-6 col-md-3\">\r\n                                    <img ng-show=\"formRegister.file.$valid\" ngf-thumbnail=\"picFile\" class=\"img-responsive\">\r\n                                    <button ng-click=\"picFile = null\" ng-show=\"picFile\" class=\"btn btn-success\">Remove</button>\r\n                                </div>\r\n\r\n                                <div ng-messages=\"formRegister.file.$error\" ng-if=\"formRegister.file.$invalid && formRegister.file.$touched\">\r\n                                    <span ng-message=\"required\" class=\"help-block\">Image is required.</span>\r\n                                </div>\r\n                                <div ng-messages=\"formRegister.file.$error\" ng-if=\"formRegister.file.$invalid && formRegister.file.$touched\">\r\n                                    <span ng-message=\"maxSize\" class=\"help-block\">File too large {{errorFile.size / 1000000|number:1}}MB: max 2M</span>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <a class=\"btn btn-primary btn-block\" ng-class=\"{\'disabled\':formRegister.$invalid}\" ng-click=\"vm.submitRegisterForm(formRegister,picFile)\"\r\n                    ng-disabled=\"formRegister.$invalid\" href>Register</a>\r\n            </div>\r\n            <div class=\"text-center\">\r\n                <a class=\"d-block small mt-3\" ui-sref=\"login\">Login Page</a>\r\n                <a class=\"d-block small\" ui-sref=\"forgotPassword\">Forgot Password?</a>\r\n            </div>\r\n        </div>\r\n        <div class=\"card-footer\" ng-if=\"vm.alerts.length\">\r\n            <div uib-alert ng-repeat=\"alert in vm.alerts\" typeof=\"{{alert.type}}\" ng-class=\"\'alert-\' + (alert.type)\" close=\"vm.alerts.splice(index, 1)\">{{alert.message}}</div>\r\n        </div>\r\n    </div>\r\n</div>");
$templateCache.put("app/login/reset.html","<div class=\"container\">\r\n    <div class=\"card card-login mx-auto mt-5\">\r\n        <div class=\"card-header\">Reset Password</div>\r\n        <div class=\"card-body\">\r\n            <div ng-form=\"resetForm\">\r\n                <div class=\"form-group\">\r\n                    <div class=\'text-left\' ng-class=\"{\'has-error\' : resetForm.password.$invalid && resetForm.password.$touched}\">\r\n                        <label for=\"password\">New Password</label>\r\n                        <input name=\"password\" type=\'password\' class=\'form-control\' ng-model=\'vm.user.password\' required title=\"Please enter the old password!\"\r\n                            ng-pattern=\"/^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{6,12}$/i\" />\r\n                        <div ng-messages=\"resetForm.password.$error\" ng-if=\"resetForm.password.$invalid && resetForm.password.$touched\">\r\n                            <span ng-message=\"required\" class=\"help-block\">Password is required.</span>\r\n                        </div>\r\n                        <div ng-messages=\"resetForm.password.$error\" ng-if=\"resetForm.password.$invalid && resetForm.password.$touched\">\r\n                            <span ng-message=\"pattern\" class=\"help-block\">Password must contain between 6 and 12 characters, including UPPER/lowercase and numbers.</span>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <div class=\"form-group\">\r\n                    <div class=\'text-left\' ng-class=\"{\'has-error\' : resetForm.confirmPassword.$invalid && resetForm.confirmPassword.$touched}\">\r\n                        <label for=\"confirmPassword\">Confirm Password</label>\r\n                        <input name=\"confirmPassword\" type=\'password\' class=\'form-control\' ng-model=\'vm.user.confirmPassword\' required ng-change=\"vm.checkPassword(vm.user.password, vm.user.confirmPassword, resetForm)\"\r\n                            title=\"Please enter the same Password as above\" />\r\n                        <div ng-messages=\"resetForm.confirmPassword.$error\" ng-if=\"resetForm.confirmPassword.$invalid && resetForm.confirmPassword.$touched\">\r\n                            <span ng-message=\"required\" class=\"help-block\">Confirm Password is required.</span>\r\n                        </div>\r\n                        <div ng-messages=\"resetForm.confirmPassword.$error\" ng-if=\"resetForm.confirmPassword.$error.validationError\">\r\n                            <span ng-message=\"validationError\" class=\"help-block\">Confirm Password is not the same with Password.</span>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <a class=\"btn btn-primary btn-block\" ng-class=\"{\'disabled\':resetForm.$invalid}\" ng-click=\"vm.submitResetForm(resetForm)\"\r\n                    ng-disabled=\"formresetForm.$invalid\" href>Reset Password</a>\r\n            </div>\r\n            <div class=\"text-center\">\r\n                <a class=\"d-block small mt-3\" ui-sref=\"register\">Register an Account</a>\r\n                <a class=\"d-block small mt-3\" ui-sref=\"forgotPassword\">Forgot Password?</a>\r\n                <a class=\"d-block small\" ui-sref=\"login\">Login Page</a>\r\n            </div>\r\n        </div>\r\n        <div class=\"card-footer\" ng-if=\"vm.alerts.length\">\r\n            <div uib-alert ng-repeat=\"alert in vm.alerts\" typeof=\"{{alert.type}}\" ng-class=\"\'alert-\' + (alert.type)\" close=\"vm.alerts.splice(index, 1)\">{{alert.message}}</div>\r\n        </div>\r\n    </div>\r\n</div>");
$templateCache.put("app/main-view/main-view.html","<!-- container-fluid-->\r\n<div class=\"content-wrapper\">\r\n    <div class=\"container-fluid\">\r\n        <!-- Breadcrumbs-->\r\n        <ol class=\"breadcrumb\">\r\n            <li class=\"breadcrumb-item\">\r\n                <a href=\"#\">About</a>\r\n            </li>\r\n        </ol>\r\n        <!-- Icon Cards-->\r\n        <div class=\"row\">\r\n            <div class=\"col-xl-6 col-sm-6 mb-3\">\r\n                <div class=\"card text-white bg-primary o-hidden h-100\">\r\n                    <div class=\"card-body\">\r\n                        <div class=\"card-body-icon\">\r\n                            <i class=\"fa fa-fw fa-comments\"></i>\r\n                        </div>\r\n                        <div class=\"mr-5\">{{vm.messages.length}} New Messages!</div>\r\n                    </div>\r\n                    <a class=\"card-footer text-white clearfix small z-1\" href=\"#\">\r\n                        <span class=\"float-left\">View Details</span>\r\n                        <span class=\"float-right\">\r\n                            <i class=\"fa fa-angle-right\"></i>\r\n                        </span>\r\n                    </a>\r\n                </div>\r\n            </div>\r\n            <div class=\"col-xl-6 col-sm-6 mb-3\">\r\n                <div class=\"card text-white bg-warning o-hidden h-100\">\r\n                    <div class=\"card-body\">\r\n                        <div class=\"card-body-icon\">\r\n                            <i class=\"fa fa-fw fa-list\"></i>\r\n                        </div>\r\n                        <div class=\"mr-5\">{{vm.alerts.length}} New Notifications!</div>\r\n                    </div>\r\n                    <a class=\"card-footer text-white clearfix small z-1\" href=\"#\">\r\n                        <span class=\"float-left\">View Details</span>\r\n                        <span class=\"float-right\">\r\n                            <i class=\"fa fa-angle-right\"></i>\r\n                        </span>\r\n                    </a>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <!--  Chart -->\r\n        <div class=\"row\">\r\n            <div class=\"col-lg-8\">\r\n                <div class=\"card mb-3\">\r\n                    <div class=\"card-header\">\r\n                        <i class=\"fa fa-area-chart\"></i>Your Views\r\n                    </div>\r\n                    <div class=\"card-body\">\r\n                        <canvas id=\"line\" class=\"chart chart-line\" chart-data=\"vm.chart.area.data\"\r\n                                chart-labels=\"vm.chart.area.labels\" chart-series=\"vm.chart.area.series\" chart-options=\"vm.chart.area.options\"\r\n                                chart-dataset-override=\"vm.chart.area.datasetOverride\"></canvas>\r\n                    </div>\r\n                    <div class=\"card-footer small text-muted\">Updated yesterday at 11:59 PM</div>\r\n                </div>\r\n\r\n            </div>\r\n            <div class=\"col-lg-4\">\r\n                <div class=\"card mb-3\">\r\n                    <div class=\"card-header\">\r\n                        <i class=\"fa fa-area-chart\"></i>Your Orders\r\n                    </div>\r\n                    <div class=\"card-body\">\r\n                        <canvas id=\"doughnut\" class=\"chart chart-doughnut\"\r\n                                chart-data=\"vm.chart.doughnut.data\" chart-labels=\"vm.chart.doughnut.labels\"></canvas>\r\n                    </div>\r\n                    <div class=\"card-footer small text-muted\">Updated yesterday at 11:59 PM</div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <!-- DataTables Card-->\r\n        <div class=\"card col-12 mb-3 p-0\">\r\n            <div class=\"card-header\">\r\n                <i class=\"fa fa-table\"></i> Customers\r\n            </div>\r\n            <div class=\"card-body\">\r\n                <div class=\"table-responsive\">\r\n                    <table class=\"table table-bordered\" id=\"dataTable\" cellspacing=\"0\" width=\"100%\">\r\n                        <thead>\r\n                            <tr>\r\n                                <th>Client Name</th>\r\n                                <th>Email</th>\r\n                                <th>Country</th>\r\n                                <th>Orders</th>\r\n                                <th>Deadline</th>\r\n                                <th>Price Order</th>\r\n                            </tr>\r\n                        </thead>\r\n                        <tbody>\r\n                            <tr ng-repeat=\"c in vm.customers track by c.id\">\r\n                                <td>{{c.clientName}}</td>\r\n                                <td>{{c.email}}</td>\r\n                                <td>{{c.country}}</td>\r\n                                <td>{{c.orders}}</td>\r\n                                <td>{{c.deadline}}</td>\r\n                                <td>{{c.price}}</td>\r\n                            </tr>\r\n                        </tbody>\r\n                    </table>\r\n                </div>\r\n            </div>\r\n            <div class=\"card-footer small text-muted\">Updated yesterday at 11:59 PM</div>\r\n        </div>\r\n    </div>\r\n    <!-- end container-fluid-->\r\n</div>");
$templateCache.put("app/navbar/navbar.html","<!-- Navigation-->\r\n<nav class=\"navbar navbar-expand-lg navbar-dark bg-dark fixed-top\" id=\"mainNav\" ng-if=\"vm.user\">\r\n    <a class=\"navbar-brand\" ui-sref=\'home\'>Welcome</a>\r\n    <button class=\"navbar-toggler navbar-toggler-right\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarResponsive\"\r\n        aria-controls=\"navbarResponsive\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">\r\n        <span class=\"navbar-toggler-icon\"></span>\r\n    </button>\r\n    <div class=\"collapse navbar-collapse\" id=\"navbarResponsive\">\r\n        <ul class=\"navbar-nav navbar-sidenav\" id=\"accordion\">\r\n            <li class=\"nav-item\" data-toggle=\"tooltip\" data-placement=\"right\" title=\"User\">\r\n                <div class=\"profile-userpic col-xl-12 col-md-12 col-sm-6\">\r\n                    <img ng-src=\"{{vm.userProfilePath}}\" class=\"img-responsive\" alt=\"profile-user\">\r\n                </div>\r\n                <div class=\"profile-usertitle col-xl-12 col-md-12 col-sm-6\">\r\n                    <div class=\"profile-usertitle-name\">{{vm.user.firstName}} {{vm.user.lastName}}</div>\r\n                    <div class=\"profile-usertitle-job\"> {{vm.user.occupation}} </div>\r\n                </div>\r\n            </li>\r\n            <li class=\"nav-item\" data-toggle=\"tooltip\" data-placement=\"right\" title=\"Home\">\r\n                <a class=\"nav-link\" ui-sref=\'home\'>\r\n                    <i class=\"fa fa-home\" aria-hidden=\"true\"></i>\r\n                    <span class=\"nav-link-text\">Home</span>\r\n                </a>\r\n            </li>\r\n            <li class=\"nav-item\" data-toggle=\"tooltip\" data-placement=\"right\" title=\"Buy\">\r\n                <a class=\"nav-link\" ui-sref=\'skills\'>\r\n                    <i class=\"fa fa-shopping-cart\" aria-hidden=\"true\"></i>\r\n                    <span class=\"nav-link-text\">Buy</span>\r\n                </a>\r\n            </li>\r\n            <li class=\"nav-item\" data-toggle=\"tooltip\" data-placement=\"right\" title=\"Portfolio\">\r\n                <a class=\"nav-link\" ui-sref=\'portfolio\'>\r\n                    <i class=\"fa fa-folder-open\" aria-hidden=\"true\"></i>\r\n                    <span class=\"nav-link-text\">Portfolio</span>\r\n                </a>\r\n            </li>\r\n            <li class=\"nav-item\" data-toggle=\"tooltip\" data-placement=\"right\" title=\"Friends\">\r\n                <a class=\"nav-link\" ui-sref=\'friends\'>\r\n                    <i class=\"fa fa-users\" aria-hidden=\"true\"></i>\r\n                    <span class=\"nav-link-text\">Friends</span>\r\n                </a>\r\n            </li>\r\n            <li class=\"nav-item\" data-toggle=\"tooltip\" data-placement=\"right\" title=\"Settings\">\r\n                <a class=\"nav-link nav-link-collapse collapsed\" data-toggle=\"collapse\" href=\"#collapseSettings\" data-parent=\"#accordion\"\r\n                    ng-click=\"vm.removeSidenavToggled($event)\">\r\n                    <i class=\"fa fa-cog\" aria-hidden=\"true\"></i>\r\n                    <span class=\"nav-link-text\">Settings</span>\r\n                </a>\r\n                <ul class=\"sidenav-second-level collapse\" id=\"collapseSettings\">\r\n                    <li>\r\n                        <a ui-sref=\'account-settings\'>Account Settings </a>\r\n                    </li>\r\n                    <li>\r\n                        <a ui-sref=\'portfolio-settings\'>Portfolio Settings</a>\r\n                    </li>\r\n                </ul>\r\n            </li>\r\n        </ul>\r\n        <!--toggle nav-->\r\n        <ul class=\"navbar-nav sidenav-toggler\">\r\n            <li class=\"nav-item\">\r\n                <a class=\"nav-link text-center\" id=\"sidenavToggler\" ng-click=\"vm.toggleNav()\">\r\n                    <i class=\"fa fa-fw fa-angle-left\"></i>\r\n                </a>\r\n            </li>\r\n        </ul>\r\n        <!--messages and alerts-->\r\n        <ul class=\"navbar-nav ml-auto\">\r\n            <li class=\"nav-item dropdown\" ng-class=\"{\'show\':vm.isMessage}\" ng-mouseleave=\"vm.isMessage =false\">\r\n                <a class=\"nav-link dropdown-toggle mr-lg-2\" id=\"messagesDropdown\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\"\r\n                    ng-click=\"vm.showDropdownData(1)\">\r\n                    <i class=\"fa fa-fw fa-envelope\"></i>\r\n                    <span class=\"d-lg-none\">\r\n                        Messages\r\n                        <span class=\"badge badge-pill badge-primary\">{{vm.messages.length}}</span>\r\n                    </span>\r\n                    <span class=\"indicator text-primary d-none d-lg-block\">\r\n                        <i class=\"fa fa-fw fa-circle\"></i>\r\n                    </span>\r\n                </a>\r\n                <div class=\"dropdown-menu\" aria-labelledby=\"messagesDropdown\" ng-class=\"{\'show\':vm.isMessage}\">\r\n                    <h6 class=\"dropdown-header\">New Messages:</h6>\r\n                    <div class=\"dropdown-divider\"></div>\r\n                    <a class=\"dropdown-item\" ng-repeat=\"message in vm.messages track by message.id\" ng-if=\"!vm.isDataLoading\">\r\n                        <strong>{{message.name}}</strong>\r\n                        <span class=\"small float-right text-muted\">{{message.date}}</span>\r\n                        <div class=\"dropdown-message small\">{{message.message}}</div>\r\n                    </a>\r\n                    <i class=\"fa fa-spinner\" aria-hidden=\"true\" ng-if=\"vm.isDataLoading\"></i>\r\n                    <div class=\"dropdown-divider\"></div>\r\n                    <a class=\"dropdown-item small\">View all messages</a>\r\n                </div>\r\n            </li>\r\n            <li class=\"nav-item dropdown\" ng-class=\"{\'show\':vm.isAlert}\" ng-mouseleave=\"vm.isAlert =false\">\r\n                <a class=\"nav-link dropdown-toggle mr-lg-2\" id=\"alertsDropdown\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\"\r\n                    ng-click=\"vm.showDropdownData(2)\">\r\n                    <i class=\"fa fa-fw fa-bell\"></i>\r\n                    <span class=\"d-lg-none\">\r\n                        Alerts\r\n                        <span class=\"badge badge-pill badge-warning\">{{vm.alerts.length}}</span>\r\n                    </span>\r\n                    <span class=\"indicator text-warning d-none d-lg-block\">\r\n                        <i class=\"fa fa-fw fa-circle\"></i>\r\n                    </span>\r\n                </a>\r\n                <div class=\"dropdown-menu\" aria-labelledby=\"alertsDropdown\" ng-class=\"{\'show\':vm.isAlert}\">\r\n                    <h6 class=\"dropdown-header\">New Alerts:</h6>\r\n                    <div class=\"dropdown-divider\"></div>\r\n                    <a class=\"dropdown-item\" ng-repeat=\"alert in vm.alerts track by alert.id\" ng-if=\"!vm.isDataLoading\">\r\n                        <span ng-class=\"{\'text-success\':alert.status===\'success\', \'text-danger\':alert.status===\'danger\'}\">\r\n                            <strong>\r\n                                <i class=\"fa  fa-fw\" ng-class=\"{\'fa-long-arrow-up\' :alert.status===\'success\',\'fa-long-arrow-down\' :alert.status===\'danger\'}\"></i>{{alert.name}}\r\n                            </strong>\r\n                        </span>\r\n                        <span class=\"small float-right text-muted\">{{alert.date}}</span>\r\n                        <div class=\"dropdown-message small\">{{alert.message}}</div>\r\n                    </a>\r\n                    <i class=\"fa fa-spinner\" aria-hidden=\"true\" ng-if=\"vm.isDataLoading\"></i>\r\n                    <div class=\"dropdown-divider\"></div>\r\n                    <a class=\"dropdown-item small\">View all alerts</a>\r\n                </div>\r\n            </li>\r\n            <!-- cart -->\r\n            <li class=\"nav-item\">\r\n                <a class=\"nav-link my-2 my-lg-0 mr-lg-2\" ui-sref=\"shopping-bag\">\r\n                    <i class=\"fa fa-shopping-cart\" aria-hidden=\"true\"></i>\r\n                </a>\r\n            </li>\r\n            <!--search-->\r\n            <li class=\"nav-item\">\r\n                <form class=\"form-inline my-2 my-lg-0 mr-lg-2\">\r\n                    <div class=\"input-group\">\r\n                        <input class=\"form-control\" type=\"text\" placeholder=\"Search for...\">\r\n                        <span class=\"input-group-btn\">\r\n                            <button class=\"btn btn-primary\" type=\"button\">\r\n                                <i class=\"fa fa-search\"></i>\r\n                            </button>\r\n                        </span>\r\n                    </div>\r\n                </form>\r\n            </li>\r\n            <!--logout-->\r\n            <li class=\"nav-item\">\r\n                <a class=\"nav-link\" data-toggle=\"modal\" data-target=\"#modal\" ng-click=\"vm.logOut()\">\r\n                    <i class=\"fa fa-fw fa-sign-out\"></i>Logout\r\n                </a>\r\n            </li>\r\n        </ul>\r\n    </div>\r\n</nav>");
$templateCache.put("app/portfolio/portfolio.tpl.html","<!-- container-fluid-->\r\n<div class=\"content-wrapper\">\r\n    <div class=\"container-fluid\">\r\n        <!-- Breadcrumbs-->\r\n        <ol class=\"breadcrumb\">\r\n            <li class=\"breadcrumb-item\">\r\n                <a href=\"#\">Portfolio</a>\r\n            </li>\r\n        </ol>\r\n        <!-- Filter Controls - Simple Mode -->\r\n        <div class=\"row\" ng-if=\"vm.imageCategories.length\">\r\n            <!-- A basic setup of simple mode filter controls, all you have to do is use data-filter=\"all\"\r\n        for an unfiltered gallery and then the values of your categories to filter between them -->\r\n            <ul class=\"simplefilter\">\r\n                <li class=\"active\" data-filter=\"all\" ng-click=\"vm.addItemClass($event)\">All</li>\r\n                <li data-filter=\"{{category.category}}\" ng-click=\"vm.addItemClass($event)\" ng-repeat=\"category in vm.imageCategories\" ng-init=\"category.category = $index;category.category=category.category+1; \">{{category.name}}</li>\r\n            </ul>\r\n        </div>\r\n\r\n        <!-- Search control -->\r\n        <div class=\"search-row\" ng-if=\"vm.portfolioData.length\">\r\n            <input type=\"text\" class=\"form-control filtr-search\" placeholder=\"Search...\" name=\"filtr-search\" data-search>\r\n        </div>\r\n        <div class=\"row\">\r\n            <!-- This is the set up of a basic gallery, your items must have the categories they belong to in a data-category\r\n            attribute, which starts from the value 1 and goes up from there -->\r\n            <div class=\"filtr-container\">\r\n                <div class=\"col-xs-6 col-sm-4 col-md-3 filtr-item\" data-category=\"{{data.category}}\" data-sort=\"{{data.portfolioName}}\" ng-repeat=\"data in vm.portfolioData\">\r\n\r\n                    <div class=\"hoverd-area\">\r\n                        <img ng-src=\"{{data.path}}\" class=\"img-resonsive\" />\r\n                        <div class=\"img-hover\">\r\n                            <span>\r\n                                <i class=\"fa fa-search-plus\" aria-hidden=\"true\" ng-click=\"vm.zoomImage(data.path)\"></i>\r\n                                <i class=\"fa fa-trash\" aria-hidden=\"true\" ng-click=\"vm.removeImage(data)\"></i>\r\n                            </span>\r\n                        </div>\r\n                    </div>\r\n                    <div class=\"info\">\r\n                        <div class=\"row\">\r\n                            <div class=\"price col-md-6\">\r\n                                <h5>{{data.portfolioName}}</h5>\r\n                                <h5 class=\"price-text-color\">${{data.price}}</h5>\r\n                            </div>\r\n                            <div class=\"rating hidden-sm col-md-6\">\r\n                                <i class=\"price-text-color fa fa-star\"></i>\r\n                                <i class=\"price-text-color fa fa-star\">\r\n                                </i>\r\n                                <i class=\"price-text-color fa fa-star\"></i>\r\n                                <i class=\"price-text-color fa fa-star\">\r\n                                </i>\r\n                                <i class=\"fa fa-star\"></i>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>");
$templateCache.put("app/portfolio-settings/portfolio-settings.tpl.html","<!-- container-fluid-->\r\n<div class=\"content-wrapper\">\r\n    <div class=\"container-fluid\">\r\n        <!-- Breadcrumbs-->\r\n        <ol class=\"breadcrumb\">\r\n            <li class=\"breadcrumb-item\">\r\n                <a href=\"#\">Upload images for your portfolio</a>\r\n            </li>\r\n        </ol>\r\n\r\n        <div class=\"col-xl-12 col-sm-12 mb-3\">\r\n            <div ng-form=\"portfolioForm\">\r\n                <div class=\"form-group\">\r\n                    <div class=\"form-row\">\r\n                        <div class=\"col-md-9\" ng-if=\"vm.portfolio.length\">\r\n                            <select ng-model=\"vm.portfolioSelected\" ng-options=\"item.id as item.name for item in vm.portfolio\" class=\"form-control\" ng-required=\"!isCreated\">\r\n                                <option value=\"\"></option>\r\n                            </select>\r\n                        </div>\r\n                        <div class=\"col-md-3\">\r\n                            <button class=\"btn btn-block btn-success\" ng-click=\"isCreated = !isCreated\" ng-disabled=\"vm.portfolioSelected?true:false\">Create a new one</button>\r\n                        </div>\r\n                        <div class=\"col-md-12\">\r\n                            <div class=\'text-left\' ng-if=\"isCreated\" ng-class=\"{\'has-error\' : portfolioForm.portfolioName.$invalid && portfolioForm.portfolioName.$touched}\">\r\n                                <label for=\"portfolioName\">Portfolio name</label>\r\n                                <input type=\"text\" class=\'form-control\' name=\"portfolioName\" ng-model=\"vm.selected\" ng-change=\"vm.portfolioSelected = vm.selected\"\r\n                                    required/>\r\n                                <div ng-messages=\"portfolioForm.portfolioName.$error\" ng-if=\"portfolioForm.portfolioName.$invalid && portfolioForm.portfolioName.$touched\">\r\n                                    <span ng-message=\"required\" class=\"help-block\">Portfolio name is required</span>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n\r\n                    </div>\r\n                </div>\r\n\r\n                <div class=\"form-group\">\r\n                    <div class=\"form-row\">\r\n                        <div class=\"col-md-12\">\r\n                            <div class=\'text-left\' ng-class=\"{\'has-error\' : portfolioForm.file.$invalid && portfolioForm.file.$touched}\">\r\n                                <input type=\"file\" ngf-select=\"vm.convert(picFiles)\" ng-model=\"picFiles\" multiple name=\"file\" ngf-accept=\"\'image/*\'\" ngf-model-invalid=\"vm.errorFile\"\r\n                                    required>\r\n\r\n                                <div class=\"col-sm-12 col-md-12\" ng-show=\"portfolioForm.file.$valid\">\r\n                                    <ul class=\"list-unstyled\">\r\n                                        <li ng-repeat=\"f in picFiles\" ng-init=\"f.result=\'\'\">\r\n                                            <div class=\"row \" ng-if=\"f.path;\">\r\n                                                <div class=\"col-sm-6 col-md-6 crop-area \">\r\n                                                    <img-crop image=\"f.path \" result-image=\"f.result \" area-type=\"square \"></img-crop>\r\n                                                    {{f.result}}\r\n                                                </div>\r\n                                                <div ng-show=\"portfolioForm.file.$valid \" class=\"col-sm-3 col-md-3 crop-result \">\r\n                                                    <h5>Cropped Image:</h5>\r\n                                                    <img ng-src=\"{{f.result}} \" />\r\n                                                </div>\r\n                                                <div ng-show=\"portfolioForm.file.$valid \" class=\"col-sm-3 col-md-3 crop-result \">\r\n                                                    <h5>Image Details:</h5>\r\n                                                    <labe for=\"price\">Price</labe>\r\n                                                    <input type=\"number\" name=\"price\" ng-model=\"f.price\" aria-label=\"price\" class=\"form-control\">\r\n                                                    <span id=\"currency-default\">{{f.price | currency}}</span>\r\n                                                </div>\r\n                                                <button ng-click=\"f.path=null \" ng-show=\"f \" class=\"btn btn-success \">Remove</button>\r\n                                            </div>\r\n                                            <span ng-if=\"!f.path \">\r\n                                                <i class=\"fa fa-spinner \" aria-hidden=\"true \"></i>\r\n                                            </span>\r\n                                        </li>\r\n                                    </ul>\r\n\r\n                                </div>\r\n                            </div>\r\n                            <div ng-messages=\"portfolioForm.file.$error \" ng-if=\"portfolioForm.file.$invalid && portfolioForm.file.$touched \">\r\n                                <span ng-message=\"required \" class=\"help-block \">The images are required</span>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n\r\n                <a class=\"btn btn-primary btn-block \" ng-class=\"{ \'disabled\':portfolioForm.$invalid} \" ng-click=\"vm.uploadPortfolioImages(portfolioForm,picFiles,\r\n                                            vm.portfolioSelected) \" ng-disabled=\"portfolioForm.$invalid \" href>Save images</a>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>");
$templateCache.put("app/shopping-bag/shopping-bag.tpl.html","<!-- container-fluid-->\r\n<div class=\"content-wrapper\">\r\n    <div class=\"container-fluid\">\r\n        <!-- Breadcrumbs-->\r\n        <ol class=\"breadcrumb\">\r\n            <li class=\"breadcrumb-item\">\r\n                <a href=\"#\">Shopping cart</a>\r\n            </li>\r\n        </ol>\r\n        <div class=\"container\">\r\n            <div class=\"stepwizard col-md-offset-3\">\r\n                <div class=\"stepwizard-row setup-panel\">\r\n                    <div class=\"stepwizard-step\">\r\n                        <a href=\"#step-1\" class=\"btn btn-success btn-circle\">\r\n                            <i class=\"fa fa-circle-o\" aria-hidden=\"true\"></i>\r\n                        </a>\r\n                        <p>My Cart</p>\r\n                    </div>\r\n                    <div class=\"stepwizard-step\">\r\n                        <a href=\"#step-2\" class=\"btn btn-primary btn-circle\" disabled=\"disabled\">\r\n                            <i class=\"fa fa-circle-o\" aria-hidden=\"true\"></i>\r\n                        </a>\r\n                        <p>Customer Information</p>\r\n                    </div>\r\n                    <div class=\"stepwizard-step\">\r\n                        <a href=\"#step-3\" class=\"btn btn-primary btn-circle\" disabled=\"disabled\">\r\n                            <i class=\"fa fa-circle-o\" aria-hidden=\"true\"></i>\r\n                        </a>\r\n                        <p>Shipping & Payment Method</p>\r\n                    </div>\r\n                    <div class=\"stepwizard-step\">\r\n                        <a href=\"#step-4\" class=\"btn btn-primary btn-circle\" disabled=\"disabled\">\r\n                            <i class=\"fa fa-circle-o\" aria-hidden=\"true\"></i>\r\n                        </a>\r\n                        <p>Order Review</p>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n\r\n            <form role=\"form\" action=\"\" method=\"post\">\r\n                <div class=\"row setup-content active\" id=\"step-1\">\r\n                    <div class=\"col-md-12 col-md-offset-3\">\r\n                        <div class=\"col-md-12\">\r\n                            <table class=\"table table-responsive table-striped\">\r\n                                <thead>\r\n                                    <tr>\r\n                                        <th>Quantity</th>\r\n                                        <th>UserID</th>\r\n                                        <th>Product </th>\r\n                                        <th>Price</th>\r\n                                        <th>Remove</th>\r\n                                    </tr>\r\n                                </thead>\r\n                                <tbody ng-repeat=\"product in vm.products\">\r\n                                    <tr>\r\n                                        <td>\r\n                                            <input type=\"number\" name=\"quantity\" class=\"form-control col-3\" min=\"1\" />\r\n                                        </td>\r\n                                        <td>\r\n                                            {{product.userId}}\r\n                                        </td>\r\n                                        <td>\r\n                                            <span>\r\n                                                <img ng-src=\"{{product.item.path}}\" alt=\"product-image \">\r\n                                            </span>\r\n                                            {{product.item.portfolioName}}</td>\r\n                                        <td> ${{product.item.price}}</td>\r\n                                        <td>\r\n                                            <button type=\"button\" name=\"remove\" class=\"btn btn-danger\">\r\n                                                <i class=\"fa fa-times \" aria-hidden=\"true\"></i>\r\n                                            </button>\r\n                                        </td>\r\n                                    </tr>\r\n                                </tbody>\r\n                            </table>\r\n                            <button class=\"btn btn-primary nextBtn btn-lg pull-right \" type=\"button\" ng-click=\"vm.nextStep()\">Next</button>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <div class=\"row setup-content \" id=\"step-2 \">\r\n                    <div class=\"col-xs-6 col-md-offset-3 \">\r\n                        <div class=\"col-md-12 \">\r\n                            <h3> Step 2</h3>\r\n                            <div class=\"form-group \">\r\n                                <label class=\"control-label \">Company Name</label>\r\n                                <input maxlength=\"200 \" type=\"text \" required=\"required \" class=\"form-control\r\n                                                \" placeholder=\"Enter Company Name \" />\r\n                            </div>\r\n                            <div class=\"form-group \">\r\n                                <label class=\"control-label \">Company Address</label>\r\n                                <input maxlength=\"200 \" type=\"text \" required=\"required \" class=\"form-control\r\n                                                \" placeholder=\"Enter Company Address \" />\r\n                            </div>\r\n                            <button class=\"btn btn-primary nextBtn btn-lg pull-right \" type=\"button \">Next</button>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <div class=\"row setup-content \" id=\"step-3 \">\r\n                    <div class=\"col-xs-6 col-md-offset-3 \">\r\n                        <div class=\"col-md-12 \">\r\n                            <h3> Step 3</h3>\r\n                            <button class=\"btn btn-success btn-lg pull-right \" type=\"submit \">Submit</button>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <div class=\"row setup-content \" id=\"step-4 \">\r\n                    <div class=\"col-xs-6 col-md-offset-3 \">\r\n                        <div class=\"col-md-12 \">\r\n                            <h3> Step 4</h3>\r\n                            <button class=\"btn btn-success btn-lg pull-right \" type=\"submit \">Submit</button>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </form>\r\n\r\n        </div>\r\n    </div>\r\n</div>");
$templateCache.put("app/skills/skills.tpl.html","<!-- container-fluid-->\r\n<div class=\"content-wrapper\">\r\n    <div class=\"container-fluid\">\r\n        <!-- Breadcrumbs-->\r\n        <ol class=\"breadcrumb\">\r\n            <li class=\"breadcrumb-item\">\r\n                <a href=\"#\">Skills</a>\r\n            </li>\r\n        </ol>\r\n        <!-- Skills Chart -->\r\n        <div class=\"card col-xl-12 col-md-12 col-sm-12 mb-3 p-0\">\r\n            <div class=\"card-header\">\r\n                <i class=\"fa fa-area-chart\"></i>My Skills\r\n            </div>\r\n            <div class=\"card-body\">\r\n                <canvas id=\"base\" class=\"chart-horizontal-bar\"\r\n                        chart-data=\"vm.chart.horizontal.data\" chart-labels=\"vm.chart.horizontal.labels\"></canvas>\r\n            </div>\r\n            <div class=\"card-footer small text-muted\">Updated yesterday at 11:59 PM</div>\r\n        </div>\r\n        <!--Developer skills-->\r\n        <developer-skills dev-skills=\"vm.devSkills\"></developer-skills>\r\n    </div>\r\n</div>");
$templateCache.put("app/shared/component/developer-skills.tpl.html","<div class=\"card col-xl-12 col-md-12 col-sm-12 mb-3 p-0\">\r\n    <div class=\"card-header\">\r\n        <i class=\"fa fa-area-chart\"></i>Developer Skills\r\n    </div>\r\n    <div class=\"card-body\">\r\n        <div class=\"progress\" ng-repeat=\"skill in $ctrl.devSkills\">\r\n           <div class=\"progress-bar bg-success\" role=\"progressbar\" style=\"width: {{skill.value}}%\" aria-valuenow=\"{{skill.value}}\" aria-valuemin=\"0\" aria-valuemax=\"100\">{{skill.name}}</div>\r\n        </div>\r\n    </div>\r\n    <div class=\"card-footer small text-muted\">Updated yesterday at 11:59 PM</div>\r\n</div>");
$templateCache.put("app/shared/modal/image-modal.tpl.html","<!-- Logout Modal-->\r\n<img class=\"img-responsive\" ng-src=\"{{vm.entityName}}\">");
$templateCache.put("app/shared/modal/modal.tpl.html","<!-- Logout Modal-->\r\n<div class=\"modal-header\">\r\n    <h3 class=\"modal-title\">\r\n        Do you want to {{vm.entityName}} ?\r\n    </h3>\r\n    <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\" ng-click=\"vm.cancel()\">\r\n        <span aria-hidden=\"true\">&times;</span>\r\n    </button>\r\n</div>\r\n<div class=\"modal-footer\">\r\n    <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\" ng-click=\"vm.cancel()\">No</button>\r\n    <button type=\"button\" class=\"btn btn-primary\" ng-click=\"vm.ok()\">Ok</button>\r\n</div>");}]);
/// <reference path="./userProfile.d.ts" />
var UserProfile;
(function (UserProfile) {
    "use strict";
    UserProfile.app = angular.module("userProfile", [
        "ui.router",
        "ngSanitize",
        "ngAnimate",
        "ngMessages",
        "ngCookies",
        "ui.bootstrap",
        "ngFileUpload",
        "chart.js",
        "ngImgCrop",
        "userprofile-templates"
    ]);
    UserProfile.app.config([
        "$stateProvider", "$urlRouterProvider", "$httpProvider", "$locationProvider",
        function ($stateProvider, $urlRouterProvider, $httpProvider, $locationProvider) {
            $stateProvider.state("home", {
                url: "/",
                reloadOnSearch: false,
                templateUrl: "app/main-view/main-view.html",
                controller: "MainCtrl",
                controllerAs: "vm",
                data: { requiresLogin: true }
            })
                .state("login", {
                url: "/login",
                templateUrl: "app/login/login.html",
                controller: "LoginCtrl",
                controllerAs: "vm"
            }).state("register", {
                url: "/register",
                controller: "RegisterCtrl",
                controllerAs: "vm",
                templateUrl: "app/login/register.html"
            }).state("forgotPassword", {
                url: "/forgot-password",
                controller: "ForgotPasswordCtrl",
                controllerAs: "vm",
                templateUrl: "app/login/forgot-password.html"
            }).state("reset", {
                url: "/reset/:id",
                templateUrl: "app/login/reset.html",
                controller: "ResetCtrl",
                controllerAs: "vm"
            }).state("portfolio", {
                url: "/portfolio",
                reloadOnSearch: false,
                templateUrl: "app/portfolio/portfolio.tpl.html",
                controller: "PortfolioCtrl",
                controllerAs: "vm",
                data: { requiresLogin: true }
            }).state("friends", {
                url: "/friends",
                reloadOnSearch: false,
                templateUrl: "app/friends/friends.tpl.html",
                controller: "FriendsCtrl",
                controllerAs: "vm",
                data: { requiresLogin: true }
            }).state("person", {
                url: "/people/{personId}",
                reloadOnSearch: false,
                templateUrl: "app/friend-details/friend-details.tpl.html",
                controller: "FriendDetailsCtrl",
                controllerAs: "vm",
                data: { requiresLogin: true }
            }).state("skills", {
                url: "/skills",
                reloadOnSearch: false,
                templateUrl: "app/skills/skills.tpl.html",
                controller: "SkillsCtrl",
                controllerAs: "vm",
                data: { requiresLogin: true }
            }).state("account-settings", {
                url: "/account-settings",
                reloadOnSearch: false,
                templateUrl: "app/account-settings/account-settings.tpl.html",
                controller: "AccountSettingsCtrl",
                controllerAs: "vm",
                data: { requiresLogin: true }
            }).state("portfolio-settings", {
                url: "/portfolio-settings",
                reloadOnSearch: false,
                templateUrl: "app/portfolio-settings/portfolio-settings.tpl.html",
                controller: "PortfolioSettingsCtrl",
                controllerAs: "vm",
                data: { requiresLogin: true }
            }).state("shopping-bag", {
                url: "/shopping-bag",
                reloadOnSearch: false,
                templateUrl: "app/shopping-bag/shopping-bag.tpl.html",
                controller: "ShoppingBagCtrl",
                controllerAs: "vm",
                data: { requiresLogin: true }
            });
            $urlRouterProvider.otherwise("/");
            $httpProvider.interceptors.push(["$q", "$location", function ($q, $location) {
                    return {
                        "request": function (config) {
                            config.headers = config.headers || {};
                            if (localStorage.token) {
                                config.headers.Authorization = "Bearer " + localStorage.token;
                            }
                            return config;
                        },
                        "responseError": function (response) {
                            console.log(response.status);
                            if (response.status === 401 || response.status === 403) {
                                localStorage.removeItem("isAuthenticated");
                                $location.path("/login");
                            }
                            return $q.reject(response);
                        }
                    };
                }]);
        }
    ]).run([
        "$rootScope", "$state", "$location", "AuthService",
        function ($rootScope, $state, $location, authService) {
            stateChanged($rootScope, $state, $location);
        }
    ]);
    function stateChanged($rootScope, $state, $location) {
        $rootScope.$on("$stateChangeStart", function (event, toState, toParams, fromState) {
            if (JSON.parse(localStorage.getItem("isAuthenticated")) && (toState.name === "login" || toState.name === "register" || toState.name === "forgotPassword" || toState.name === "reset")) {
                event.preventDefault();
                $location.path("/");
            }
        });
    }
})(UserProfile || (UserProfile = {}));
var UserProfile;
(function (UserProfile) {
    "use strict";
    var AccountSettingsCtrl = /** @class */ (function () {
        function AccountSettingsCtrl($scope, userProfileData, authService, $location) {
            this.$scope = $scope;
            this.userProfileData = userProfileData;
            this.authService = authService;
            this.$location = $location;
            this.alerts = [];
        }
        AccountSettingsCtrl.prototype.$onInit = function () {
            this.userId = localStorage.getItem("userId");
            this.user = this.userProfileData.getUserDetails();
        };
        AccountSettingsCtrl.prototype.checkPassword = function (password, confirmPassword, form) {
            this.form = form;
            if (password === confirmPassword) {
                this.form.confirmPassword.$error.validationError = false;
                return true;
            }
            else {
                this.form.confirmPassword.$error.validationError = true;
                return false;
            }
        };
        AccountSettingsCtrl.prototype.submitForm = function (form, file) {
            this.form = form;
            if (form.$valid) {
                this.saveChanges(file);
            }
            angular.forEach(form.$error, function (field) {
                angular.forEach(field, function (errorField) {
                    errorField.$setTouched();
                });
            });
        };
        AccountSettingsCtrl.prototype.saveChanges = function (file) {
            var _this = this;
            if (file != null) {
                this.userProfileData.saveChanges(file, this.user).then(function () {
                    _this.$location.path("/");
                })["catch"](function (err) {
                    _this.alerts.push({ type: "danger", message: err.data.message });
                });
            }
        };
        AccountSettingsCtrl.$inject = [
            "$scope",
            "UserProfileData",
            "AuthService",
            "$location"
        ];
        return AccountSettingsCtrl;
    }());
    UserProfile.AccountSettingsCtrl = AccountSettingsCtrl;
    UserProfile.app.controller("AccountSettingsCtrl", AccountSettingsCtrl);
})(UserProfile || (UserProfile = {}));
var UserProfile;
(function (UserProfile) {
    "use strict";
    var FooterCtrl = /** @class */ (function () {
        function FooterCtrl($scope) {
            var _this = this;
            this.$scope = $scope;
            this.$scope.$watch(function () { return localStorage.getItem("isAuthenticated"); }, function (newVal, oldVal) {
                _this.initData(newVal);
            });
        }
        FooterCtrl.prototype.$onInit = function () {
        };
        FooterCtrl.prototype.initData = function (isAuthenticated) {
            if (JSON.parse(isAuthenticated) === true) {
                this.year = new Date().getFullYear();
            }
            else {
                this.year = null;
            }
        };
        FooterCtrl.$inject = [
            "$scope",
        ];
        return FooterCtrl;
    }());
    UserProfile.FooterCtrl = FooterCtrl;
    UserProfile.app.controller("FooterCtrl", FooterCtrl);
})(UserProfile || (UserProfile = {}));
var UserProfile;
(function (UserProfile) {
    "use strict";
    var AddToCart = /** @class */ (function () {
        function AddToCart() {
            this.template = "";
            this.controller = "ShoppingBagCtrl";
            this.controllerAs = "vm";
            this.bindings = {
                item: "=",
                userId: "="
            };
        }
        return AddToCart;
    }());
    UserProfile.AddToCart = AddToCart;
    UserProfile.app.component("addToCart", new AddToCart());
})(UserProfile || (UserProfile = {}));
var UserProfile;
(function (UserProfile) {
    "use strict";
    var FriendDetailsCtrl = /** @class */ (function () {
        function FriendDetailsCtrl($scope, $element, userProfileData, $q, $uibModal, $state, $rootScope) {
            this.$scope = $scope;
            this.$element = $element;
            this.userProfileData = userProfileData;
            this.$q = $q;
            this.$uibModal = $uibModal;
            this.$state = $state;
            this.$rootScope = $rootScope;
        }
        FriendDetailsCtrl.prototype.$onInit = function () {
            var _this = this;
            this.userId = this.$state.params.personId;
            this.userProfileData.getUserData(this.userId).then(function (data) {
                _this.user = data;
                var count = 1;
                _this.getPortfolioImages(_this.userId).then(function (data) {
                    _this.portfolioData = _.flatMap(data, function (value) {
                        value.category = count;
                        var x = [];
                        _.forEach(value.files, function (file) {
                            x.push({
                                _id: value._id,
                                imageId: file._id,
                                path: file.path,
                                category: value.category,
                                portfolioName: value.portfolioName,
                                price: file.price,
                                createdAt: moment.utc(file.createdAt).format("Y/m/d")
                            });
                        });
                        count++;
                        return x;
                    });
                    if (_this.imageCategories.length) {
                        setTimeout(function () {
                            _this.$element.find(".filtr-container").filterizr();
                        }, 200);
                    }
                });
            });
        };
        FriendDetailsCtrl.prototype.getPortfolioImages = function (userId) {
            var _this = this;
            var deferred = this.$q.defer();
            var promise = this.userProfileData.getPortfolioList(userId).then(function (data) {
                _this.imageCategories = data;
                var images = _.map(data, function (category) {
                    return _this.userProfileData.getImages(userId, category.id);
                });
                _this.$q.all(images).then(function (data) {
                    deferred.resolve(data);
                });
            });
            return deferred.promise;
        };
        FriendDetailsCtrl.prototype.zoomImage = function (img) {
            var open = this.$uibModal.open({
                animation: true,
                templateUrl: "app/shared/modal/image-modal.tpl.html",
                windowClass: "show",
                backdropClass: "show",
                controller: "ModalController",
                controllerAs: "vm",
                size: "lg",
                resolve: {
                    entityName: function () {
                        return img;
                    }
                }
            });
            open.result.then(function (success) {
                if (success) {
                }
            });
        };
        FriendDetailsCtrl.prototype.addItemClass = function ($event) {
            _.forEach(this.$element.find(".simplefilter").children(), function (item) {
                angular.element(item).removeClass("active");
            });
            angular.element($event.currentTarget).addClass("active");
        };
        FriendDetailsCtrl.prototype.addToCart = function (item) {
            console.log(item, this.userId);
            this.userProfileData.addedItem({ item: item, userId: this.userId });
        };
        FriendDetailsCtrl.$inject = [
            "$scope",
            "$element",
            "UserProfileData",
            "$q",
            "$uibModal",
            "$state",
            "$rootScope"
        ];
        return FriendDetailsCtrl;
    }());
    UserProfile.FriendDetailsCtrl = FriendDetailsCtrl;
    UserProfile.app.controller("FriendDetailsCtrl", FriendDetailsCtrl);
})(UserProfile || (UserProfile = {}));
var UserProfile;
(function (UserProfile) {
    "use strict";
    var FriendsCtrl = /** @class */ (function () {
        function FriendsCtrl($scope, userProfileData, $uibModal, $state) {
            this.$scope = $scope;
            this.userProfileData = userProfileData;
            this.$uibModal = $uibModal;
            this.$state = $state;
            this.friends = [];
            this.addedFriends = [];
        }
        FriendsCtrl.prototype.$onInit = function () {
            var _this = this;
            this.userId = localStorage.getItem("userId");
            this.userProfileData.getFriends().then(function (data) {
                _this.friends = data;
                _this.getAddedFriendsList();
                console.log(_this.friends);
            });
        };
        FriendsCtrl.prototype.onSelect = function ($item, $model, $label) {
            $item.fullName = $item.firstName + " " + $item.lastName;
        };
        FriendsCtrl.prototype.addFriend = function (friend) {
            var _this = this;
            friend.status = true;
            if (this.addedFriends) {
                this.updateFriendList(friend);
            }
            else {
                this.userProfileData.addFriend(this.userId, friend).then(function (data) {
                    _this.getAddedFriendsList();
                });
            }
        };
        FriendsCtrl.prototype.getAddedFriendsList = function () {
            var _this = this;
            this.userProfileData.getAddedFriendsList(this.userId).then(function (data) {
                _this.addedFriends = data;
                _this.friends = _.filter(_this.friends, function (friend) {
                    var foundFriend = _.find(_this.addedFriends, function (fr) {
                        return fr._id === friend._id;
                    });
                    if (foundFriend) {
                        console.log(foundFriend);
                        return foundFriend._id !== _this.userId && foundFriend._id !== friend._id;
                    }
                    else {
                        return friend._id !== _this.userId;
                    }
                });
                console.log(_this.friends);
            })["catch"](function () {
                _this.addedFriends = [];
                _this.friends = _.filter(_this.friends, function (friend) {
                    return friend._id !== _this.userId;
                });
            });
        };
        FriendsCtrl.prototype.updateFriendList = function (friend) {
            var _this = this;
            this.userProfileData.updateFriendsList(this.userId, friend).then(function (data) {
                _this.getAddedFriendsList();
            });
        };
        FriendsCtrl.prototype.removeFriend = function (friend) {
            var _this = this;
            var open = this.$uibModal.open({
                animation: true,
                templateUrl: "app/shared/modal/modal.tpl.html",
                windowClass: "show",
                backdropClass: "show",
                controller: "ModalController",
                controllerAs: "vm",
                resolve: {
                    entityName: function () {
                        return "delete" + " " + friend.firstName + " " + friend.lastName;
                    }
                }
            });
            open.result.then(function (success) {
                if (success) {
                    _this.friends.push(friend);
                    _this.userProfileData.removeFriend(_this.userId, friend._id).then(function (data) {
                        _this.getAddedFriendsList();
                    });
                }
            });
        };
        FriendsCtrl.$inject = [
            "$scope",
            "UserProfileData",
            "$uibModal",
            "$state"
        ];
        return FriendsCtrl;
    }());
    UserProfile.FriendsCtrl = FriendsCtrl;
    UserProfile.app.controller("FriendsCtrl", FriendsCtrl);
})(UserProfile || (UserProfile = {}));
var UserProfile;
(function (UserProfile) {
    "use strict";
    var ForgotPasswordCtrl = /** @class */ (function () {
        function ForgotPasswordCtrl($scope, authService, $timeout, $location) {
            this.$scope = $scope;
            this.authService = authService;
            this.$timeout = $timeout;
            this.$location = $location;
            this.user = new UserProfile.User();
            this.alerts = [];
        }
        ForgotPasswordCtrl.prototype.$onInit = function () { };
        ForgotPasswordCtrl.prototype.submitResetForm = function (form) {
            this.formRegister = form;
            if (form.$valid) {
                this.forgotPassword();
            }
            angular.forEach(form.$error, function (field) {
                angular.forEach(field, function (errorField) {
                    errorField.$setTouched();
                });
            });
        };
        ForgotPasswordCtrl.prototype.forgotPassword = function () {
            var _this = this;
            this.authService.forgotPassword(this.user).then(function (result) {
                _this.alerts.push({ type: "success", message: result.message });
            })["catch"](function (err) {
                _this.alerts.push({ type: "danger", message: err.data.message });
            });
        };
        ForgotPasswordCtrl.$inject = [
            "$scope",
            "AuthService",
            "$timeout",
            "$location"
        ];
        return ForgotPasswordCtrl;
    }());
    UserProfile.ForgotPasswordCtrl = ForgotPasswordCtrl;
    UserProfile.app.controller("ForgotPasswordCtrl", ForgotPasswordCtrl);
})(UserProfile || (UserProfile = {}));
var UserProfile;
(function (UserProfile) {
    "use strict";
    var LoginCtrl = /** @class */ (function () {
        function LoginCtrl($scope, authService, $location, userProfileData) {
            this.$scope = $scope;
            this.authService = authService;
            this.$location = $location;
            this.userProfileData = userProfileData;
            this.alerts = [];
        }
        LoginCtrl.prototype.$onInit = function () {
        };
        LoginCtrl.prototype.submitLoginForm = function (form) {
            this.form = form;
            if (form.$valid) {
                this.setToken();
            }
            angular.forEach(form.$error, function (field) {
                angular.forEach(field, function (errorField) {
                    errorField.$setTouched();
                });
            });
        };
        LoginCtrl.prototype.setToken = function () {
            var _this = this;
            this.authService.login(this.user).then(function (data) {
                localStorage.setItem("token", data.token);
                localStorage.setItem("userId", data.userId);
                localStorage.setItem("isAuthenticated", _.toString(data.isAuthenticated));
                _this.authService.isLoggedIn = data.isAuthenticated;
                console.log(_this.authService.isLoggedIn);
                if (_this.authService.isLoggedIn) {
                    _this.$location.path("/");
                }
            })["catch"](function (err) {
                _this.alerts.push({ type: "danger", message: err.data.message });
            });
        };
        LoginCtrl.$inject = [
            "$scope",
            "AuthService",
            "$location",
            "UserProfileData"
        ];
        return LoginCtrl;
    }());
    UserProfile.LoginCtrl = LoginCtrl;
    UserProfile.app.controller("LoginCtrl", LoginCtrl);
})(UserProfile || (UserProfile = {}));
var UserProfile;
(function (UserProfile) {
    "use strict";
    var RegisterCtrl = /** @class */ (function () {
        function RegisterCtrl($scope, authService, $timeout, $location) {
            this.$scope = $scope;
            this.authService = authService;
            this.$timeout = $timeout;
            this.$location = $location;
            this.user = new UserProfile.User();
            this.alerts = [];
        }
        RegisterCtrl.prototype.$onInit = function () { };
        RegisterCtrl.prototype.checkPassword = function (password, confirmPassword, form) {
            this.formRegister = form;
            if (password === confirmPassword) {
                this.formRegister.confirmPassword.$error.validationError = false;
                return true;
            }
            else {
                this.formRegister.confirmPassword.$error.validationError = true;
                return false;
            }
        };
        RegisterCtrl.prototype.submitRegisterForm = function (form, file) {
            this.formRegister = form;
            if (form.$valid) {
                this.registerUser(file);
            }
            angular.forEach(form.$error, function (field) {
                angular.forEach(field, function (errorField) {
                    errorField.$setTouched();
                });
            });
        };
        RegisterCtrl.prototype.registerUser = function (file) {
            if (file != null) {
                this.upload(file);
            }
        };
        RegisterCtrl.prototype.upload = function (file) {
            var _this = this;
            this.authService.registerUser(file, this.user).then(function (result) {
                _this.$location.path("/login");
            })["catch"](function (err) {
                _this.alerts.push({ type: "danger", message: err.data.message });
            });
        };
        RegisterCtrl.$inject = [
            "$scope",
            "AuthService",
            "$timeout",
            "$location"
        ];
        return RegisterCtrl;
    }());
    UserProfile.RegisterCtrl = RegisterCtrl;
    UserProfile.app.controller("RegisterCtrl", RegisterCtrl);
})(UserProfile || (UserProfile = {}));
var UserProfile;
(function (UserProfile) {
    "use strict";
    var ResetCtrl = /** @class */ (function () {
        function ResetCtrl($scope, authService, $stateParams, $location) {
            this.$scope = $scope;
            this.authService = authService;
            this.$stateParams = $stateParams;
            this.$location = $location;
            this.user = new UserProfile.User();
            this.alerts = [];
        }
        ResetCtrl.prototype.$onInit = function () { };
        ResetCtrl.prototype.checkPassword = function (password, confirmPassword, form) {
            this.resetForm = form;
            if (password === confirmPassword) {
                this.resetForm.confirmPassword.$error.validationError = false;
                return true;
            }
            else {
                this.resetForm.confirmPassword.$error.validationError = true;
                return false;
            }
        };
        ResetCtrl.prototype.submitResetForm = function (form) {
            this.resetForm = form;
            if (form.$valid) {
                this.resetPassword();
            }
            angular.forEach(form.$error, function (field) {
                angular.forEach(field, function (errorField) {
                    errorField.$setTouched();
                });
            });
        };
        ResetCtrl.prototype.resetPassword = function () {
            var _this = this;
            this.authService.resetPassword(this.$stateParams.id, { newPassword: this.user.password, confirmPassword: this.user.confirmPassword }).then(function (result) {
                _this.$location.path("/login");
            })["catch"](function (err) {
                _this.alerts.push({ type: "danger", message: err.data.message });
            });
        };
        ResetCtrl.$inject = [
            "$scope",
            "AuthService",
            "$stateParams",
            "$location"
        ];
        return ResetCtrl;
    }());
    UserProfile.ResetCtrl = ResetCtrl;
    UserProfile.app.controller("ResetCtrl", ResetCtrl);
})(UserProfile || (UserProfile = {}));
var UserProfile;
(function (UserProfile) {
    "use strict";
    var MainCtrl = /** @class */ (function () {
        function MainCtrl($scope, userProfileData, $element) {
            this.$scope = $scope;
            this.userProfileData = userProfileData;
            this.$element = $element;
        }
        MainCtrl.prototype.$onInit = function () {
            var _this = this;
            this.userProfileData.getMessages().then(function (resp) {
                _this.messages = resp;
            });
            this.userProfileData.getAlerts().then(function (resp) {
                _this.alerts = resp;
            });
            this.userProfileData.getChartData().then(function (resp) {
                _this.chart = resp;
            });
            this.userProfileData.getCustomers().then(function (resp) {
                _this.customers = resp;
                _this.$element.find("#dataTable").DataTable();
            });
        };
        MainCtrl.$inject = [
            "$scope",
            "UserProfileData",
            "$element"
        ];
        return MainCtrl;
    }());
    UserProfile.MainCtrl = MainCtrl;
    UserProfile.app.controller("MainCtrl", MainCtrl);
})(UserProfile || (UserProfile = {}));
var UserProfile;
(function (UserProfile) {
    "use strict";
    var User = /** @class */ (function () {
        function User() {
            this.file = undefined;
        }
        return User;
    }());
    UserProfile.User = User;
})(UserProfile || (UserProfile = {}));
var UserProfile;
(function (UserProfile) {
    "use strict";
    var DataType;
    (function (DataType) {
        DataType[DataType["Message"] = 1] = "Message";
        DataType[DataType["Alert"] = 2] = "Alert";
    })(DataType = UserProfile.DataType || (UserProfile.DataType = {}));
    var NavbarCtrl = /** @class */ (function () {
        function NavbarCtrl($scope, $uibModal, $location, authService, $element, userProfileData) {
            var _this = this;
            this.$scope = $scope;
            this.$uibModal = $uibModal;
            this.$location = $location;
            this.authService = authService;
            this.$element = $element;
            this.userProfileData = userProfileData;
            this.isMessage = false;
            this.isAlert = false;
            this.isExpand = true;
            this.isDataLoading = false;
            this.messages = [];
            this.alerts = [];
            this.$scope.$watch(function () { return localStorage.getItem("isAuthenticated"); }, function (newVal, oldVal) {
                _this.initData(newVal);
            });
        }
        NavbarCtrl.prototype.$onInit = function () { };
        NavbarCtrl.prototype.initData = function (isAuthenticated) {
            var _this = this;
            if (JSON.parse(isAuthenticated) === true) {
                this.userId = localStorage.getItem("userId");
                this.userProfileData.getUserData(this.userId).then(function (data) {
                    _this.user = data;
                    _this.userProfilePath = data.file;
                })["catch"](function (err) {
                    _this.userProfilePath = "images/profile/profile_user.jpg";
                });
                this.alertsInfo();
                this.messagesInfo();
            }
            else {
                this.user = null;
                console.log("user null");
            }
        };
        NavbarCtrl.prototype.messagesInfo = function () {
            var _this = this;
            this.isDataLoading = true;
            this.userProfileData.getMessages().then(function (resp) {
                _this.messages = resp;
                _this.isDataLoading = false;
            });
        };
        NavbarCtrl.prototype.alertsInfo = function () {
            var _this = this;
            this.isDataLoading = true;
            this.userProfileData.getAlerts().then(function (resp) {
                _this.alerts = resp;
                _this.isDataLoading = false;
            });
        };
        NavbarCtrl.prototype.showDropdownData = function (type) {
            switch (type) {
                case DataType.Message:
                    this.isMessage = !this.isMessage;
                    this.isAlert = false;
                    break;
                case DataType.Alert:
                    this.isAlert = !this.isAlert;
                    this.isMessage = false;
                    break;
            }
        };
        NavbarCtrl.prototype.logOut = function () {
            var _this = this;
            var open = this.$uibModal.open({
                animation: true,
                templateUrl: "app/shared/modal/modal.tpl.html",
                controller: "ModalController",
                controllerAs: "vm",
                windowClass: "show",
                backdropClass: "show",
                resolve: {
                    entityName: function () {
                        return "logout";
                    }
                }
            });
            open.result.then(function (success) {
                if (success) {
                    _this.authService.isLoggedIn = false;
                    localStorage.removeItem("token");
                    localStorage.removeItem("isAuthenticated");
                    _this.$location.path("/login");
                }
            });
        };
        NavbarCtrl.prototype.toggleNav = function () {
            console.log(this.isExpand);
            this.isExpand = !this.isExpand;
            this.isExpand ? this.$element.parent().removeClass("sidenav-toggled") : this.$element.parent().addClass("sidenav-toggled");
        };
        NavbarCtrl.prototype.removeSidenavToggled = function (e) {
            e.preventDefault();
            angular.element("body").removeClass("sidenav-toggled");
        };
        NavbarCtrl.$inject = [
            "$scope",
            "$uibModal",
            "$location",
            "AuthService",
            "$element",
            "UserProfileData"
        ];
        return NavbarCtrl;
    }());
    UserProfile.NavbarCtrl = NavbarCtrl;
    UserProfile.app.controller("NavbarCtrl", NavbarCtrl);
})(UserProfile || (UserProfile = {}));
var UserProfile;
(function (UserProfile) {
    "use strict";
    var PortfolioCtrl = /** @class */ (function () {
        function PortfolioCtrl($scope, $element, userProfileData, $q, $uibModal, $location) {
            this.$scope = $scope;
            this.$element = $element;
            this.userProfileData = userProfileData;
            this.$q = $q;
            this.$uibModal = $uibModal;
            this.$location = $location;
        }
        PortfolioCtrl.prototype.$onInit = function () {
            var _this = this;
            this.userId = localStorage.getItem("userId");
            var count = 1;
            this.getPortfolioImages(this.userId).then(function (data) {
                _this.portfolioData = _.flatMap(data, function (value) {
                    value.category = count;
                    var x = [];
                    _.forEach(value.files, function (file) {
                        x.push({
                            _id: value._id,
                            imageId: file._id,
                            path: file.path,
                            category: value.category,
                            portfolioName: value.portfolioName,
                            price: file.price,
                            createdAt: moment.utc(file.createdAt).format("Y/m/d")
                        });
                    });
                    count++;
                    return x;
                });
                if (_this.imageCategories.length) {
                    setTimeout(function () {
                        _this.$element.find(".filtr-container").filterizr();
                    }, 200);
                }
            });
        };
        PortfolioCtrl.prototype.getPortfolioImages = function (userId) {
            var _this = this;
            var deferred = this.$q.defer();
            var promise = this.userProfileData.getPortfolioList(userId).then(function (data) {
                _this.imageCategories = data;
                var images = _.map(data, function (category) {
                    return _this.userProfileData.getImages(userId, category.id);
                });
                _this.$q.all(images).then(function (data) {
                    deferred.resolve(data);
                });
            });
            return deferred.promise;
        };
        PortfolioCtrl.prototype.removeImage = function (img) {
            var _this = this;
            console.log(img);
            var open = this.$uibModal.open({
                animation: true,
                templateUrl: "app/shared/modal/modal.tpl.html",
                windowClass: "show",
                backdropClass: "show",
                controller: "ModalController",
                controllerAs: "vm",
                resolve: {
                    entityName: function () {
                        return "delete";
                    }
                }
            });
            open.result.then(function (success) {
                if (success) {
                    var count_1 = 1;
                    _this.userProfileData.removePortfolioImage(_this.userId, img._id, img.imageId).then(function (data) {
                        _this.getPortfolioImages(_this.userId).then(function (data) {
                            _this.portfolioData = _.flatMap(data, function (value) {
                                value.category = count_1;
                                var x = [];
                                _.forEach(value.files, function (file) {
                                    x.push({ _id: value._id, imageId: file._id, path: file.path, category: value.category, portfolioName: value.portfolioName });
                                });
                                count_1++;
                                return x;
                            });
                            setTimeout(function () {
                                _this.$element.find(".filtr-container").filterizr();
                            }, 200);
                        });
                    });
                }
            });
        };
        PortfolioCtrl.prototype.zoomImage = function (img) {
            var open = this.$uibModal.open({
                animation: true,
                templateUrl: "app/shared/modal/image-modal.tpl.html",
                windowClass: "show",
                backdropClass: "show",
                controller: "ModalController",
                controllerAs: "vm",
                size: "lg",
                resolve: {
                    entityName: function () {
                        return img;
                    }
                }
            });
            open.result.then(function (success) {
                if (success) {
                }
            });
        };
        PortfolioCtrl.prototype.addItemClass = function ($event) {
            _.forEach(this.$element.find(".simplefilter").children(), function (item) {
                angular.element(item).removeClass("active");
            });
            angular.element($event.currentTarget).addClass("active");
        };
        PortfolioCtrl.$inject = [
            "$scope",
            "$element",
            "UserProfileData",
            "$q",
            "$uibModal",
            "$location"
        ];
        return PortfolioCtrl;
    }());
    UserProfile.PortfolioCtrl = PortfolioCtrl;
    UserProfile.app.controller("PortfolioCtrl", PortfolioCtrl);
})(UserProfile || (UserProfile = {}));
var UserProfile;
(function (UserProfile) {
    "use strict";
    var PortfolioSettingsCtrl = /** @class */ (function () {
        function PortfolioSettingsCtrl($scope, userProfileData, authService, $location) {
            this.$scope = $scope;
            this.userProfileData = userProfileData;
            this.authService = authService;
            this.$location = $location;
            this.alerts = [];
        }
        PortfolioSettingsCtrl.prototype.$onInit = function () {
            var _this = this;
            this.userId = localStorage.getItem("userId");
            this.userProfileData.getPortfolioList(this.userId).then(function (result) {
                _this.portfolio = result;
            })["catch"](function () {
            });
        };
        PortfolioSettingsCtrl.prototype.convert = function (files) {
            this.userProfileData.convertToBase64(files).then(function (data) {
                _.forEach(files, function (file, keyFile) {
                    file.path = data[keyFile];
                });
                console.log(files);
            });
        };
        PortfolioSettingsCtrl.prototype.uploadPortfolioImages = function (form, files, portfolio) {
            this.form = form;
            if (form.$valid) {
                var portfolioNameFound = _.find(this.portfolio, function (item) {
                    return item.id === portfolio;
                });
                if (portfolioNameFound) {
                    this.updatePortfolioImages(files, portfolioNameFound);
                }
                else {
                    var newPortfolio = {
                        name: portfolio,
                        id: null
                    };
                    this.savePortfolioImages(files, newPortfolio);
                }
            }
            angular.forEach(form.$error, function (field) {
                angular.forEach(field, function (errorField) {
                    errorField.$setTouched();
                });
            });
        };
        PortfolioSettingsCtrl.prototype.updatePortfolioImages = function (files, portfolio) {
            if (files.length) {
                this.userProfileData.updateImages(files, this.userId, portfolio).then(function (data) {
                    console.log("portfolio updated", data);
                });
            }
        };
        PortfolioSettingsCtrl.prototype.savePortfolioImages = function (files, portfolio) {
            if (files.length) {
                this.userProfileData.saveImages(files, this.userId, portfolio).then(function (data) {
                    console.log("portfolio saved", data);
                });
            }
        };
        PortfolioSettingsCtrl.$inject = [
            "$scope",
            "UserProfileData",
            "AuthService",
            "$location"
        ];
        return PortfolioSettingsCtrl;
    }());
    UserProfile.PortfolioSettingsCtrl = PortfolioSettingsCtrl;
    UserProfile.app.controller("PortfolioSettingsCtrl", PortfolioSettingsCtrl);
})(UserProfile || (UserProfile = {}));
var UserProfile;
(function (UserProfile) {
    "use strict";
    var ShoppingBagCtrl = /** @class */ (function () {
        function ShoppingBagCtrl($scope, $uibModal, $location, $element, userProfileData, $rootScope, $cookies) {
            var _this = this;
            this.$scope = $scope;
            this.$uibModal = $uibModal;
            this.$location = $location;
            this.$element = $element;
            this.userProfileData = userProfileData;
            this.$rootScope = $rootScope;
            this.$cookies = $cookies;
            this.products = [];
            this.userProfileData.onAddedItem(function (data) {
                _this.products.push(data);
                console.log(_this.products);
                _this.$cookies.put("products", JSON.stringify(_this.products), { expires: moment(new Date(), "DD-MM-YYYY").add(5, "days").utc() });
            });
        }
        ShoppingBagCtrl.prototype.$onInit = function () {
            console.log(this.products);
        };
        ShoppingBagCtrl.prototype.nextStep = function () {
        };
        ShoppingBagCtrl.prototype.buyImage = function (img, userId) {
            this.userProfileData.buyImage(userId, img).then(function (payment) {
                console.log(payment);
                for (var i = 0; i < payment.links.length; i++) {
                    if (payment.links[i].rel === "approval_url") {
                        window.location.href = payment.links[i].href;
                    }
                }
            });
        };
        ShoppingBagCtrl.$inject = [
            "$scope",
            "$uibModal",
            "$location",
            "$element",
            "UserProfileData",
            "$rootScope",
            "$cookies"
        ];
        return ShoppingBagCtrl;
    }());
    UserProfile.ShoppingBagCtrl = ShoppingBagCtrl;
    UserProfile.app.controller("ShoppingBagCtrl", ShoppingBagCtrl);
})(UserProfile || (UserProfile = {}));
var UserProfile;
(function (UserProfile) {
    "use strict";
    var SkillsCtrl = /** @class */ (function () {
        function SkillsCtrl($scope, userProfileData) {
            this.$scope = $scope;
            this.userProfileData = userProfileData;
            this.devSkills = [];
        }
        SkillsCtrl.prototype.$onInit = function () {
            var _this = this;
            this.devSkills = [{
                    name: 'javascript',
                    value: 80
                },
                {
                    name: 'angular',
                    value: 80
                },
                {
                    name: 'jquery',
                    value: 70
                },
                {
                    name: 'typescript',
                    value: 85
                },
                {
                    name: 'html',
                    value: 90
                },
                {
                    name: 'css',
                    value: 90
                },
                {
                    name: 'bootstrap',
                    value: 85
                }];
            this.userProfileData.getChartData().then(function (data) {
                _this.chart = data;
            });
        };
        SkillsCtrl.$inject = [
            "$scope",
            "UserProfileData"
        ];
        return SkillsCtrl;
    }());
    UserProfile.SkillsCtrl = SkillsCtrl;
    UserProfile.app.controller("SkillsCtrl", SkillsCtrl);
})(UserProfile || (UserProfile = {}));
var UserProfile;
(function (UserProfile) {
    "use strict";
    var AuthService = /** @class */ (function () {
        function AuthService($http, $q, Upload) {
            var _this = this;
            this.$http = $http;
            this.$q = $q;
            this.Upload = Upload;
            this.isLoggedIn = false;
            this.alreadyLoggedIn = function () {
                return _this.isLoggedIn;
            };
            this.apiPath = "http://localhost:4000";
        }
        AuthService.prototype.$onInit = function () {
            console.log(this.isLoggedIn);
        };
        AuthService.prototype.login = function (data) {
            return this.post(this.apiPath + "/auth/login", data);
        };
        AuthService.prototype.forgotPassword = function (data) {
            return this.post(this.apiPath + "/auth/forgot_password", data);
        };
        AuthService.prototype.resetPassword = function (id, data) {
            return this.post(this.apiPath + "/auth/reset/" + id, data);
        };
        AuthService.prototype.registerUser = function (file, user) {
            var _this = this;
            return this.convertToBase64(file).then(function (baseUrl) {
                user.file = baseUrl;
                return _this.post(_this.apiPath + "/auth/register", user);
            });
        };
        AuthService.prototype.getUserProfileImage = function (userId) {
            return this.get(this.apiPath + "/image/" + userId);
        };
        AuthService.prototype.convertToBase64 = function (file) {
            var deferred = this.$q.defer();
            this.Upload.base64DataUrl(file).then(function (base64Urls) {
                deferred.resolve(base64Urls);
            });
            return deferred.promise;
        };
        AuthService.prototype.get = function (url) {
            return this.$http.get(url, {
                headers: {
                    "Content-Type": "application/json"
                }
            }).then(function (response) {
                return response.data;
            });
        };
        AuthService.prototype.put = function (url, data) {
            return this.$http.put(url, data).then(function (response) {
                return response.data;
            });
        };
        AuthService.prototype.post = function (url, data) {
            return this.$http.post(url, data).then(function (response) {
                return response.data;
            });
        };
        AuthService.prototype["delete"] = function (url) {
            return this.$http["delete"](url).then(function (response) {
                return response.data;
            });
        };
        AuthService.prototype.deleteWithBody = function (url, data) {
            return this.$http({
                method: "DELETE",
                url: url,
                data: data,
                headers: { "Content-Type": "application/json;charset=utf-8" }
            }).then(function (response) {
                return response.data;
            });
        };
        AuthService.$inject = ["$http", "$q",
            "Upload"];
        return AuthService;
    }());
    UserProfile.AuthService = AuthService;
    UserProfile.app.service("AuthService", AuthService);
})(UserProfile || (UserProfile = {}));
var UserProfile;
(function (UserProfile) {
    "use strict";
    var DeveloperSkills = /** @class */ (function () {
        function DeveloperSkills() {
            this.templateUrl = "app/shared/component/developer-skills.tpl.html";
            this.bindings = {
                devSkills: "="
            };
        }
        return DeveloperSkills;
    }());
    UserProfile.DeveloperSkills = DeveloperSkills;
    UserProfile.app.component("developerSkills", new DeveloperSkills());
})(UserProfile || (UserProfile = {}));
var UserProfile;
(function (UserProfile) {
    "use strict";
    var ModalCtrl = /** @class */ (function () {
        function ModalCtrl($modalInstance, entityName) {
            this.$modalInstance = $modalInstance;
            this.entityName = entityName;
        }
        ModalCtrl.prototype.$onInit = function () { };
        ModalCtrl.prototype.ok = function () {
            this.$modalInstance.close(true);
        };
        ModalCtrl.prototype.cancel = function () {
            this.$modalInstance.dismiss(false);
        };
        ModalCtrl.$inject = [
            "$uibModalInstance",
            "entityName"
        ];
        return ModalCtrl;
    }());
    UserProfile.ModalCtrl = ModalCtrl;
    UserProfile.app.controller("ModalController", ModalCtrl);
})(UserProfile || (UserProfile = {}));
var UserProfile;
(function (UserProfile) {
    "use strict";
    var UserProfileData = /** @class */ (function () {
        function UserProfileData($http, $q, Upload, authService, $rootScope) {
            this.$http = $http;
            this.$q = $q;
            this.Upload = Upload;
            this.authService = authService;
            this.$rootScope = $rootScope;
            this.apiPath = "http://localhost:4000";
        }
        UserProfileData.prototype.addFriend = function (userId, friend) {
            var data = [];
            data = data.concat(friend);
            return this.post(this.apiPath + "/friends/" + userId, { userId: userId, list: data });
        };
        UserProfileData.prototype.getAddedFriendsList = function (userId) {
            return this.get(this.apiPath + "/friends/" + userId);
        };
        UserProfileData.prototype.removeFriend = function (userId, friendId) {
            return this["delete"](this.apiPath + "/friends/" + friendId + "/" + userId);
        };
        UserProfileData.prototype.updateFriendsList = function (userId, friend) {
            return this.put(this.apiPath + "/friends/" + userId, { list: [friend] });
        };
        UserProfileData.prototype.saveChanges = function (file, user) {
            var _this = this;
            return this.convertToBase64([file]).then(function (baseUrl) {
                user.file = _.first(baseUrl);
                return _this.put(_this.apiPath + "/users/" + user._id, user);
            });
        };
        UserProfileData.prototype.buyImage = function (userId, img) {
            return this.post(this.apiPath + "/paypal/" + userId, { userId: userId, product: img });
        };
        UserProfileData.prototype.removePortfolioImage = function (userId, portfolioId, imgId) {
            return this["delete"](this.apiPath + "/portfolio/" + imgId + "/" + portfolioId + "/" + userId);
        };
        UserProfileData.prototype.convertToBase64 = function (files) {
            var _this = this;
            var deferred = this.$q.defer();
            if (files.length) {
                var promiseArr_1 = [];
                _.forEach(files, function (file) {
                    promiseArr_1.push(_this.Upload.resize(file, { height: 300, width: 300 }).then(function (resizedFile) {
                        return _this.Upload.base64DataUrl(resizedFile);
                    }));
                });
                this.$q.all(promiseArr_1).then(function (data) {
                    deferred.resolve(data);
                });
            }
            return deferred.promise;
        };
        UserProfileData.prototype.updateImages = function (files, userId, portfolio) {
            console.log(files);
            var newSchema = _.map(files, function (val) {
                return { path: val.path, price: val.price, createdAt: val.lastModifiedDate };
            });
            console.log(newSchema);
            return this.put(this.apiPath + "/portfolio/" + portfolio.id + "/" + userId, { files: newSchema });
        };
        UserProfileData.prototype.saveImages = function (files, userId, portfolio) {
            var newSchema = _.map(files, function (val) {
                return { path: val.path, price: val.price, createdAt: val.lastModifiedDate };
            });
            return this.post(this.apiPath + "/portfolio/" + userId, { files: newSchema, userId: userId, portfolioName: portfolio.name });
        };
        UserProfileData.prototype.getPortfolioList = function (userId) {
            return this.get(this.apiPath + "/portfolio/" + userId + "/list");
        };
        UserProfileData.prototype.getUserData = function (id) {
            var _this = this;
            return this.get(this.apiPath + "/users/" + id).then(function (data) { return _this.user = data; });
        };
        UserProfileData.prototype.getImages = function (userId, portfolioId) {
            return this.get(this.apiPath + "/portfolio/" + portfolioId + "/" + userId);
        };
        UserProfileData.prototype.getCustomers = function () {
            return this.get("app/shared/service/mock-data/customers.json");
        };
        UserProfileData.prototype.getChartData = function () {
            return this.get("app/shared/service/mock-data/chart.json");
        };
        UserProfileData.prototype.getFriends = function () {
            return this.get(this.apiPath + "/users");
        };
        UserProfileData.prototype.getMessages = function () {
            return this.get("app/shared/service/mock-data/messages.json");
        };
        UserProfileData.prototype.getAlerts = function () {
            return this.get("app/shared/service/mock-data/alerts.json");
        };
        UserProfileData.prototype.onAddedItem = function (handle) {
            return this.$rootScope.$on("metadataComplete", function (event, data) {
                console.log(data);
                handle(data);
            });
        };
        UserProfileData.prototype.addedItem = function (data) {
            this.$rootScope.$emit("metadataComplete", data);
        };
        UserProfileData.prototype.getUserDetails = function () {
            return this.user;
        };
        UserProfileData.prototype.get = function (url) {
            return this.$http.get(url).then(function (response) {
                return response.data;
            });
        };
        UserProfileData.prototype.put = function (url, data) {
            return this.$http.put(url, data).then(function (response) {
                return response.data;
            });
        };
        UserProfileData.prototype.post = function (url, data) {
            return this.$http.post(url, data).then(function (response) {
                return response.data;
            });
        };
        UserProfileData.prototype["delete"] = function (url) {
            return this.$http["delete"](url).then(function (response) {
                return response.data;
            });
        };
        UserProfileData.prototype.deleteWthBody = function (url, data) {
            return this.$http({
                method: "DELETE",
                url: url,
                data: data,
                headers: { "Content-Type": "application/json;charset=utf-8" }
            }).then(function (response) {
                return response.data;
            });
        };
        // metadataComplete: boolean;
        UserProfileData.$inject = ["$http", "$q", "Upload", "AuthService", "$rootScope"];
        return UserProfileData;
    }());
    UserProfile.UserProfileData = UserProfileData;
    UserProfile.app.service("UserProfileData", UserProfileData);
})(UserProfile || (UserProfile = {}));
