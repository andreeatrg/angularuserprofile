﻿namespace UserProfile {
    "use strict";
    export class DeveloperSkills implements ng.IComponentOptions {
        controller: string;
        templateUrl: string;
        controllerAs: string;
        bindings: { [boundProperty: string]: string };
        transclude: boolean;

        constructor() {
            this.templateUrl = "app/shared/component/developer-skills.tpl.html";
            this.bindings = {
                devSkills: "="
            };
        }
    }

    app.component("developerSkills", new DeveloperSkills());
}