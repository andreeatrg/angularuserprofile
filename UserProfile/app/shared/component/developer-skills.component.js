var UserProfile;
(function (UserProfile) {
    "use strict";
    var DeveloperSkills = (function () {
        function DeveloperSkills() {
            this.templateUrl = "app/shared/component/developer-skills.tpl.html";
            this.bindings = {
                devSkills: "="
            };
        }
        return DeveloperSkills;
    }());
    UserProfile.DeveloperSkills = DeveloperSkills;
    UserProfile.app.component("developerSkills", new DeveloperSkills());
})(UserProfile || (UserProfile = {}));
//# sourceMappingURL=developer-skills.component.js.map