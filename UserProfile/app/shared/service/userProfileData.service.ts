﻿namespace UserProfile {
    "use strict";

    export class UserProfileData {
        apiPath: string;
        user: IUserData;
        // metadataComplete: boolean;
        static $inject = ["$http", "$q", "Upload", "AuthService", "$rootScope"];

        constructor(
            private $http: angular.IHttpService,
            private $q: angular.IQService,
            private Upload: angular.angularFileUpload.IUploadService,
            private authService: AuthService,
            private $rootScope: ng.IRootScopeService
        ) {
            this.apiPath = "http://localhost:4000";
        }
        addFriend(userId: string, friend: IFriend) {
            let data: IFriend[] = [];
            data = data.concat(friend);
            return this.post(`${this.apiPath}/friends/${userId}`, { userId: userId, list: data });
        }
        getAddedFriendsList(userId: string) {
            return this.get<IFriend[]>(`${this.apiPath}/friends/${userId}`);
        }
        removeFriend(userId: string, friendId: string) {
            return this.delete(`${this.apiPath}/friends/${friendId}/${userId}`);
        }
        updateFriendsList(userId: string, friend: IFriend) {
            return this.put(`${this.apiPath}/friends/${userId}`, { list: [friend] });
        }
        saveChanges(file: File, user: IUserData) {
            return this.convertToBase64([file]).then((baseUrl: string[]) => {
                user.file = _.first(baseUrl);
                return this.put(`${this.apiPath}/users/${user._id}`, user);
            });
        }
        buyImage(userId: string, img: IImageData) {
            return this.post(`${this.apiPath}/paypal/${userId}`, { userId: userId, product: img });
        }
        removePortfolioImage(userId: string, portfolioId: string, imgId: string) {
            return this.delete(`${this.apiPath}/portfolio/${imgId}/${portfolioId}/${userId}`);
        }
        convertToBase64(files: File[]) {
            let deferred = this.$q.defer();
            if ((<File[]>files).length) {
                let promiseArr: ng.IPromise<string | string[]>[] = [];
                _.forEach((<File[]>files), (file: File) => {
                    promiseArr.push(this.Upload.resize(file, { height: 300, width: 300 }).then((resizedFile) => {
                        return this.Upload.base64DataUrl(resizedFile);
                    }));
                });
                this.$q.all(promiseArr).then((data) => {
                    deferred.resolve(data);
                });
            }

            return deferred.promise;
        }
        updateImages(files: File[], userId: string, portfolio: IPortfolio) {
            console.log(files);
            let newSchema = _.map(files, (val) => {
                return { path: (<any>val).path, price: (<any>val).price, createdAt: val.lastModifiedDate };
            });
            console.log(newSchema);
            return this.put(`${this.apiPath}/portfolio/${portfolio.id}/${userId}`, { files: newSchema });

        }
        saveImages(files: File[], userId: string, portfolio: IPortfolio) {
            let newSchema = _.map(files, (val) => {
                return { path: (<any>val).path, price: (<any>val).price, createdAt: val.lastModifiedDate };
            });
            return this.post(`${this.apiPath}/portfolio/${userId}`, { files: newSchema, userId: userId, portfolioName: portfolio.name });
        }
        getPortfolioList(userId: string) {
            return this.get<IPortfolio[]>(`${this.apiPath}/portfolio/${userId}/list`);
        }
        getUserData(id: string) {
            return this.get<IUserData>(`${this.apiPath}/users/${id}`).then(data => this.user = data);
        }
        getImages(userId: string, portfolioId: string) {
            return this.get<IPortfolioImage>(`${this.apiPath}/portfolio/${portfolioId}/${userId}`);
        }

        getCustomers() {
            return this.get<ICustomer[]>("app/shared/service/mock-data/customers.json");
        }
        getChartData() {
            return this.get<IChartData>("app/shared/service/mock-data/chart.json");
        }
        getFriends() {
            return this.get<IFriend[]>(`${this.apiPath}/users`);
        }
        getMessages() {
            return this.get<IMessage[]>("app/shared/service/mock-data/messages.json");
        }
        getAlerts() {
            return this.get<IAlert[]>("app/shared/service/mock-data/alerts.json");
        }
        addedItem(data: { item: IImageData, user: IUserData }) {
            console.info(data);
            this.$rootScope.$emit("products", data);
        }
        onAddedItem(handle: Function) {
            return this.$rootScope.$on("products", (event: ng.IAngularEvent, data: { item: IImageData, user: IUserData }) => {
                console.info(data);
                handle(data);
            });
        }
        getUserDetails() {
            return this.user;
        }
        private get<T>(url: string): angular.IPromise<T> {
            return this.$http.get<T>(url).then((response) => {
                return response.data;
            });

        }
        private put<T>(url: string, data: any): angular.IPromise<T> {
            return this.$http.put<T>(url, data).then((response) => {
                return response.data;
            });
        }
        private post<T>(url: string, data: any): angular.IPromise<T> {
            return this.$http.post<T>(url, data).then((response) => {
                return response.data;
            });
        }
        private delete<T>(url: string): ng.IPromise<T> {
            return this.$http.delete<T>(url).then((response) => {
                return response.data;
            });
        }

        private deleteWthBody<T>(url: string, data: any): ng.IPromise<T> {
            return this.$http<T>({
                method: "DELETE",
                url: url,
                data: data,
                headers: { "Content-Type": "application/json;charset=utf-8" }
            }).then((response) => {
                return response.data;
            });
        }

    }

    app.service("UserProfileData", UserProfileData);
}