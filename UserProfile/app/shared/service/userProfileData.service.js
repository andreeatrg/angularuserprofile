var UserProfile;
(function (UserProfile) {
    "use strict";
    var UserProfileData = (function () {
        function UserProfileData($http, $q) {
            this.$http = $http;
            this.$q = $q;
        }
        UserProfileData.prototype.getCustomers = function () {
            return this.get("app/shared/service/mock-data/customers.json");
        };
        UserProfileData.prototype.getChartData = function () {
            return this.get("app/shared/service/mock-data/chart.json");
        };
        UserProfileData.prototype.getFriends = function () {
            return this.get("app/shared/service/mock-data/friends.json");
        };
        UserProfileData.prototype.getMessages = function () {
            return this.get("app/shared/service/mock-data/messages.json");
        };
        UserProfileData.prototype.getAlerts = function () {
            return this.get("app/shared/service/mock-data/alerts.json");
        };
        UserProfileData.prototype.getImages = function () {
            return this.get("app/shared/service/mock-data/work.json");
        };
        UserProfileData.prototype.get = function (url) {
            return this.$http.get(url).then(function (response) {
                return response.data;
            });
        };
        UserProfileData.prototype.put = function (url, data) {
            return this.$http.put(url, data).then(function (response) {
                return response.data;
            });
        };
        UserProfileData.prototype.post = function (url, data) {
            return this.$http.post(url, data).then(function (response) {
                return response.data;
            });
        };
        UserProfileData.prototype.delete = function (url) {
            return this.$http.delete(url).then(function (response) {
                return response.data;
            });
        };
        UserProfileData.prototype.deleteWithBody = function (url, data) {
            return this.$http({
                method: "DELETE",
                url: url,
                data: data,
                headers: { "Content-Type": "application/json;charset=utf-8" }
            }).then(function (response) {
                return response.data;
            });
        };
        return UserProfileData;
    }());
    UserProfileData.$inject = ["$http", "$q"];
    UserProfile.UserProfileData = UserProfileData;
    UserProfile.app.service("UserProfileData", UserProfileData);
})(UserProfile || (UserProfile = {}));
//# sourceMappingURL=userProfileData.service.js.map