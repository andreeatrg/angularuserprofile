var UserProfile;
(function (UserProfile) {
    "use strict";
    var AuthService = (function () {
        function AuthService() {
        }
        AuthService.prototype.$onInit = function () {
            this.isLoggedIn = false;
        };
        return AuthService;
    }());
    AuthService.$inject = [];
    UserProfile.AuthService = AuthService;
    UserProfile.app.service("AuthService", AuthService);
})(UserProfile || (UserProfile = {}));
//# sourceMappingURL=auth.service.js.map