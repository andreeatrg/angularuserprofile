﻿namespace UserProfile {
    "use strict";

    export class AuthService {
        apiPath: string;
        isLoggedIn: boolean = false;
        static $inject = ["$http", "$q",
            "Upload"];

        constructor(
            private $http: angular.IHttpService,
            private $q: angular.IQService,
            private Upload: angular.angularFileUpload.IUploadService
        ) {
            this.apiPath = `http://localhost:4000`;
        }

        $onInit() {
            console.log(this.isLoggedIn);
        }
        login(data: IUserData) {
            return this.post(`${this.apiPath}/auth/login`, data);
        }
        alreadyLoggedIn = () => {
            return this.isLoggedIn;
        }
        forgotPassword(data: IUserData) {
            return this.post(`${this.apiPath}/auth/forgot_password`, data);
        }
        resetPassword(id: string, data: { newPassword: string, confirmPassword: string }) {
            return this.post(`${this.apiPath}/auth/reset/${id}`, data);
        }
        registerUser(file: File, user: IUserData) {
            return this.convertToBase64(file).then((baseUrl: string) => {
                user.file = baseUrl;
                return this.post(`${this.apiPath}/auth/register`, user);
            });
        }
        getUserProfileImage(userId: string) {
            return this.get(`${this.apiPath}/image/${userId}`);
        }
        convertToBase64(file: File) {
            let deferred = this.$q.defer();
            this.Upload.base64DataUrl(file).then((base64Urls: string) => {
                deferred.resolve(base64Urls);
            });
            return deferred.promise;
        }
        private get<T>(url: string): angular.IPromise<T> {
            return this.$http.get<T>(url, {
                headers: {
                    "Content-Type": "application/json"
                }
            }).then((response) => {
                return response.data;
            });

        }
        private put<T>(url: string, data: any): angular.IPromise<T> {
            return this.$http.put<T>(url, data).then((response) => {
                return response.data;
            });
        }
        private post<T>(url: string, data: any): angular.IPromise<T> {
            return this.$http.post<T>(url, data).then((response) => {
                return response.data;
            });
        }
        private delete<T>(url: string): ng.IPromise<T> {
            return this.$http.delete<T>(url).then((response) => {
                return response.data;
            });
        }

        private deleteWithBody<T>(url: string, data: any): ng.IPromise<T> {
            return this.$http<T>({
                method: "DELETE",
                url: url,
                data: data,
                headers: { "Content-Type": "application/json;charset=utf-8" }
            }).then((response) => {
                return response.data;
            });
        }
    }

    app.service("AuthService", AuthService);
}