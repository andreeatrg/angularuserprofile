﻿namespace UserProfile {
    "use strict";

    export class ModalCtrl {
        static $inject = [
            "$uibModalInstance",
            "entityName"
        ];

        constructor(private $modalInstance: angular.ui.bootstrap.IModalServiceInstance,
            public entityName: string
        ) {
        }
        $onInit() { }
        ok(): void {
            this.$modalInstance.close(true);
        }

        cancel(): void {
            this.$modalInstance.dismiss(false);
        }

    }

    app.controller("ModalController", ModalCtrl);
}