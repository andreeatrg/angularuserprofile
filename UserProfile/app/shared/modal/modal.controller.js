var UserProfile;
(function (UserProfile) {
    "use strict";
    var ModalController = (function () {
        function ModalController($modalInstance, entityName) {
            this.$modalInstance = $modalInstance;
            this.entityName = entityName;
        }
        ModalController.prototype.ok = function () {
            this.$modalInstance.close(true);
        };
        ModalController.prototype.cancel = function () {
            this.$modalInstance.dismiss(false);
        };
        return ModalController;
    }());
    ModalController.$inject = [
        "$uibModalInstance",
        "entityName"
    ];
    UserProfile.ModalController = ModalController;
    UserProfile.app.controller("ModalController", ModalController);
})(UserProfile || (UserProfile = {}));
//# sourceMappingURL=modal.controller.js.map