namespace UserProfile {
    "use strict";

    export class PortfolioSettingsCtrl {
        alerts: IAlert[] = [];
        form: ng.IFormController;
        userId: string;
        portfolio: IPortfolio[];
        static $inject = [
            "$scope",
            "UserProfileData",
            "AuthService",
            "$location"
        ];

        constructor(
            private $scope: ng.IScope,
            private userProfileData: UserProfileData,
            private authService: AuthService,
            private $location: ng.ILocationService
        ) {
        }

        $onInit() {
            this.userId = localStorage.getItem("userId");
            this.userProfileData.getPortfolioList(this.userId).then((result: IPortfolio[]) => {
                this.portfolio = result;
            }).catch(() => {
            });
        }
        convert(files: File[]) {
            this.userProfileData.convertToBase64(files).then((data: string[]) => {
                _.forEach(files, (file, keyFile) => {
                    (<any>file).path = data[keyFile];
                });
                console.log(files);
            });
        }
        uploadPortfolioImages(form: ng.IFormController, files: File[], portfolio: string) {
            this.form = form;
            if (form.$valid) {
                let portfolioNameFound = _.find(this.portfolio, (item) => {
                    return item.id === portfolio;
                });
                if (portfolioNameFound) {
                    this.updatePortfolioImages(files, portfolioNameFound);
                } else {
                    let newPortfolio: IPortfolio = {
                        name: portfolio,
                        id: null
                    };
                    this.savePortfolioImages(files, newPortfolio);
                }

            }

            angular.forEach(form.$error,
                (field) => {
                    angular.forEach(field,
                        (errorField) => {
                            errorField.$setTouched();
                        });
                });
        }
        updatePortfolioImages(files: File[], portfolio: IPortfolio) {
            if (files.length) {
                this.userProfileData.updateImages(files, this.userId, portfolio).then((data) => {
                    console.log("portfolio updated", data);
                });
            }
        }

        savePortfolioImages(files: File[], portfolio: IPortfolio) {
            if (files.length) {
                this.userProfileData.saveImages(files, this.userId, portfolio).then((data) => {
                    console.log("portfolio saved", data);
                });
            }
        }

    }

    app.controller("PortfolioSettingsCtrl", PortfolioSettingsCtrl);
}