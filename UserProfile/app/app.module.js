var UserProfile;
(function (UserProfile) {
    "use strict";
    UserProfile.app = angular.module('userProfile', [
        'ui.router',
        "ui.bootstrap",
        'ngSanitize',
        'ngAnimate',
        'ngMessages',
        'chart.js',
        'userprofile-templates'
    ]);
    UserProfile.app.config([
        '$stateProvider', '$urlRouterProvider', '$httpProvider',
        function ($stateProvider, $urlRouterProvider, $httpProvider) {
            $stateProvider.state("home", {
                url: "/",
                reloadOnSearch: false,
                views: {
                    'nav': {
                        templateUrl: "app/navbar/navbar.html",
                        controller: "NavbarCtrl",
                        controllerAs: "vm"
                    },
                    '': {
                        templateUrl: 'app/main-view/main-view.html',
                        controller: "MainCtrl",
                        controllerAs: "vm"
                    },
                    'footer': {
                        templateUrl: "app/footer/footer.html",
                        controller: "FooterCtrl",
                        controllerAs: "vm"
                    }
                },
                data: { requireLogin: true }
            })
                .state('login', {
                    url: '/login',
                    templateUrl: 'app/login/login.html',
                    controller: "LoginCtrl",
                    controllerAs: "vm"
                }).state('register', {
                    url: '/register',
                    templateUrl: 'app/login/register.html'
                }).state('forgotPassword', {
                    url: '/forgot-password',
                    templateUrl: 'app/login/forgot-password.html'
                }).state('/reset/:id', {
                    templateUrl: "app/login/forgot-password.html",
                    controller: 'Ctrl'
                }).state('portfolio', {
                    url: '/portfolio',
                    reloadOnSearch: false,
                    views: {
                        'nav': {
                            templateUrl: "app/navbar/navbar.html",
                            controller: "NavbarCtrl",
                            controllerAs: "vm"
                        },
                        '': {
                            templateUrl: 'app/portfolio/portfolio.tpl.html',
                            controller: "PortfolioCtrl",
                            controllerAs: "vm",
                        },
                        'footer': {
                            templateUrl: "app/footer/footer.html",
                            controller: "FooterCtrl",
                            controllerAs: "vm"
                        }
                    },
                    data: { requireLogin: true }
                }).state('friends', {
                    url: '/friends',
                    reloadOnSearch: false,
                    views: {
                        'nav': {
                            templateUrl: "app/navbar/navbar.html",
                            controller: "NavbarCtrl",
                            controllerAs: "vm"
                        },
                        '': {
                            templateUrl: 'app/friends/friends.tpl.html',
                            controller: "FriendsCtrl",
                            controllerAs: "vm",
                        },
                        'footer': {
                            templateUrl: "app/footer/footer.html",
                            controller: "FooterCtrl",
                            controllerAs: "vm"
                        }
                    },
                    data: { requireLogin: true }
                }).state('skills', {
                    url: '/skills',
                    reloadOnSearch: false,
                    views: {
                        'nav': {
                            templateUrl: "app/navbar/navbar.html",
                            controller: "NavbarCtrl",
                            controllerAs: "vm"
                        },
                        '': {
                            templateUrl: 'app/skills/skills.tpl.html',
                            controller: "SkillsCtrl",
                            controllerAs: "vm",
                        },
                        'footer': {
                            templateUrl: "app/footer/footer.html",
                            controller: "FooterCtrl",
                            controllerAs: "vm"
                        }
                    },
                    data: { requireLogin: true }
                }).state('account-settings', {
                    url: '/account-settings',
                    reloadOnSearch: false,
                    views: {
                        'nav': {
                            templateUrl: "app/navbar/navbar.html",
                            controller: "NavbarCtrl",
                            controllerAs: "vm"
                        },
                        '': {
                            templateUrl: 'app/account-settings/account-settings.tpl.html',
                            controller: "AccountSettingsCtrl",
                            controllerAs: "vm",
                        },
                        'footer': {
                            templateUrl: "app/footer/footer.html",
                            controller: "FooterCtrl",
                            controllerAs: "vm"
                        }
                    },
                    data: { requireLogin: true }
                });
            $urlRouterProvider.otherwise('/login');
        }
    ]).run([
        '$rootScope', '$state', '$location', 'AuthService',
        function ($rootScope, $state, $location, authService) {
            $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState) {
                var shouldLogin = toState.data !== undefined && toState.data.requireLogin && !authService.isLoggedIn;
                // NOT authenticated 
                if (shouldLogin) {
                    $state.go('login');
                    event.preventDefault();
                    return;
                }
                // authenticated
                if (authService.isLoggedIn) {
                    var shouldGoToMainView = fromState.name === "" && toState.name !== "home";
                    if (shouldGoToMainView) {
                        $state.go('home');
                        event.preventDefault();
                    }
                    return;
                }
            });
        }
    ]);
})(UserProfile || (UserProfile = {}));
//# sourceMappingURL=app.module.js.map