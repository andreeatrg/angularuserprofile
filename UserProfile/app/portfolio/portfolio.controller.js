var UserProfile;
(function (UserProfile) {
    "use strict";
    var PortfolioCtrl = (function () {
        function PortfolioCtrl($scope, $element, userProfileData) {
            this.$scope = $scope;
            this.$element = $element;
            this.userProfileData = userProfileData;
            this.portfoliData = [];
        }
        PortfolioCtrl.prototype.$onInit = function () {
            var _this = this;
            this.userProfileData.getImages().then(function (data) {
                _this.portfoliData = data;
                setTimeout(function () {
                    _this.$element.find('.filtr-container').filterizr();
                }, 100);
            });
        };
        PortfolioCtrl.prototype.addItemClass = function ($event) {
            _.forEach(this.$element.find('.simplefilter').children(), function (item) {
                angular.element(item).removeClass('active');
            });
            angular.element($event.currentTarget).addClass('active');
        };
        return PortfolioCtrl;
    }());
    PortfolioCtrl.$inject = [
        "$scope",
        "$element",
        "UserProfileData"
    ];
    UserProfile.PortfolioCtrl = PortfolioCtrl;
    UserProfile.app.controller("PortfolioCtrl", PortfolioCtrl);
})(UserProfile || (UserProfile = {}));
//# sourceMappingURL=portfolio.controller.js.map