﻿declare var moment: any;
namespace UserProfile {
    "use strict";

    export class PortfolioCtrl {
        portfolioData: any[];
        imageCategories: IPortfolio[];
        userId: string;
        static $inject = [
            "$scope",
            "$element",
            "UserProfileData",
            "$q",
            "$uibModal",
            "$location"
        ];

        constructor(
            private $scope: ng.IScope,
            private $element: ng.IAugmentedJQuery,
            private userProfileData: UserProfileData,
            private $q: ng.IQService,
            private $uibModal: angular.ui.bootstrap.IModalService,
            private $location: ng.ILocationService
        ) {
        }

        $onInit() {
            this.userId = localStorage.getItem("userId");
            let count: number = 1;
            this.getPortfolioImages(this.userId).then((data: IPortfolioImage[]) => {
                this.portfolioData = _.flatMap(data, (value) => {
                    value.category = count;
                    let x: any[] = [];
                    _.forEach(value.files, (file) => {
                        x.push({
                            _id: value._id,
                            imageId: file._id,
                            path: file.path,
                            category: value.category,
                            portfolioName: value.portfolioName,
                            price: (<any>file).price,
                            createdAt: moment.utc((<any>file).createdAt).format("Y/m/d")
                        });
                    });
                    count++;
                    return x;
                });
                if (this.imageCategories.length) {
                    setTimeout(() => {
                        this.$element.find(".filtr-container").filterizr();
                    }, 200);
                }
            });
        }
        getPortfolioImages(userId: string) {
            let deferred = this.$q.defer();
            let promise = this.userProfileData.getPortfolioList(userId).then((data: IPortfolio[]) => {
                this.imageCategories = data;
                let images = _.map(data, (category) => {
                    return this.userProfileData.getImages(userId, category.id);
                });

                this.$q.all(images).then((data: IPortfolioImage[]) => {
                    deferred.resolve(data);
                });
            });


            return deferred.promise;
        }
        removeImage(img: { _id: string, imageId: string, path: string, category: number, portfolioName: string }) {
            console.log(img);
            const open = this.$uibModal.open({
                animation: true,
                templateUrl: `app/shared/modal/modal.tpl.html`,
                windowClass: "show",
                backdropClass: "show",
                controller: "ModalController",
                controllerAs: "vm",
                resolve: {
                    entityName: () => {
                        return "delete";
                    }
                }
            });
            open.result.then((success) => {
                if (success) {
                    let count = 1;
                    this.userProfileData.removePortfolioImage(this.userId, img._id, img.imageId).then((data: IPortfolio[]) => {
                        this.getPortfolioImages(this.userId).then((data: IPortfolioImage[]) => {
                            this.portfolioData = _.flatMap(data, (value) => {
                                value.category = count;
                                let x: any[] = [];
                                _.forEach(value.files, (file) => {
                                    x.push({ _id: value._id, imageId: file._id, path: file.path, category: value.category, portfolioName: value.portfolioName });
                                });
                                count++;
                                return x;
                            });
                            setTimeout(() => {
                                this.$element.find(".filtr-container").filterizr();
                            }, 200);
                        });
                    });
                }
            });
        }
        zoomImage(img: string) {
            const open = this.$uibModal.open({
                animation: true,
                templateUrl: `app/shared/modal/image-modal.tpl.html`,
                windowClass: "show",
                backdropClass: "show",
                controller: "ModalController",
                controllerAs: "vm",
                size: "lg",
                resolve: {
                    entityName: () => {
                        return img;
                    }
                }
            });
            open.result.then((success) => {
                if (success) {
                }
            });
        }
        addItemClass($event: Event) {
            _.forEach(this.$element.find(".simplefilter").children(), (item) => {
                angular.element(item).removeClass("active");
            });

            angular.element($event.currentTarget).addClass("active");
        }
    }

    app.controller("PortfolioCtrl", PortfolioCtrl);
}
