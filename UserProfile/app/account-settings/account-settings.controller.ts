namespace UserProfile {
    "use strict";

    export class AccountSettingsCtrl {
        alerts: IAlert[] = [];
        user: IUserData;
        form: ng.IFormController;
        userId: string;
        static $inject = [
            "$scope",
            "UserProfileData",
            "AuthService",
            "$location"
        ];

        constructor(
            private $scope: ng.IScope,
            private userProfileData: UserProfileData,
            private authService: AuthService,
            private $location: ng.ILocationService
        ) {
        }

        $onInit() {
            this.userId = localStorage.getItem("userId");
            this.user = this.userProfileData.getUserDetails();
        }
        checkPassword(password: string, confirmPassword: string, form: ng.IFormController) {
            this.form = form;
            if (password === confirmPassword) {
                this.form.confirmPassword.$error.validationError = false;
                return true;
            } else {
                this.form.confirmPassword.$error.validationError = true;
                return false;
            }
        }
        submitForm(form: ng.IFormController, file: File) {
            this.form = form;
            if (form.$valid) {
                this.saveChanges(file);
            }

            angular.forEach(form.$error,
                (field) => {
                    angular.forEach(field,
                        (errorField) => {
                            errorField.$setTouched();
                        });
                });
        }
        saveChanges(file?: File) {
            if (file != null) {
                this.userProfileData.saveChanges(file, this.user).then(() => {
                    this.$location.path("/");
                }).catch((err) => {
                    this.alerts.push({ type: "danger", message: err.data.message });
                });
            }
        }

    }

    app.controller("AccountSettingsCtrl", AccountSettingsCtrl);
}