﻿namespace UserProfile {
    "use strict";

    export class MainCtrl {
        messages: IMessage[];
        alerts: IAlert[];
        chart: IChartData;
        customers: ICustomer[];


        static $inject = [
            "$scope",
            "UserProfileData",
            "$element"
        ];

        constructor(
            private $scope: ng.IScope,
            private userProfileData: UserProfileData,
            private $element: ng.IAugmentedJQuery
        ) {
        }

        $onInit() {

            this.userProfileData.getMessages().then((resp) => {
                this.messages = resp;
            });
            this.userProfileData.getAlerts().then((resp) => {
                this.alerts = resp;
            });

            this.userProfileData.getChartData().then((resp) => {
                this.chart = resp;
            });

            this.userProfileData.getCustomers().then((resp) => {
                this.customers = resp;
                this.$element.find("#dataTable").DataTable();
            });
        }

    }

    app.controller("MainCtrl", MainCtrl);
}