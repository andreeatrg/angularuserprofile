var UserProfile;
(function (UserProfile) {
    "use strict";
    var MainCtrl = (function () {
        function MainCtrl($scope, userProfileData, $element) {
            this.$scope = $scope;
            this.userProfileData = userProfileData;
            this.$element = $element;
        }
        MainCtrl.prototype.$onInit = function () {
            var _this = this;
            this.userProfileData.getMessages().then(function (resp) {
                _this.messages = resp;
            });
            this.userProfileData.getAlerts().then(function (resp) {
                _this.alerts = resp;
            });
            this.userProfileData.getChartData().then(function (resp) {
                _this.chart = resp;
            });
            this.userProfileData.getCustomers().then(function (resp) {
                _this.customers = resp;
                _this.$element.find('#dataTable').DataTable();
            });
        };
        return MainCtrl;
    }());
    MainCtrl.$inject = [
        "$scope",
        "UserProfileData",
        "$element"
    ];
    UserProfile.MainCtrl = MainCtrl;
    UserProfile.app.controller("MainCtrl", MainCtrl);
})(UserProfile || (UserProfile = {}));
//# sourceMappingURL=main-view.controller.js.map