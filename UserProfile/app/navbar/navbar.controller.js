var UserProfile;
(function (UserProfile) {
    "use strict";
    var DataType;
    (function (DataType) {
        DataType[DataType["Message"] = 1] = "Message";
        DataType[DataType["Alert"] = 2] = "Alert";
    })(DataType = UserProfile.DataType || (UserProfile.DataType = {}));
    var NavbarCtrl = (function () {
        function NavbarCtrl($scope, $uibModal, $location, authService, $element, userProfileData) {
            this.$scope = $scope;
            this.$uibModal = $uibModal;
            this.$location = $location;
            this.authService = authService;
            this.$element = $element;
            this.userProfileData = userProfileData;
            this.isMessage = false;
            this.isAlert = false;
            this.isExpand = true;
            this.isDataLoading = false;
            this.messages = [];
            this.alerts = [];
        }
        NavbarCtrl.prototype.$onInit = function () {
            this.alertsInfo();
            this.messagesInfo();
        };
        NavbarCtrl.prototype.messagesInfo = function () {
            var _this = this;
            this.isDataLoading = true;
            this.userProfileData.getMessages().then(function (resp) {
                _this.messages = resp;
                _this.isDataLoading = false;
            });
        };
        NavbarCtrl.prototype.alertsInfo = function () {
            var _this = this;
            this.isDataLoading = true;
            this.userProfileData.getAlerts().then(function (resp) {
                _this.alerts = resp;
                _this.isDataLoading = false;
            });
        };
        NavbarCtrl.prototype.showDropdownData = function (type) {
            switch (type) {
                case DataType.Message:
                    this.isMessage = !this.isMessage;
                    this.isAlert = false;
                    break;
                case DataType.Alert:
                    this.isAlert = !this.isAlert;
                    this.isMessage = false;
                    break;
            }
        };
        NavbarCtrl.prototype.logOut = function () {
            var _this = this;
            var open = this.$uibModal.open({
                animation: true,
                templateUrl: "app/shared/modal/modal.tpl.html",
                controller: "ModalController",
                controllerAs: "vm",
                windowClass: 'show',
                backdropClass: 'show',
                resolve: {
                    entityName: function () {
                        return "logout";
                    }
                }
            });
            open.result.then(function (success) {
                if (success) {
                    _this.authService.isLoggedIn = true;
                    _this.$location.path('/login');
                }
            });
        };
        NavbarCtrl.prototype.toggleNav = function () {
            this.isExpand = !this.isExpand;
            this.isExpand ? this.$element.parent().removeClass('sidenav-toggled') : this.$element.parent().addClass('sidenav-toggled');
        };
        return NavbarCtrl;
    }());
    NavbarCtrl.$inject = [
        "$scope",
        "$uibModal",
        "$location",
        "AuthService",
        "$element",
        "UserProfileData"
    ];
    UserProfile.NavbarCtrl = NavbarCtrl;
    UserProfile.app.controller("NavbarCtrl", NavbarCtrl);
})(UserProfile || (UserProfile = {}));
//# sourceMappingURL=navbar.controller.js.map