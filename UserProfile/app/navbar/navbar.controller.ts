﻿namespace UserProfile {
    "use strict";
    export enum DataType {
        Message = 1,
        Alert = 2
    }
    export class NavbarCtrl {
        isMessage: boolean = false;
        isAlert: boolean = false;
        isExpand: boolean = true;
        isDataLoading: boolean = false;
        messages: IMessage[] = [];
        alerts: IAlert[] = [];
        userId: string;
        userProfilePath: string;
        user: IUserData;
        static $inject = [
            "$scope",
            "$uibModal",
            "$location",
            "AuthService",
            "$element",
            "UserProfileData"
        ];

        constructor(
            private $scope: ng.IScope,
            private $uibModal: angular.ui.bootstrap.IModalService,
            private $location: ng.ILocationService,
            private authService: AuthService,
            private $element: ng.IAugmentedJQuery,
            private userProfileData: UserProfileData
        ) {
            this.$scope.$watch(() => { return localStorage.getItem("isAuthenticated"); }, (newVal, oldVal) => {
                this.initData(newVal);
            });
        }

        $onInit() { }
        initData(isAuthenticated: string) {
            if (JSON.parse(isAuthenticated) === true) {
                this.userId = localStorage.getItem("userId");
                this.userProfileData.getUserData(this.userId).then((data) => {
                    this.user = data;
                    this.userProfilePath = data.file;
                }).catch((err) => {
                    this.userProfilePath = "images/profile/profile_user.jpg";
                });
                this.alertsInfo();
                this.messagesInfo();
            } else {
                this.user = null;
                this.$location.path("login");
                console.log("user null");
            }

        }
        messagesInfo() {
            this.isDataLoading = true;

            this.userProfileData.getMessages().then((resp) => {
                this.messages = resp;
                this.isDataLoading = false;
            });
        }
        alertsInfo() {
            this.isDataLoading = true;

            this.userProfileData.getAlerts().then((resp) => {
                this.alerts = resp;
                this.isDataLoading = false;
            });
        }
        showDropdownData(type: number) {
            switch (type) {
                case DataType.Message:
                    this.isMessage = !this.isMessage;
                    this.isAlert = false;
                    break;
                case DataType.Alert:
                    this.isAlert = !this.isAlert;
                    this.isMessage = false;
                    break;
            }

        }
        logOut() {
            const open = this.$uibModal.open({
                animation: true,
                templateUrl: `app/shared/modal/modal.tpl.html`,
                controller: "ModalController",
                controllerAs: "vm",
                windowClass: "show",
                backdropClass: "show",
                resolve: {
                    entityName: () => {
                        return "logout";
                    }
                }
            });
            open.result.then((success) => {
                if (success) {
                    this.authService.isLoggedIn = false;
                    localStorage.removeItem("token");
                    localStorage.removeItem("isAuthenticated");
                    this.$location.path("/login");
                }
            });
        }
        toggleNav() {
            console.log(this.isExpand);
            this.isExpand = !this.isExpand;
            this.isExpand ? this.$element.parent().removeClass("sidenav-toggled") : this.$element.parent().addClass("sidenav-toggled");
        }
        removeSidenavToggled(e: Event) {
            e.preventDefault();
            angular.element("body").removeClass("sidenav-toggled");
        }
    }
    app.controller("NavbarCtrl", NavbarCtrl);
}