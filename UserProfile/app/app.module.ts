﻿/// <reference path="./userProfile.d.ts" />
namespace UserProfile {
    "use strict";
    export let app: ng.IModule = angular.module("userProfile", [
        "ui.router",
        "ngSanitize",
        "ngAnimate",
        "ngMessages",
        "ngCookies",
        "ui.bootstrap",
        "ngFileUpload",
        "chart.js",
        "ngImgCrop",
        "userprofile-templates"
    ]);

    app.config([
        "$stateProvider", "$urlRouterProvider", "$httpProvider", "$locationProvider",
        ($stateProvider: ng.ui.IStateProvider,
            $urlRouterProvider: ng.ui.IUrlRouterProvider,
            $httpProvider: ng.IHttpProvider,
            $locationProvider: ng.ILocationProvider) => {

            $stateProvider.state("home",
                {
                    url: "/",
                    reloadOnSearch: false,
                    templateUrl: "app/main-view/main-view.html",
                    controller: "MainCtrl",
                    controllerAs: "vm",
                    data: { requiresLogin: true }
                })
                .state("login", {
                    url: "/login",
                    templateUrl: "app/login/login.html",
                    controller: "LoginCtrl",
                    controllerAs: "vm"
                }).state("register", {
                    url: "/register",
                    controller: "RegisterCtrl",
                    controllerAs: "vm",
                    templateUrl: "app/login/register.html"
                }).state("forgotPassword", {
                    url: "/forgot-password",
                    controller: "ForgotPasswordCtrl",
                    controllerAs: "vm",
                    templateUrl: "app/login/forgot-password.html"
                }).state("reset", {
                    url: "/reset/:id",
                    templateUrl: "app/login/reset.html",
                    controller: "ResetCtrl",
                    controllerAs: "vm"
                }).state("portfolio", {
                    url: "/portfolio",
                    reloadOnSearch: false,
                    templateUrl: "app/portfolio/portfolio.tpl.html",
                    controller: "PortfolioCtrl",
                    controllerAs: "vm",
                    data: { requiresLogin: true }
                }).state("friends", {
                    url: "/friends",
                    reloadOnSearch: false,
                    templateUrl: "app/friends/friends.tpl.html",
                    controller: "FriendsCtrl",
                    controllerAs: "vm",
                    data: { requiresLogin: true }
                }).state("person", {
                    url: "/people/{personId}",
                    reloadOnSearch: false,
                    templateUrl: "app/friend-details/friend-details.tpl.html",
                    controller: "FriendDetailsCtrl",
                    controllerAs: "vm",
                    data: { requiresLogin: true }
                }).state("skills", {
                    url: "/skills",
                    reloadOnSearch: false,
                    templateUrl: "app/skills/skills.tpl.html",
                    controller: "SkillsCtrl",
                    controllerAs: "vm",
                    data: { requiresLogin: true }
                }).state("account-settings", {
                    url: "/account-settings",
                    reloadOnSearch: false,
                    templateUrl: "app/account-settings/account-settings.tpl.html",
                    controller: "AccountSettingsCtrl",
                    controllerAs: "vm",
                    data: { requiresLogin: true }
                }).state("portfolio-settings", {
                    url: "/portfolio-settings",
                    reloadOnSearch: false,
                    templateUrl: "app/portfolio-settings/portfolio-settings.tpl.html",
                    controller: "PortfolioSettingsCtrl",
                    controllerAs: "vm",
                    data: { requiresLogin: true }
                }).state("shopping-bag", {
                    url: "/shopping-bag",
                    reloadOnSearch: false,
                    templateUrl: "app/shopping-bag/shopping-bag.tpl.html",
                    controller: "ShoppingBagCtrl",
                    controllerAs: "vm",
                    data: { requiresLogin: true }
                });
            $urlRouterProvider.otherwise("/");

            $httpProvider.interceptors.push(["$q", "$location", ($q: ng.IQService, $location: ng.ILocationService) => {
                return {
                    "request": function (config) {
                        config.headers = config.headers || {};
                        if (localStorage.token) {
                            config.headers.Authorization = "Bearer " + localStorage.token;
                        }
                        return config;
                    },
                    "responseError": function (response) {
                        console.log(response.status);
                        if (response.status === 401 || response.status === 403) {
                            localStorage.removeItem("isAuthenticated");
                            $location.path("/login");
                        }
                        return $q.reject(response);
                    }
                };
            }]);
        }
    ]).run([
        "$rootScope", "$state", "$location", "AuthService",
        ($rootScope: angular.IRootScopeService, $state: ng.ui.IStateService, $location: angular.ILocationService, authService: AuthService) => {

            stateChanged($rootScope, $state, $location);
        }]);

    function stateChanged($rootScope: angular.IRootScopeService, $state: ng.ui.IStateService, $location: angular.ILocationService) {
        $rootScope.$on("$stateChangeStart",
            (event, toState, toParams, fromState) => {
                if (JSON.parse(localStorage.getItem("isAuthenticated")) && (toState.name === "login" || toState.name === "register" || toState.name === "forgotPassword" || toState.name === "reset")) {
                    event.preventDefault();
                    $location.path("/");
                }
            });
    }
}
