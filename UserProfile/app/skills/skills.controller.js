var UserProfile;
(function (UserProfile) {
    "use strict";
    var SkillsCtrl = (function () {
        function SkillsCtrl($scope, userProfileData) {
            this.$scope = $scope;
            this.userProfileData = userProfileData;
            this.devSkills = [];
        }
        SkillsCtrl.prototype.$onInit = function () {
            var _this = this;
            this.devSkills = [{
                    name: 'javascript',
                    value: 80
                },
                {
                    name: 'angular',
                    value: 80
                },
                {
                    name: 'jquery',
                    value: 70
                },
                {
                    name: 'typescript',
                    value: 85
                },
                {
                    name: 'html',
                    value: 90
                },
                {
                    name: 'css',
                    value: 90
                },
                {
                    name: 'bootstrap',
                    value: 85
                }];
            this.userProfileData.getChartData().then(function (data) {
                _this.chart = data;
            });
        };
        return SkillsCtrl;
    }());
    SkillsCtrl.$inject = [
        "$scope",
        "UserProfileData"
    ];
    UserProfile.SkillsCtrl = SkillsCtrl;
    UserProfile.app.controller("SkillsCtrl", SkillsCtrl);
})(UserProfile || (UserProfile = {}));
//# sourceMappingURL=skills.controller.js.map