﻿namespace UserProfile {
    "use strict";

    export class SkillsCtrl {
        chart: IChartData;
        devSkills: IDevSkill[] = [];

        static $inject = [
            "$scope",
            "UserProfileData"
        ];

        constructor(
            private $scope: ng.IScope,
            private userProfileData: UserProfileData
        ) {

        }

        $onInit() {
            this.devSkills = [{
                name: 'javascript',
                value: 80
            },
            {
                name: 'angular',
                value: 80
            },
            {
                name: 'jquery',
                value: 70
            },
            {
                name: 'typescript',
                value: 85
            },
            {
                name: 'html',
                value: 90
            },
            {
                name: 'css',
                value: 90
            },
            {
                name: 'bootstrap',
                value: 85
            }]
            this.userProfileData.getChartData().then((data) => {
                this.chart = data;
            });
        }
    }

    app.controller("SkillsCtrl", SkillsCtrl);
}
