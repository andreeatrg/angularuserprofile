﻿/// <reference path="../Scripts/typings/jquery/jquery.d.ts" />
/// <reference path="../Scripts/typings/angularjs/angular.d.ts" />
/// <reference path="../node_modules/@types/angular-cookies/index.d.ts" />
/// <reference path="../Scripts/typings/angular-ui-router/index.d.ts" />
/// <reference path="../Scripts/typings/angular-ui-bootstrap/index.d.ts" />
/// <reference path="../Scripts/typings/ng-file-upload/ng-file-upload.d.ts" />
/// <reference path="../node_modules/moment/moment.d.ts" />


declare namespace UserProfile {

    interface IMessage {
        id: number,
        message: string,
        date: string,
        name: string
    }

    interface IAlert {
        type: string;
        message: string;
    }

    interface IFriend {
        _id: string;
        firstName: string;
        lastName: string;
        email: string;
        occupation: string;
        file: string;
        fullName?: string;
        status: boolean;
    }

    interface IChartData {
        labels: string[];
        series: string[];
        data: number[][];
        datasetOverride: { [key: string]: string }[];
        options: any;
    }

    interface ICustomer {
        id: number;
        clientName: string;
        email: string;
        country: string;
        orders: number;
        deadline: string;
        price: string;
    }
    interface IDevSkill {
        name: string;
        value: number;
    }
    interface IUserData {
        _id: string;
        file: string;
        email: string;
        password: string;
        userName: string;
        isAdmin: boolean,
        confirmPassword: string,
        newPassword?: string;
        firstName: string,
        lastName: string,
        occupation: string
    }
    interface IPortfolio {
        id: string;
        name: string;
        category?: number;
    }
    interface IPortfolioImage {
        _id: string;
        userId: string;
        portfolioName: string;
        files: [{ path: string, _id: string }];
        category: number;
        
    }
    interface IImageData {
        _id: string,
        imageId: string,
        path: string,
        category: number,
        portfolioName: string,
        currency: string,
        quantity: number,
        sku: string,
        price: string,
        createdAt: string
    }
    interface IPayment {
        create_time: string,
        httpStatusCode: number,
        id: string,
        intent: string,
        links: {
            href: string,
            method: string,
            rel: string
        }[],
        payer: {
            payment_method: string
        },
        state: string,
        transactions: {
            amount: {
                currency: string,
                total: string
            },
            description: string,
            item_list: {
                items: IImageData[]
            },
            related_resources: any[]
        }[]
    }
}