﻿namespace UserProfile {
    "use strict";

    export class FooterCtrl {
        year: number;
        static $inject = [
            "$scope",

        ];

        constructor(
            private $scope: ng.IScope
        ) {
            this.$scope.$watch(() => { return localStorage.getItem("isAuthenticated"); }, (newVal, oldVal) => {
                this.initData(newVal);
            });
        }

        $onInit() {
        }
        initData(isAuthenticated: string) {
            if (JSON.parse(isAuthenticated) === true) {
                this.year = new Date().getFullYear();
            } else {
                this.year = null;
            }
        }
    }

    app.controller("FooterCtrl", FooterCtrl);
}