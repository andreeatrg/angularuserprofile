var UserProfile;
(function (UserProfile) {
    "use strict";
    var FooterCtrl = (function () {
        function FooterCtrl($scope) {
            this.$scope = $scope;
        }
        FooterCtrl.prototype.$onInit = function () {
            this.year = new Date().getFullYear();
        };
        return FooterCtrl;
    }());
    FooterCtrl.$inject = [
        "$scope",
    ];
    UserProfile.FooterCtrl = FooterCtrl;
    UserProfile.app.controller("FooterCtrl", FooterCtrl);
})(UserProfile || (UserProfile = {}));
//# sourceMappingURL=footer.controller.js.map