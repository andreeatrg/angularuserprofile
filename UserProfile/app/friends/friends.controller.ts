﻿namespace UserProfile {
    "use strict";
    export class FriendsCtrl {
        friends: IFriend[] = [];
        userId: string;
        addedFriends: IFriend[] = [];

        static $inject = [
            "$scope",
            "UserProfileData",
            "$uibModal",
            "$state"
        ];

        constructor(
            private $scope: ng.IScope,
            private userProfileData: UserProfileData,
            private $uibModal: angular.ui.bootstrap.IModalService,
            private $state: ng.ui.IStateService
        ) {

        }

        $onInit() {
            this.userId = localStorage.getItem("userId");

            this.userProfileData.getFriends().then((data) => {
                this.friends = data;
                this.getAddedFriendsList();
                console.log(this.friends);
            });

        }
        onSelect($item: IFriend, $model: IFriend, $label: string) {
            $item.fullName = $item.firstName + " " + $item.lastName;
        }
        addFriend(friend: IFriend) {
            friend.status = true;
            if (this.addedFriends) {
                this.updateFriendList(friend);
            } else {
                this.userProfileData.addFriend(this.userId, friend).then((data) => {
                    this.getAddedFriendsList();
                });
            }
        }
        getAddedFriendsList() {
            this.userProfileData.getAddedFriendsList(this.userId).then((data) => {
                this.addedFriends = data;
                this.friends = _.filter(this.friends, (friend) => {
                    let foundFriend = _.find(this.addedFriends, (fr) => {
                        return fr._id === friend._id;
                    });
                    if (foundFriend) {
                        console.log(foundFriend);
                        return foundFriend._id !== this.userId && foundFriend._id !== friend._id;
                    } else {

                        return friend._id !== this.userId;
                    }
                });
                console.log(this.friends);

            }).catch(() => {
                this.addedFriends = [];
                this.friends = _.filter(this.friends, (friend) => {
                    return friend._id !== this.userId;
                });
            });
        }
        updateFriendList(friend: IFriend) {
            this.userProfileData.updateFriendsList(this.userId, friend).then((data) => {
                this.getAddedFriendsList();
            });
        }
        removeFriend(friend: IFriend) {
            const open = this.$uibModal.open({
                animation: true,
                templateUrl: `app/shared/modal/modal.tpl.html`,
                windowClass: "show",
                backdropClass: "show",
                controller: "ModalController",
                controllerAs: "vm",
                resolve: {
                    entityName: () => {
                        return "delete" + " " + friend.firstName + " " + friend.lastName;
                    }
                }
            });
            open.result.then((success) => {
                if (success) {
                    this.friends.push(friend);
                    this.userProfileData.removeFriend(this.userId, friend._id).then((data: IPortfolio[]) => {
                        this.getAddedFriendsList();
                    });
                }
            });
        }
    }

    app.controller("FriendsCtrl", FriendsCtrl);
}