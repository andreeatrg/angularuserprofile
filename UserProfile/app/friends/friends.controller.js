var UserProfile;
(function (UserProfile) {
    "use strict";
    var FriendsCtrl = (function () {
        function FriendsCtrl($scope, userProfileData) {
            this.$scope = $scope;
            this.userProfileData = userProfileData;
            this.friends = [];
        }
        FriendsCtrl.prototype.$onInit = function () {
            var _this = this;
            this.userProfileData.getFriends().then(function (data) {
                _this.friends = data;
            });
        };
        return FriendsCtrl;
    }());
    FriendsCtrl.$inject = [
        "$scope",
        "UserProfileData"
    ];
    UserProfile.FriendsCtrl = FriendsCtrl;
    UserProfile.app.controller("FriendsCtrl", FriendsCtrl);
})(UserProfile || (UserProfile = {}));
//# sourceMappingURL=friends.controller.js.map