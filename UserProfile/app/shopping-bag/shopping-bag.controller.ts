namespace UserProfile {
    "use strict";

    export class ShoppingBagCtrl {
        userId: string;
        item: IPortfolioImage;
        products: { item: IImageData, userId: string, userName: string }[] = [];
        static $inject = [
            "$scope",
            "$uibModal",
            "$location",
            "$element",
            "UserProfileData"
        ];

        constructor(
            private $scope: ng.IScope,
            private $uibModal: angular.ui.bootstrap.IModalService,
            private $location: ng.ILocationService,
            private $element: ng.IAugmentedJQuery,
            private userProfileData: UserProfileData
        ) {
            let products: { item: IImageData, userId: string, userName: string }[] = [];
            this.userProfileData.onAddedItem((data: { item: IImageData, user: IUserData }) => {
                products.push({ item: data.item, userId: data.user._id, userName: data.user.firstName + data.user.lastName });
                console.log(products);
                localStorage.setItem("products", JSON.stringify(products));
            });
        }

        $onInit() {
            this.products = JSON.parse(localStorage.getItem("products")) || [];
            console.log(this.products);
        }
        nextStep() {

        }

        buyImage(img: IImageData, userId: string) {
            this.userProfileData.buyImage(userId, img).then((payment: IPayment) => {
                console.log(payment);
                for (let i = 0; i < payment.links.length; i++) {
                    if (payment.links[i].rel === "approval_url") {
                        window.location.href = payment.links[i].href;
                    }
                }
            });
        }
    }
    app.controller("ShoppingBagCtrl", ShoppingBagCtrl);
}