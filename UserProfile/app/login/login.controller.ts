﻿namespace UserProfile {
    "use strict";

    export class LoginCtrl {
        form: ng.IFormController;
        user: IUserData;
        alerts: IAlert[] = [];
        static $inject = [
            "$scope",
            "AuthService",
            "$location",
            "UserProfileData"
        ];

        constructor(
            private $scope: ng.IScope,
            private authService: AuthService,
            private $location: ng.ILocationService,
            private userProfileData: UserProfileData
        ) {

        }

        $onInit() {
        }
        submitLoginForm(form: ng.IFormController) {
            this.form = form;
            if (form.$valid) {
                this.setToken();
            }

            angular.forEach(form.$error,
                (field) => {
                    angular.forEach(field,
                        (errorField) => {
                            errorField.$setTouched();
                        });
                });


        }
        setToken() {
            this.authService.login(this.user).then((data: { token: string, userId: string, isAuthenticated: boolean }) => {
                localStorage.setItem("token", data.token);
                localStorage.setItem("userId", data.userId);
                localStorage.setItem("isAuthenticated", _.toString(data.isAuthenticated));
                this.authService.isLoggedIn = data.isAuthenticated;
                console.log(this.authService.isLoggedIn);
                if (this.authService.isLoggedIn) {
                    this.$location.path("/");
                }
            }).catch((err) => {
                this.alerts.push({ type: "danger", message: err.data.message });
            });
        }
    }

    app.controller("LoginCtrl", LoginCtrl);
}