namespace UserProfile {
    "use strict";

    export class ForgotPasswordCtrl {
        formRegister: ng.IFormController;
        user: IUserData = new User();
        file: File;
        alerts: IAlert[] = [];
        static $inject = [
            "$scope",
            "AuthService",
            "$timeout",
            "$location"
        ];

        constructor(
            private $scope: ng.IScope,
            private authService: AuthService,
            private $timeout: ng.ITimeoutService,
            private $location: ng.ILocationService
        ) {
        }
        $onInit() { }
        submitResetForm(form: ng.IFormController) {
            this.formRegister = form;
            if (form.$valid) {
                this.forgotPassword();
            }

            angular.forEach(form.$error,
                (field) => {
                    angular.forEach(field,
                        (errorField) => {
                            errorField.$setTouched();
                        });
                });
        }
        forgotPassword() {
            this.authService.forgotPassword(this.user).then((result: { message: string }) => {
                this.alerts.push({ type: "success", message: result.message });
            }).catch((err) => {
                this.alerts.push({ type: "danger", message: err.data.message });
            });
        }

    }

    app.controller("ForgotPasswordCtrl", ForgotPasswordCtrl);
}