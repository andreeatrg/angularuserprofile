namespace UserProfile {
    "use strict";

    export class ResetCtrl {
        resetForm: ng.IFormController;
        user: IUserData = new User();
        file: File;
        alerts: IAlert[] = [];
        static $inject = [
            "$scope",
            "AuthService",
            "$stateParams",
            "$location"
        ];

        constructor(
            private $scope: ng.IScope,
            private authService: AuthService,
            private $stateParams: ng.ui.IStateParamsService,
            private $location: ng.ILocationService
        ) {
        }
        $onInit() { }
        checkPassword(password: string, confirmPassword: string, form: ng.IFormController) {
            this.resetForm = form;
            if (password === confirmPassword) {
                this.resetForm.confirmPassword.$error.validationError = false;
                return true;
            } else {
                this.resetForm.confirmPassword.$error.validationError = true;
                return false;
            }
        }
        submitResetForm(form: ng.IFormController) {
            this.resetForm = form;
            if (form.$valid) {
                this.resetPassword();
            }

            angular.forEach(form.$error,
                (field) => {
                    angular.forEach(field,
                        (errorField) => {
                            errorField.$setTouched();
                        });
                });
        }
        resetPassword() {
            this.authService.resetPassword(this.$stateParams.id, { newPassword: this.user.password, confirmPassword: this.user.confirmPassword }).then((result) => {
                this.$location.path("/login");
            }).catch((err) => {
                this.alerts.push({ type: "danger", message: err.data.message });
            });
        }

    }

    app.controller("ResetCtrl", ResetCtrl);
}