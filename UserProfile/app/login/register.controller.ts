namespace UserProfile {
    "use strict";

    export class RegisterCtrl {
        formRegister: ng.IFormController;
        user: IUserData = new User();
        alerts: IAlert[] = [];
        static $inject = [
            "$scope",
            "AuthService",
            "$timeout",
            "$location"
        ];

        constructor(
            private $scope: ng.IScope,
            private authService: AuthService,
            private $timeout: ng.ITimeoutService,
            private $location: ng.ILocationService
        ) {
        }
        $onInit() { }
        checkPassword(password: string, confirmPassword: string, form: ng.IFormController) {
            this.formRegister = form;
            if (password === confirmPassword) {
                this.formRegister.confirmPassword.$error.validationError = false;
                return true;
            } else {
                this.formRegister.confirmPassword.$error.validationError = true;
                return false;
            }
        }
        submitRegisterForm(form: ng.IFormController, file: File) {
            this.formRegister = form;
            if (form.$valid) {
                this.registerUser(file);
            }

            angular.forEach(form.$error,
                (field) => {
                    angular.forEach(field,
                        (errorField) => {
                            errorField.$setTouched();
                        });
                });
        }

        registerUser(file: File) {
            if (file != null) {
                this.upload(file);
            }
        }
        upload(file: File) {
            this.authService.registerUser(file, this.user).then((result: any) => {
                this.$location.path("/login");
            }).catch((err) => {
                this.alerts.push({ type: "danger", message: err.data.message });
            });
        }

    }

    app.controller("RegisterCtrl", RegisterCtrl);
}