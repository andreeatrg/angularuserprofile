var UserProfile;
(function (UserProfile) {
    "use strict";
    var LoginCtrl = (function () {
        function LoginCtrl($scope, authService, $location) {
            this.$scope = $scope;
            this.authService = authService;
            this.$location = $location;
        }
        LoginCtrl.prototype.$onInit = function () {
            this.isLoggedIn = this.authService.isLoggedIn;
        };
        LoginCtrl.prototype.submitLoginForm = function (form) {
            this.form = form;
            if (form.$valid) {
                this.authService.isLoggedIn = true;
                this.$location.path('/');
            }
            angular.forEach(form.$error, function (field) {
                angular.forEach(field, function (errorField) {
                    errorField.$setTouched();
                });
            });
        };
        return LoginCtrl;
    }());
    LoginCtrl.$inject = [
        "$scope",
        "AuthService",
        '$location'
    ];
    UserProfile.LoginCtrl = LoginCtrl;
    UserProfile.app.controller("LoginCtrl", LoginCtrl);
})(UserProfile || (UserProfile = {}));
//# sourceMappingURL=login.controller.js.map