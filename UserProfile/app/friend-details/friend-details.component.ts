namespace UserProfile {
    "use strict";
    export class AddToCart implements ng.IComponentOptions {
        controller: string;
        template: string;
        controllerAs: string;
        bindings: { [boundProperty: string]: string };
        transclude: boolean;

        constructor() {
            this.template = ``;
            this.controller = "ShoppingBagCtrl";
            this.controllerAs = "vm";
            this.bindings = {
                item: "=",
                userId: "="
            };
        }
    }

    app.component("addToCart", new AddToCart());
}