namespace UserProfile {
    "use strict";
    export class User implements IUserData {
        _id: string;
        file: string;
        email: string;
        password: string;
        userName: string;
        isAdmin: boolean;
        confirmPassword: string;
        firstName: string;
        lastName: string;
        occupation: string;
        newPassword?: string;

        constructor() {
            this.file = undefined;
        }
    }
}