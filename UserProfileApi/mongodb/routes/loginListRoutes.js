(function () {
    'use strict';
    var mongoose = require('mongoose');
    var loginList = require('../controllers/loginListController');
    var portfolioList = require('../controllers/portfolioController');
    var friendsList = require('../controllers/friendsController');
    var express = require('express');
    var app = express.Router();
    var jwt = require('jsonwebtoken');
    var multer = require('multer');
    // apiList Routes
    app.route('/portfolio/:userId/list')
        .get(ensureAuthorized, portfolioList.list_all_portfolios);

    app.route('/portfolio/:userId')
        .get(ensureAuthorized, portfolioList.portfolio_images_for_a_user)
        .post(ensureAuthorized, portfolioList.create_portfolio)
        .delete(ensureAuthorized, portfolioList.delete_a_portfolio_by_userId_for_a_user);

    app.route('/portfolio/:portfolioId/:userId')
        .get(ensureAuthorized, portfolioList.images_from_a_portfolio)
        .put(ensureAuthorized, portfolioList.update_portfolio);

    app.route('/portfolio/:imageId/:portfolioId/:userId')
        .delete(ensureAuthorized, portfolioList.delete_a_image_by_imageId_for_a_user);

    app.route('/users')
        .get(ensureAuthorized, loginList.list_all_users)
        .post(ensureAuthorized, loginList.create_a_user);

    app.route('/users/:userId')
        .get(ensureAuthorized, loginList.read_a_user)
        .put(ensureAuthorized, loginList.update_a_user)
        .delete(ensureAuthorized, loginList.delete_a_user);

    app.route('/auth/login')
        .post(loginList.login);

    app.route('/auth/forgot_password')
        .post(loginList.forgot_password);

    app.route('/auth/reset/:token')
        .post(loginList.reset);

    app.route('/auth/register')
        .post(loginList.register);

    app.route('/image/:userId')
        .get(ensureAuthorized, loginList.image_profile);

    app.route('/friends/:userId')
        .get(ensureAuthorized, friendsList.get_friends)
        .post(ensureAuthorized, friendsList.add_friend)
        .put(ensureAuthorized, friendsList.update_friend)
        .delete(ensureAuthorized, friendsList.remove_all_friends);

    app.route('/friends/:userId/list')
        .get(ensureAuthorized, friendsList.list_all_friends);

    app.route('/friends/:friendId/:userId')
        .delete(ensureAuthorized, friendsList.remove_friend);

    app.route('/paypal/:userId')
        .post(ensureAuthorized, portfolioList.pay_now);

    app.route('/paypal/success')
        .get(portfolioList.execute);

    function ensureAuthorized(req, res, next) {
        var bearerToken;
        var bearerHeader = req.headers.authorization;
        if (bearerHeader) {
            bearerToken = bearerHeader.split(" ");
            if (bearerToken.length === 2) {
                jwt.verify(bearerToken[1], require('../../config').secret, function (error, decodedToken) {
                    if (error) {
                        return res.status(401).send({ "success": false, "error": "Invalid authorization token" });
                    }
                    if (decodedToken.exp > new Date().getTime()) {
                        return res.status(400).send({ "success": false, "error": "Token expired!" });
                    }
                    req.decodedToken = decodedToken;
                    next();
                });
            }
        } else {
            res.status(401).send({ "success": false, "error": "An authorization header is required" });
        }
    }
    module.exports = app;
})();