
(function () {
    "use strict";
    var mongoose = require('mongoose');

    var ListSchema = new mongoose.Schema({
        email: { type: String, required: true },
        occupation: { type: String, required: true },
        firstName: { type: String, required: true },
        lastName: { type: String, required: true },
        file: { type: String, required: true },
        status: { type: Boolean, required: true }
    });
    var FriendSchema = new mongoose.Schema({
        userId: { type: String, required: true },
        list: [ListSchema]
    });

    module.exports = mongoose.model('Friend', FriendSchema);
})();