(function () {
    "use strict";
    var mongoose = require('mongoose');
    var bcrypt = require('bcrypt');
    var SALT_WORK_FACTOR = 10;


    var UserSchema = new mongoose.Schema({
        isAdmin: Boolean,
        firstName: String,
        lastName: String,
        email: { type: String, required: true, index: { unique: true } },
        file: { type: String, required: true },
        occupation: String,
        password: { type: String, required: true },
        confirmPassword: String,
        resetPasswordToken: String,
        resetPasswordExpires: Date
    });

    UserSchema.pre("save", function (next) {
        var user = this;

        // only hash the password if it has been modified (or is new)
        if (!user.isModified('password')) return next();
        // generate a salt
        bcrypt.genSalt(SALT_WORK_FACTOR, function (err, salt) {
            if (err) return next(err);

            // hash the password along with our new salt
            bcrypt.hash(user.password, salt, function (err, hash) {
                if (err) return next(err);

                // override the cleartext password with the hashed one
                user.password = hash;
                user.confirmPassword = hash;
                next();
            });
        });
    });


    module.exports = mongoose.model('User', UserSchema);
})();