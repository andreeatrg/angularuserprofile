(function () {
    "use strict";
    var mongoose = require('mongoose');
    var FilesSchema = new mongoose.Schema({
        path: { type: String, required: true },
        price: { type: String, required: true },
        createdAt: { type: Date, required: true },
    });
    var PortfolioSchema = new mongoose.Schema({
        userId: { type: String, required: true },
        portfolioName: { type: String, required: true },
        files: [FilesSchema]
    });


    module.exports = mongoose.model('Portfolio', PortfolioSchema);
})();