(function () {

    var express = require('express');
    var app = express();
    var config = require('../../config'); // get our config file
    var mongoose = require('mongoose'),
        Friend = mongoose.model('Friend');
    var async = require('async');
    var _ = require('lodash')

    exports.list_all_friends = function (req, res) {
        Friend.find({}, function (err, friends) {
            if (err)
                res.send(err);
            res.json(friends);
        });
    };

    exports.remove_friend = function (req, res) {
        Friend.findOne({ userId: req.params.userId },
            function (err, friend) {
                if (err) throw err;
                if (!friend) {
                    res.send(404, { message: "Friend not found!" });
                } else if (friend) {
                    friend.list.id(req.params.friendId).remove();
                    friend.save(function (err, friend) {
                        if (err)
                            res.send(err);
                        res.json(friend);
                    });
                }
            });
    };
    exports.remove_all_friends = function (req, res) {
        Friend.remove({
            userId: req.params.userId
        }, function (err, user) {
            if (err)
                res.send(err);
            res.json({ message: 'Friend successfully deleted' });
        });
    };

    exports.add_friend = function (req, res) {
        var friend = new Friend(req.body);
        friend.save(function (err, list) {
            if (err)
                res.send(err);
            res.json(list);
        });
    };
    exports.get_friends = function (req, res) {
        Friend.findOne({ userId: req.params.userId }, function (err, friends) {
            if (err)
                res.send(err);
            if (!friends) {
                res.send(404, { message: "Friends list not found!" });
            } else if (friends) {
                res.json(friends.list);
            }
        });
    };
    exports.update_friend = function (req, res) {
        Friend.findOne({ userId: req.params.userId }, function (err, friend) {
            if (err) throw err;

            if (!friend) {
                res.send(404, { message: "Friends list not found!" });
            } else if (friend) {
                friend.list = friend.list.concat(req.body.list);
                friend.save(function (err, friend) {
                    if (err)
                        res.send(err);
                    res.json(friend);
                });
            }
        });
    };
    // If no route is matched by now, it must be a 404
    app.use(function (req, res, next) {
        var err = new Error('Not Found');
        err.status = 404;
        next(err);
    });
    // route middleware to verify a token
    app.use(function (req, res, next) {
        // check header or url parameters or post parameters for token
        var token = req.body.token || req.query.token || req.headers['x-access-token'];

        // decode token
        if (token) {

            // verifies secret and checks exp
            jwt.verify(token, app.get('superSecret'), function (err, decoded) {
                if (err) {
                    return res.json({ success: false, message: 'Failed to authenticate token.' });
                } else {
                    // if everything is good, save to request for use in other routes
                    req.decoded = decoded;
                    next();
                }
            });

        } else {
            // if there is no token
            // return an error
            return res.status(403).send({
                success: false,
                message: 'No token provided.'
            });

        }
    });
})();