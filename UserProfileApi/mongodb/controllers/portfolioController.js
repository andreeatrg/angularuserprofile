(function () {
    'use strict';
    var express = require('express');
    var app = express();
    var config = require('../../config'); // get our config file
    var mongoose = require('mongoose'),
        Portfolio = mongoose.model('Portfolio');
    var async = require('async');
    var _ = require('lodash');

    var paypal = require('paypal-rest-sdk');
    paypal.configure({
        "host": "api.sandbox.paypal.com",
        "port": "",
        'client_id': 'ASM3Mhh3ymQh2ncJZfa7xXmn9LR5lypwiPI3XNZsWsq3bDjNKQCd72KoVtZqbNcIW-avt1Cz3x8qS7fO',
        'client_secret': 'EIohBWnfcxM_MeaG2-VpI9yqlpkXJliWmnCrPZ7PCVfEmizZGxsz4eSbbxM6PW8zzR7ctuZDIGC0Le44'
    });


    exports.pay_now = function (req, res) {
        const create_payment_json = {
            "intent": "authorize",
            "payer": {
                "payment_method": "paypal"
            },
            "redirect_urls": {
                "return_url": config.localhost + '/paypal/success',
                "cancel_url": config.localhost + '/paypal/cancel'
            },
            "transactions": [{
                "item_list": {
                    "items": [{
                        "name": req.body.product.portfolioName,
                        "sku": req.body.product._id + "sku",
                        "price": req.body.product.price,
                        "currency": "USD",
                        "quantity": 1
                    }]
                },
                "amount": {
                    "currency": "USD",
                    "total": req.body.product.price
                },
                "description": req.body.product.portfolioName
            }]
        };
        paypal.payment.create(create_payment_json, function (error, payment) {
            if (error)
                throw error;
            if (!payment) {
                res.send(404, { message: "Payment not found!" });
            } else if (payment.payer.payment_method === 'paypal') {
                res.json(payment);
            }
        });
    };

    exports.execute = function (req, res) {
        const payerId = req.query.PayerID;
        const paymentId = req.query.paymentId;
        var execute_payment_json = {
            "payer_id": payerId,
            "transactions": [{
                "amount": {
                    "currency": "USD",
                    "total": "2.00"
                }
            }]
        };
        paypal.payment.execute(paymentId, execute_payment_json, function (error, payment) {
            if (error) {
                throw error;
            } else {
                console.log("Get Payment Response");
                res.json(payment);
            }
        });
    };
    exports.create_portfolio = function (req, res) {
        var portfolio = new Portfolio(req.body);
        portfolio.save(function (err, files) {
            if (err)
                res.send(err);
            res.json(files);
        });
    };
    exports.portfolio_images_for_a_user = function (req, res) {
        Portfolio.find({ userId: req.params.userId }, function (err, files) {
            if (err)
                res.send(err);
            res.json(files);
        });
    };
    exports.update_portfolio = function (req, res) {

        Portfolio.findOne({ _id: req.params.portfolioId, userId: req.params.userId }, function (err, portfolio) {
            if (err) throw err;

            if (!portfolio) {
                res.send(404, { message: "Portfolio not found!" });
            } else if (portfolio) {
                portfolio.files = portfolio.files.concat(req.body.files);
                portfolio.save(function (err, portfolio) {
                    if (err)
                        res.send(err);
                    res.json(portfolio);
                });
            }
        });


    };
    exports.list_all_portfolios = function (req, res) {
        Portfolio.find({ userId: req.params.userId }, function (err, files) {
            if (err)
                res.send(err);
            if (files) {
                let portfolioName = files.map(function (value, index) {
                    if (value.portfolioName) {
                        return { id: value._id, name: value.portfolioName };
                    }

                }).filter((value, index) => { return value; });
                res.json(portfolioName);
            }
        });
    };
    exports.delete_a_portfolio_by_userId_for_a_user = function (req, res) {
        Portfolio.remove({
            userId: req.params.userId
        }, function (err, user) {
            if (err)
                res.send(err);
            res.json({ message: 'Portfolio successfully deleted' });
        });
    };
    exports.delete_a_image_by_imageId_for_a_user = function (req, res) {
        Portfolio.findById(
            {
                _id: req.params.portfolioId,
                userId: req.params.userId
            },
            function (err, portfolio) {
                if (err) throw err;
                portfolio.files.id(req.params.imageId).remove();
                portfolio.save(function (err, portfolio) {
                    if (err)
                        res.send(err);
                    res.json(portfolio);
                });
            });
    }
    exports.images_from_a_portfolio = function (req, res) {
        Portfolio.findOne({ _id: req.params.portfolioId, userId: req.params.userId }, function (err, images) {
            if (err)
                res.send(err);
            res.json(images);
        });

    };
    exports.list_all_users = function (req, res) {
        User.find({}, function (err, user) {
            if (err)
                res.send(err);
            res.json(user);
        });
    };
    // If no route is matched by now, it must be a 404
    app.use(function (req, res, next) {
        var err = new Error('Not Found');
        err.status = 404;
        next(err);
    });
    // route middleware to verify a token
    app.use(function (req, res, next) {
        // check header or url parameters or post parameters for token
        var token = req.body.token || req.query.token || req.headers['x-access-token'];

        // decode token
        if (token) {

            // verifies secret and checks exp
            jwt.verify(token, app.get('superSecret'), function (err, decoded) {
                if (err) {
                    return res.json({ success: false, message: 'Failed to authenticate token.' });
                } else {
                    // if everything is good, save to request for use in other routes
                    req.decoded = decoded;
                    next();
                }
            });

        } else {
            // if there is no token
            // return an error
            return res.status(403).send({
                success: false,
                message: 'No token provided.'
            });

        }
    });

})();