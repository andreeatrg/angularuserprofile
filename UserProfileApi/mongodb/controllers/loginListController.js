(function () {
    'use strict';
    var express = require('express');
    var app = express();
    var config = require('../../config'); // get our config file
    var fs = require('fs');
    var mongoose = require('mongoose'),
        User = mongoose.model('User');
    var bcrypt = require('bcrypt');
    var SALT_WORK_FACTOR = 10;
    var crypto = require('crypto');
    var jwt = require('jsonwebtoken'); // used to create, sign, and verify tokens
    var async = require('async');
    var _ = require('lodash');
    var nodemailer = require('nodemailer');
    var sgTransport = require('nodemailer-sendgrid-transport');
    var sendgrid = require('@sendgrid/mail');

    sendgrid.setApiKey("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");

    exports.list_all_users = function (req, res) {
        User.find({}, function (err, users) {
            if (err)
                res.send(err);
            let usersFilteredData = _.map(users, (user) => {
                return {
                    _id: user._id,
                    firstName: user.firstName,
                    lastName: user.lastName,
                    occupation: user.occupation,
                    email: user.email,
                    file: user.file
                };
            });
            res.json(usersFilteredData);
        });
    };

    exports.create_a_user = function (req, res) {
        var new_user = new User(req.body);
        new_user.save(function (err, user) {
            if (err)
                res.send(err);
            res.json(user);
        });
    };

    exports.read_a_user = function (req, res) {
        User.findById(req.params.userId, function (err, user) {
            if (err)
                res.send(err);
            res.json({
                _id: user._id,
                firstName: user.firstName,
                lastName: user.lastName,
                occupation: user.occupation,
                email: user.email,
                file: user.file
            });
        });
    };

    exports.update_a_user = function (req, res) {
        User.findOne({ _id: req.params.userId }, function (err, user) {
            if (err) throw err;

            if (!user) {
                res.send(404, { message: "User not found!" });
            } else if (user) {

                user.firstName = req.body.firstName || user.firstName;
                user.lastName = req.body.lastName || user.lastName;
                user.email = req.body.email || user.email;
                user.file = req.body.file || user.file;
                user.occupation = req.body.occupation || user.occupation;
                user.isAdmin = req.body.isAdmin || user.isAdmin;
                user.password = req.body.password || user.password;
                user.confirmPassword = req.body.confirmPassword || user.confirmPassword;
                user.resetPasswordToken = req.body.resetPasswordToken || user.resetPasswordToken;
                user.resetPasswordExpires = req.body.resetPasswordExpires || user.resetPasswordExpires;

                console.log(req.body);
                user.save(function (err, user) {
                    if (err)
                        res.send(err);
                    res.json(user);
                });
            }
        });
    };

    exports.delete_a_user = function (req, res) {
        User.remove({
            _id: req.params.userId
        }, function (err, user) {
            if (err)
                res.send(err);
            res.json({ message: 'User successfully deleted' });
        });
    };

    exports.login = function (req, res) {
        // find the user
        User.findOne({
            email: req.body.email
        }, function (err, user) {
            if (err) throw err;

            if (!user) {
                res.send(404, { message: "User not found!" });
            } else if (user) {
                bcrypt.compare(req.body.password, user.password, function (err, isMatch) {
                    if (err) return res.send(err);
                    // check if password matches
                    if (!isMatch) {
                        res.send(404, { message: "User or password is incorrect!" });
                    } else {
                        // if user is found and password is right
                        // create a token with only our given payload
                        // we don't want to pass in the entire user since that has the password
                        var payload = {
                            email: user.email,
                            password: user.password
                        };
                        var token = jwt.sign(payload, config.secret, {
                            expiresIn: 1440 // expires in 24 hours
                        });

                        // return the information including token as JSON
                        res.json({
                            userId: user._id,
                            isAuthenticated: true,
                            token: token
                        });
                    }
                });
            }

        });
    };

    exports.image_profile = function (req, res) {
        User.findById(req.params.userId, function (err, user) {
            if (err)
                res.send(err);
            // read binary data
            if (user.file) {
                res.send(user.file);
            } else {
                res.status(404).send({ message: "Image not found!" });
            }
        });
    };

    exports.forgot_password = function (req, res, next) {
        async.waterfall([
            function (done) {
                crypto.randomBytes(20, function (err, buf) {
                    var token = buf.toString('hex');
                    done(err, token);
                });
            },
            function (token, done) {
                User.findOne({ email: req.body.email }, function (err, user) {
                    if (!user) {
                        res.sendStatus(404).send({ message: "Email not found!" }).end();
                    }

                    user.resetPasswordToken = token;
                    user.resetPasswordExpires = Date.now() + 3600000; // 1 hour

                    user.save(function (err) {
                        done(err, token, user);
                    });
                });
            },
            function (token, user) {
                const msg = {
                    to: user.email,
                    from: 'noreplay@test.com',
                    subject: 'Password Reset',
                    text: 'and easy to do anywhere, even with Node.js',
                    html: 'You are receiving this because you (or someone else) have requested the reset of the password for your account.\n\n' +
                    'Please click on the following link, or paste this into your browser to complete the process:\n\n' +
                    'http://localhost:3000/#/reset/' + token + '\n\n' +
                    'If you did not request this, please ignore this email and your password will remain unchanged.\n',
                };
                sendgrid.send(msg, function (err, result) {
                    if (err) {
                        return res.sendStatus(500).send({ message: "Email not sent!" }).end();
                    }
                    res.json({ message: 'Email sent! Please verify your email address!' });
                });
            }
        ], function (error) {
            if (error) {
                return res.sendStatus(500).send({ message: "An error has occurred!" }).end();
            }
        });
    };

    exports.register = function (req, res, next) {
        var data = {
            email: req.body.email,
            password: req.body.password,
            firstName: req.body.firstName,
            lastName: req.body.lastName,
            confirmPassword: req.body.confirmPassword,
            occupation: req.body.occupation,
            file: req.body.file,
            isAdmin: false
        };
        var new_user = new User(data);
        new_user.save(function (err, user) {
            if (err)
                res.status(500).send(err);
            res.json(user);
        });
    };
    exports.reset = function (req, res, next) {
        User.findOne({ resetPasswordToken: req.params.token }, function (err, user) {
            if (err) throw err;

            if (!user) {
                res.send(404, { message: "User not found!" });
            } else if (user) {
                user.password = req.body.newPassword;
                user.confirmPassword = req.body.confirmPassword;
                console.log(req.body);
                user.save(function (err, user) {
                    if (err)
                        res.send(err);
                    res.json(user);
                });
            }
        });
    };
    // If no route is matched by now, it must be a 404
    app.use(function (req, res, next) {
        var err = new Error('Not Found');
        err.status = 404;
        next(err);
    });
    // route middleware to verify a token
    app.use(function (req, res, next) {
        // check header or url parameters or post parameters for token
        var token = req.body.token || req.query.token || req.headers['x-access-token'];

        // decode token
        if (token) {

            // verifies secret and checks exp
            jwt.verify(token, app.get('superSecret'), function (err, decoded) {
                if (err) {
                    return res.json({ success: false, message: 'Failed to authenticate token.' });
                } else {
                    // if everything is good, save to request for use in other routes
                    req.decoded = decoded;
                    next();
                }
            });

        } else {
            // if there is no token
            // return an error
            return res.status(403).send({
                success: false,
                message: 'No token provided.'
            });

        }
    });

})();