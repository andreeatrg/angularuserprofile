(function () {
    'use strict';
    var express = require('express');
    // var session = require('express-session');
    var app = express();
    var expressValidator = require('express-validator');
    var path = require('path');
    var port = process.env.PORT || 4000;
    var async = require('async');

    var mongoose = require('mongoose'),
        morgan = require('morgan'),
        bodyParser = require('body-parser');

    var config = require('./config'); // get our config file
    var User = require('./mongodb/models/loginListModel'); // get our mongoose model
    var Portfolio = require('./mongodb/models/portfolioModel');
    var Friend = require('./mongodb/models/friendsModel');

    // mongoose instance connection url connection
    mongoose.Promise = global.Promise;
    mongoose.connect(config.database, {
        useMongoClient: true
    }); // connect to database

    app.set('superSecret', config.secret); // secret variable
    // use morgan to log requests to the console
    app.use(morgan('dev'));
    app.use(bodyParser.urlencoded({ extended: true }));
    app.use(bodyParser.json({
        type: 'application/json',
        limit: 52428800
    }));
    // We add the middleware after we load the body parser
    app.use(expressValidator());
    // app.use(session({ secret: 'session secret key' }));
    //Secure
    app.all('/*', function (req, res, next) {
        // CORS headers
        res.header("Access-Control-Allow-Origin", "*"); // restrict it to the required domain
        res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
        // Set custom headers for CORS
        res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization');
        if (req.method == 'OPTIONS') {
            res.status(200).end();
        } else {
            next();
        }
    });



    // routes ================
    var routes = require('./mongodb/routes/loginListRoutes');
    app.use('/', routes);
    app.use(express.static('routes'));


    app.listen(port, function () {
        console.log('Listening on port 3000');
    });

    module.exports = app;
})();